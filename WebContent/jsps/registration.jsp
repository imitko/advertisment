<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
    <%@ taglib uri='http://java.sun.com/jsp/jstl/fmt' prefix='fmt'%>
    <fmt:setBundle basename="resources.elements"/>
<!DOCTYPE html>
<html>
<head>
 <meta charset="utf-8">
  <title><fmt:message key='title.personaldata'/></title>

<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<link href="<c:url value='/css/default.css' />" rel="stylesheet"></link>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style type="text/css">
<%@include file='../css/default.css' %></style>
<script type="text/javascript">
function error () {	
	    var a = document.getElementById('password').value;
	    var b = document.getElementById('confirmpass').value;
	     if (a == b){
	    	 return true;
	   }
	     alert ('<fmt:message key='warning.confirmpass'/>');
	     return false;
	}
</script>
</head>
<body>
<%@include file="topmenu.jsp"%>
	<%@include file="header.jsp"%>
	<!--main-->
	<div class="container">
		<div class="row">
			<!--left-->
			

		<div class="col-md-12">
				<div class="row">
					<div class="col-md-12">
		<p>${success}${error}</p>
		<h1 class="center"><fmt:message key='h1.personaldata'/></h1>
		<br>
		

	<form name="registration" method="POST" action="${pageContext.request.contextPath}/controller" onsubmit="return error()" class="form-horizontal"> 
	<c:choose>
		<c:when test="${edit}">
		<input type="hidden" name="command" value="updateuser" />
		</c:when>
		<c:otherwise>
	<input type="hidden" name="command" value="registration" /> 
	</c:otherwise>
	</c:choose>
	<input type="hidden" name="userId" value="${loggeduser.id}"/>
	
	<div class="row">
		<div class="col-md-12">
		<label class="col-md-3 control-lable" for="name"><fmt:message key='label.name'/> </label>
	<div class="col-md-7"><input type="text" name="username" value="${loggeduser.name}" required class="form-control input-sm"/><br/> </div>
	</div>
	</div>
	
	<div class="col-md-12">
	<div class="row">
		<label class="col-md-3 control-lable" for="email"><fmt:message key='label.email'/> </label>
	<c:choose>
		<c:when test="${edit && role eq 'USER'}">
	<div class="col-md-7"><input type="email" name="email" value="${loggeduser.email}" readonly="readonly" class="form-control input-sm"/><br/> </div>
	</c:when>
		<c:otherwise>
		<div class="col-md-7"><input type="email" name="email" value="${loggeduser.email}" class="form-control input-sm"/><br/> </div>
		</c:otherwise>
					</c:choose>
	</div>
	 </div>
	 
	 <div class="row">
		<div class="col-md-12">
		<label class="col-md-3 control-lable" for="pass"><fmt:message key='label.phone'/> </label>
		
	<div class="col-md-7"><input type="tel" name="phone" value="${loggeduser.phone}" placeholder="+375 XX XXXXXXX" pattern="[\+]\d{3}\s\d{2}\s\d{7}"
	maxlength="19" required class="form-control input-sm"/>
	 
					
		<br/> </div>
	</div>
	</div>
	<div class="row">
		<div class="col-md-12">
		<label class="col-md-3 control-lable" for="password"><fmt:message key='label.password'/> </label>
	<div class="col-md-7"><input type="password" name="password" value="" minlength="5" id="password" required class="form-control input-sm"/><br/> </div>
	</div>
	</div>
	<div class="row">
		<div class="col-md-12">
		<label class="col-md-3 control-lable" for="confirmpass"><fmt:message key='label.confirmpassword'/> </label>
	<div class="col-md-7"><input type="password" name="confirmpass" value="" minlength="5" id="confirmpass"  required class="form-control input-sm"/><br/> </div>
	
	<p id="password_status"></p>
	
	</div>
	</div>
		<div class="row">
			<div class="col-md-12">
			<label class="col-md-3 control-lable" for=""></label>
		<c:choose>
			<c:when test="${edit}">
				<input class="col-md-7" type="submit" value="<fmt:message key='label.save'/>"/>
				</c:when>
			<c:otherwise>
		<input class="col-md-7" type="submit"  value="<fmt:message key='label.registration'/>"  id="submit"/>
			</c:otherwise>
		</c:choose>
			</div>
		</div>
	</form> 
	<br>
	<br>
</div>
			<!--/right-->
		</div>
		<!--/row-->
	</div>
	<!--/container-->
</div>
</div>
<%@include file="footer.jsp"%>
</body>
</html>
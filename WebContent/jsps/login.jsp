<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/fmt' prefix='fmt'%>
<fmt:setBundle basename="resources.elements" />
<!DOCTYPE html>
<html>
<head>

<title><fmt:message key='title.login' /></title>
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script
	src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<link href="<c:url value='../css/default.css' />" rel="stylesheet"></link>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style type="text/css">
<%@include file ='../css/default.css'%>
</style>
</head>
<body>
	<%@include file="topmenu.jsp"%>
	<%@include file="header.jsp"%>
	<!--main-->
	<div class="container">
		<div class="row">
			
			<div class="col-md-12">
				<div class="row">
					<div class="col-md-12">
						<p>${error}</p>
						<h1 class="center">
							<fmt:message key='h1.login' />
						</h1>
						<br>
					</div>
				</div>
				<form name="login" method="POST"
					action="${pageContext.request.contextPath}/controller"
					class="form-horizontal">

					<input type="hidden" name="command" value="login" />
					<div class="row">
						<div class="col-md-12">
							<label class="col-md-3 control-lable" for="email"><fmt:message
									key='label.email' /> </label>
							<div class="col-md-7">
								<input type="text" name="email" value=""
									class="form-control input-sm" /><br />
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<label class="col-md-3 control-lable" for="pass"><fmt:message
									key='label.password' /> </label>
							<div class="col-md-7">
								<input type="password" name="password" value=""
									class="form-control input-sm" /><br />
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<label class="col-md-3 control-lable" for=""></label> <input
								class="col-md-7" type="submit"
								value="<fmt:message key='label.enter'/>" />
							<p></p>
						</div>
					</div>
				</form>
				<p></p>

			</div>
			<!--/right-->
		</div>
		<!--/row-->
	</div>
	<!--/container-->
	<%@include file="footer.jsp"%>
</body>
</html>
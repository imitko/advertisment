<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="ctg" uri="customtags"%>
<%@ taglib prefix="main" uri="mainpageads"%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/fmt' prefix='fmt'%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<fmt:setBundle basename="resources.elements"/>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  
  <title><fmt:message key='title.main'/></title>

<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<style type="text/css">
   <%@include file='../css/default.css' %>
</style>
 </head>
 <body>

<%@include file="topmenu.jsp" %>

<%@include file="header.jsp" %>

<!--main-->
<div class="container">
  <div class="row">
  
      <!--left-->
      <div class="col-md-3" id="leftCol">
		<%@include file="leftMenu.jsp" %>
        
      </div><!--/left-->
      
      <!--right-->
      <div class="col-md-9">
     ${wrongAction}
        <h1><fmt:message key='h2.main'/></h1>
        
        <p>
          <fmt:message key='text.main'/></p>
        <hr>
        
        <main:lastads/>
        
       
        </div><!--/right-->
    </div><!--/row-->
</div><!--/container-->

<div class="container">
  <div class="text-center">
    <h2><fmt:message key='h2.possibilities'/></h2>
     <div class="row">
          <div class="col-md-4">
            <div class="panel panel-default">
              <div class="panel-heading"><h3><fmt:message key='h3.mpage1'/></h3></div>
              <div class="panel-body"><fmt:message key='text.main1'/>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="panel panel-default">
              <div class="panel-heading"><h3><fmt:message key='h3.mpage2'/></h3></div>
              <div class="panel-body"><fmt:message key='text.main3'/>
              </div>
            </div>
          </div>  
             <div class="col-md-4">
            <div class="panel panel-default">
              <div class="panel-heading"><h3><fmt:message key='h3.mpage3'/></h3></div>
              <div class="panel-body"><fmt:message key='text.main2'/>
              </div>
            </div>
          </div>
        </div>
        <hr>
  </div>
</div>
<%@include file="footer.jsp" %>



 </body>
</html>
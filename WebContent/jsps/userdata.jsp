<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/fmt' prefix='fmt'%>
<fmt:setBundle basename="resources.elements"/>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  
  <title><fmt:message key='title.edituser'/></title>
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href=css/default.css></link>
<style type="text/css">
<%@include file='../css/default.css' %></style>

 </head>
 <body>
<%@include file="topmenu.jsp" %>
<%@include file="header.jsp" %>

<!--main-->
<div class="container">

  <div class="row">
      <!--left-->
      <div class="col-md-3" id="leftCol">
		<%@include file="leftMenu.jsp" %>
      </div><!--/left-->
      
      <!--right-->
      <div class="col-md-9">
      <h1><fmt:message key='h1.edituser'/></h1>
      <form name="edituser" method="POST" action="${pageContext.request.contextPath}/controller" onsubmit="return error()" class="form-horizontal"> 
	
	<input type="hidden" name="command" value="updateuser" /> 
	
	<input type="hidden" name="userId" value="${manageduser.id}"/>
	
	<div class="row">
		<div class="col-md-12">
		<label class="col-md-3 control-lable" for="username"><fmt:message key='label.name'/> </label>
	<div class="col-md-7"><input type="text" name="username" value="${manageduser.name}" class="form-control input-sm"/><br/> </div>
	</div>
	</div>
	
	<div class="col-md-12">
	<div class="row">
		<label class="col-md-3 control-lable" for="email"><fmt:message key='label.email'/> </label>
		<div class="col-md-7"><input type="email" name="email" value="${manageduser.email}" class="form-control input-sm"/><br/> </div>
	</div>
	 </div>
	 
	 
	 <div class="row">
		<div class="col-md-12">
		<label class="col-md-3 control-lable" for="pass"><fmt:message key='label.phone'/> </label>
		
	<div class="col-md-7"><input type="text" name="phone" value="${manageduser.phone}" pattern="[\+]\d{3}\s\d{2}\s\d{7}"
	 minlength="15" maxlength="19" class="form-control input-sm"/>
		<br/> 
		<input class="col-md-7" type="submit" value="<fmt:message key='label.save'/>"/>
	</div>
	</div>
	</div>
	
	</div>
	</div>
		<div class="row">
			<div class="col-md-12">
		
			</div>
		</div>
	</form> 

        </div><!--/right-->
    </div><!--/row-->
</div><!--/container-->
<%@include file="footer.jsp" %>

 </body>
</html>
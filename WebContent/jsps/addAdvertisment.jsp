<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
    <%@ taglib uri='http://java.sun.com/jsp/jstl/fmt' prefix='fmt'%>
    <fmt:setBundle basename="resources.elements"/>
    
<!DOCTYPE html>
<html>
<head>
 <meta charset="utf-8">
  <title><fmt:message key='title.addads'/></title>
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<link href="../css/default.css" rel="stylesheet"></link>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script type="text/javascript">
function func(){
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1;
	var yyyy = today.getFullYear();
	if(dd<10) {
	    dd = '0'+dd
	} 
	if(mm<10) {
	    mm = '0'+mm
	} 
	today = yyyy + '-' + mm + '-' + dd;
	document.getElementById('startdate').value =today;
	}
	
function func1(){
	var today = new Date(),
	    newDate = new Date();
	newDate.setDate(today.getDate()+17);
	var dd = newDate.getDate();
	var mm = newDate.getMonth()+1; //January is 0!
	var yyyy = newDate.getFullYear();
	newDate = yyyy + '-' + mm + '-' + dd;
	document.getElementById('enddate').value =newDate;
	
	}
</script>
<style type="text/css">
<%@include file='../css/default.css' %></style>
</head>
<body>
<%@include file="topmenu.jsp" %>
<%@include file="header.jsp" %>
<div class="row">
		<div class="col-md-12">
		
		<c:choose>
		<c:when test="${editadv}"><h2 class="center"><fmt:message key='h1.editads'/></h2>
		</c:when>
		<c:otherwise>
		<h2 class="center"><fmt:message key='h1.addads'/></h2>
		</c:otherwise>
	</c:choose>
		<br>
		</div>
</div>
<form name="addAdv" method="POST" action="${pageContext.request.contextPath}/controller" class="form-horizontal">
		<c:choose>
			<c:when test="${editadv}">
				<input type="hidden" name="command" value="updateadv" />
			</c:when>
			<c:otherwise>
				<input type="hidden" name="command" value="newadv" />
			</c:otherwise>
		</c:choose>
		<input type="hidden" name="id" value=""/>
		<input type="hidden" name="advId" value="${adv.id}"/>
		<input type="hidden" name="userId" value="${userId}"/>
		<div class="row">
			<div class="col-md-12">
				<label class="col-md-3 control-lable" for="category"><fmt:message key='label.category'/></label>
				<div class="col-md-7">
				
		<select name="category" required class="form-control input-sm"  onchange='func(), func1()'>
				<c:choose>
						<c:when test="${editadv}">
							<option selected value='${category.id}'>${category.name}</option>
							</c:when>
						<c:otherwise>
							<option selected disabled value=''><fmt:message key='label.choose'/></option>
						</c:otherwise>
					</c:choose>
   						 	<c:forEach var="clip" items="${categories}">
        							<option value="${clip.id}"><c:out value="${clip.name}" ></c:out></option>   
    							</c:forEach>                     
					</select><br />
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-12">
				<label class="col-md-3 control-lable" for="title"><fmt:message key='label.header'/> </label>
				<div class="col-md-7">
					<input type="text" name="title" value="${adv.title}" required
						class="form-control input-sm" /><br />
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<label class="col-md-3 control-lable" for="description"><fmt:message key='label.content'/> </label>
				<div class="col-md-7">
					<input type="text" name="description" value="${adv.text}" required
						class="form-control input-sm" /><br />
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<label class="col-md-3 control-lable" for="price"><fmt:message key='label.price'/> </label>
				<div class="col-md-7">
					<input type="number" name="price" pattern="^[ 0-9]+$" min="0" value="${adv.price}"
						required class="form-control input-sm" /><br />
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<label class="col-md-3 control-lable" for="startdate"><fmt:message key='label.start'/> </label>
				<div class="col-md-7">
					<input type="text" readonly name="startdate" id="startdate" value="${adv.startDate}"
						class="form-control input-sm" /><br />
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<label class="col-md-3 control-lable" for="enddate"><fmt:message key='label.end'/> </label>
				<div class="col-md-7">
					<input type="text" readonly name="enddate" id="enddate" value="${adv.endDate}"
						class="form-control input-sm" /><br />
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<label class="col-md-3 control-lable" for=""></label> <input
					class="col-md-7" type="submit" value="<fmt:message key='label.continue'/>" />
					<p></p>
			</div>
		</div>
	</form>
<p></p>
<%@include file="footer.jsp" %>
</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="msgs" uri="messagestag"%>
<%@ taglib prefix="sent" uri="sentmessagestag"%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/fmt' prefix='fmt'%>
<fmt:setBundle basename="resources.elements"/>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  
  <title><fmt:message key='title.messages'/></title>
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href=css/default.css></link>
<style type="text/css">
<%@include file='../css/default.css' %></style>
<script type="text/javascript">
function openTab(evt, cityName) {
    
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
}
</script>
 </head>
 <body>
<%@include file="topmenu.jsp" %>

<%@include file="header.jsp" %>

<!--main-->
<div class="container">
  <div class="row">
      <!--left-->
      <div class="col-md-3" id="leftCol">

        <%@include file="leftMenu.jsp" %>
      </div><!--/left-->
      
      <!--right-->
      <div class="col-md-9">
      
        <h1><fmt:message key='h1.messages'/></h1>
        <!-- Tab links -->
<div class="tab">
  <button class="tablinks" onclick="openTab(event, 'Received')"><fmt:message key='messages.received'/></button>
  <button class="tablinks" onclick="openTab(event, 'Sent')"><fmt:message key='messages.sent'/></button>
 
</div>
<!-- Tab content -->
<div id="Received" class="tabcontent">
 
  <msgs:messages/>
</div>

<div id="Sent" class="tabcontent">
  <sent:sentmessages/>
</div>

	<p></p>
        </div><!--/right-->
    </div><!--/row-->
</div><!--/container-->

<div class="container">
 </div>

<%@include file="footer.jsp" %>
 </body>
</html>
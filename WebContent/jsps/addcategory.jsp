<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="ctg" uri="customtags"%>
<%@ taglib prefix="ctg3" uri="customtags3"%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/fmt' prefix='fmt'%>
<fmt:setBundle basename="resources.elements"/>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  
  <title><fmt:message key='title.addcategory'/></title>

<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href=css/default.css></link>
<style type="text/css">
<%@include file='../css/default.css' %></style>

 </head>
 <body>
<%@include file="topmenu.jsp" %>
<%@include file="header.jsp" %>
<!--main-->
<div class="container">
  <div class="row">
      <!--left-->
      <div class="col-md-3" id="leftCol">
		<%@include file="leftMenu.jsp" %>
      </div><!--/left-->
      <!--right-->
      <div class="col-md-9">
        <h2><fmt:message key='title.addcategory'/></h2>
        <form name="addcategory" method="POST" action="${pageContext.request.contextPath}/controller" class="form-horizontal">
        <input type="hidden" name="categoryid" value="${category.id}"/>
        <c:choose>
		<c:when test="${category.id > 0}">
		<input type="hidden" name="command" value="updatecategory" />
		</c:when>
		<c:otherwise>
        <input type="hidden" name="command" value="newcategory" />
        </c:otherwise>
        </c:choose>
        
        
        <div class="row">
			<div class="col-md-12">
				<label class="col-md-2 control-lable" for="title"><fmt:message key='label.header'/> </label>
				<div class="col-md-9">
					<input type="text" name="title" value="${category.name}" required
						class="form-control input-sm" /><br />
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<label class="col-md-1 control-lable" for=""></label> 
				<input class="col-md-7" type="submit" value="<fmt:message key='label.save'/>" />
			</div>
		</div>
        </form>
        
        
        
        </div><!--/right-->
    </div><!--/row-->
</div><!--/container-->
<%@include file="footer.jsp" %>

 </body>
</html>
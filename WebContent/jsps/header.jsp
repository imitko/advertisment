<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/fmt' prefix='fmt'%>
<fmt:requestEncoding value="UTF-8" />
<%@ taglib prefix="ctg" uri="customtags"%>
<fmt:setBundle basename="resources.elements"/>
<div id="masthead">  
  <div class="container">
      <div class="row">
        <div class="col-md-7">
          <h1><fmt:message key='h1.main'/>
            <p class="lead"><fmt:message key='h1.sub'/></p>
          </h1>
        </div>
        <div class="col-md-5">
            <div class="well well-lg"> 
              <div class="row">
                <div class="col-sm-3">
                  
                  <i class="fa fa-hand-peace-o" style="font-size:56px;color:red; margin-left: 20px;"></i>
                </div>
                <div class="col-sm-9">
                <ctg:hello role="${role}"/>
                </div>
              </div>
            </div>
        </div>

      </div> 
      <hr>
  </div><!--/container-->
</div><!--/masthead-->
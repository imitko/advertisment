<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri='http://java.sun.com/jsp/jstl/fmt' prefix='fmt'%>
    <fmt:setBundle basename="resources.elements"/>
<div id="footer" class="navbar-default container-fluid">
  <div class="container-fluid">
  
    <address>
    <p></p>
         <strong><fmt:message key='footer.company'/></strong><br /> <fmt:message key='footer.address'/><br /><fmt:message key='footer.city'/><br /> <fmt:message key='footer.tel'/>
      </address>
</div>
    
  </div>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/fmt' prefix='fmt'%>
<fmt:setBundle basename="resources.elements"/>
<nav class="navbar navbar-default navbar-fixed-top" role="banner">
  <div class="container">
  <div class="row">
  <div class="col-md-8">
    <div class="navbar-header">
      <a href="${pageContext.request.contextPath}/" class="navbar-brand"><fmt:message key='menu.main'/></a>
    </div>
    
    <nav class="collapse navbar-collapse" role="navigation">
      <ul class="nav navbar-nav">
        <li>
        
          <a href="${pageContext.request.contextPath}/jsps/rules.jsp"><fmt:message key='menu.rules'/></a>
        </li>
        <li>
          <a href="${pageContext.request.contextPath}/jsps/addAdvertisment.jsp"><fmt:message key='menu.addadv'/></a>
        </li>
      </ul>
    </nav>
    </div>
    <div class="col-md-4">
    
    <p class="lang"><a href="${pageContext.request.contextPath}/controller?command=locale&lang=be"><img width="30" src="${pageContext.request.contextPath}/images/be.png"></img></a><a href="${pageContext.request.contextPath}/controller?command=locale&lang=ru"><img width="30" src="${pageContext.request.contextPath}/images/ru.png"></img></a><a href="${pageContext.request.contextPath}/controller?command=locale&lang=en"><img width="30" src="${pageContext.request.contextPath}/images/en.png"></img></a></p>
    </div>
    </div>
  </div>
</nav>
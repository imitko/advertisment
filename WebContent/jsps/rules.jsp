<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="msg" uri="messagetag"%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/fmt' prefix='fmt'%>
<fmt:setBundle basename="resources.elements"/>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  
  <title><fmt:message key='title.rules'/></title>
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href=css/default.css></link>
<style type="text/css">
<%@include file='../css/default.css' %></style>

 </head>
 <body>
<%@include file="topmenu.jsp" %>

<%@include file="header.jsp" %>

<!--main-->
<div class="container">
  <div class="row">
     
      
      
      <div class="col-md-12">
      <h2><fmt:message key='h2.rules'/></h2>
      <h4><fmt:message key='rules.h4-1'/></h4>
      <p><fmt:message key='rules.paragraph1'/></p>
      <h4><fmt:message key='rules.h4-2'/></h4>
      <p><fmt:message key='rules.paragraph2'/></p>
      <h4><fmt:message key='rules.h4-3'/></h4>
      <p><fmt:message key='rules.paragraph3'/></p>
      
      
      
      
        </div><!--/right-->
    </div><!--/row-->
</div><!--/container-->



<%@include file="footer.jsp" %>



 </body>
</html>
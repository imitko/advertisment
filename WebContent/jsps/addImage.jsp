<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
    <%@ taglib uri="/WEB-INF/tld/images.tld" prefix="ctg1" %>
    <%@ taglib uri='http://java.sun.com/jsp/jstl/fmt' prefix='fmt'%>
    <fmt:setBundle basename="resources.elements"/>
<!DOCTYPE html>
<html>
<head>
 <meta charset="utf-8">
  <title><fmt:message key='title.addimg'/></title>
  
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<link href="<c:url value='/css/default.css' />" rel="stylesheet"></link>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style type="text/css">
<%@include file='../css/default.css' %></style>


</head>
<body>
<%@include file="topmenu.jsp" %>

<%@include file="header.jsp" %>

<!--main-->
<div class="container">
  <div class="row">
      <!--left-->
      <div class="col-md-3" id="leftCol">

      
         <%@include file="leftMenu.jsp" %>
        
      </div><!--/left-->
      
      <!--right-->
      <div class="col-md-9">
		<p class="center">${saveadvsuccess}${error}<p>
		<h2 class="center"><fmt:message key='h1.addimg'/></h2>
		<p></p>
		
		<form name="addimage" method="POST" action="${pageContext.request.contextPath}/controller" enctype="multipart/form-data" class="form-horizontal">
		<input type="hidden" name="command" value="addimage" />
			
		<input type="hidden" name="id" value=""/>
		<input type="hidden" name="userid" value="${loggeduser.id}"/>
		<input type="hidden" name="advId" value="${adv1.id}" />
		<ctg1:images adv1="${adv1}"/>
		
		
		
		</form>
		<p></p>
		
</div>
</div>

<p></p>
        
       
        </div><!--/right-->
    </div><!--/row-->
</div><!--/container-->


<%@include file="footer.jsp" %>


</body>
</html>
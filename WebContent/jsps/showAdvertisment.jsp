<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="ctg" uri="customtags"%>
<%@ taglib prefix="ctg3" uri="customtags3"%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/fmt' prefix='fmt'%>
<fmt:setBundle basename="resources.elements"/>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  
  <title><fmt:message key='title.advertisment'/></title>

<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href=css/default.css></link>
<style type="text/css">
<%@include file='../css/default.css' %></style>
<script type='text/javascript'>
window.onload = function(){
    var id_images = ['image0','image1','image2'] ,image, even = true, imageWidth;
    for(var i=0; i< id_images.length; i++){
  image = document.getElementById(id_images[i]);
  imageWidth = image.clientWidth;
          
         image.onclick = function(ev,imageWidth){
return function(){
   if (ev) {
	   this.style.zoom = "4";
       
       ev = false;
   } else {
	   this.style.zoom = "1";
      ev = true;
   }
      }                    
 }(even,imageWidth);

}
};
     </script>
 </head>
 <body>
<%@include file="topmenu.jsp" %>
<%@include file="header.jsp" %>

<!--main-->
<div class="container">
  <div class="row">
      <!--left-->
      <div class="col-md-3" id="leftCol">
		<%@include file="leftMenu.jsp" %>
      </div><!--/left-->
      
      <!--right-->
      <div class="col-md-9">
        <p>${notfound}${sendmessage}${sendmark}</p>
        <ctg3:advertisment/>
        <br>
        <br>
        <br>
        <br>
        </div><!--/right-->
    </div><!--/row-->
</div><!--/container-->
<%@include file="footer.jsp" %>



 </body>
</html>
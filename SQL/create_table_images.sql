--
-- Структура таблицы `images`
--

CREATE TABLE `images` (
  `id` bigint(11) NOT NULL,
  `advertisment_id` int(11) NOT NULL COMMENT 'Id объявления которому привязывается изображение',
  `file_name` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `image` mediumblob NOT NULL COMMENT 'Картинка для объявления',
  `encoding` longtext COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Индексы таблицы `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`,`advertisment_id`),
  ADD KEY `fk_images_advertisment_id` (`advertisment_id`) USING BTREE;


--
-- Ограничения внешнего ключа таблицы `images`
--
ALTER TABLE `images`
  ADD CONSTRAINT `fk_images_advertisment_id` FOREIGN KEY (`advertisment_id`) REFERENCES `advertisment` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- AUTO_INCREMENT для таблицы `images`
--
ALTER TABLE `images`
  MODIFY `id` bigint(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

--
-- Структура таблицы `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL COMMENT 'Уникальный идентификатор пользователя',
  `name` varchar(45) DEFAULT 'Пользователь' COMMENT 'Имя/псевдоним пользователя, отображаемое другим пользователям системы',
  `email` varchar(60) DEFAULT NULL,
  `phone` varchar(19) DEFAULT NULL,
  `password` varchar(100) NOT NULL COMMENT 'Пароль пользователя, хранимый в шифрованном виде.',
  `role` enum('ADMIN','USER','MODERATOR') NOT NULL DEFAULT 'USER' COMMENT 'Роль пользователя из таблицы roles (ADMIN, USER, MODERATOR)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='таблица для хранения всех пользователей системы';

--
-- Индексы таблицы `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email_UNIQUE` (`email`);


--
-- AUTO_INCREMENT для таблицы `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Уникальный идентификатор пользователя', AUTO_INCREMENT=1;
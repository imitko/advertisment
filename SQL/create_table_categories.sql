--
-- Структура таблицы `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `category` varchar(120) NOT NULL COMMENT 'Название раздела'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Разделы каталога объявлений.';

--
-- Дамп данных таблицы `categories`
--

INSERT INTO `categories` (`id`, `category`) VALUES
(1, 'Техника'),
(2, 'Спорт'),
(3, 'Животные'),
(4, 'Транспорт'),
(5, 'Мода'),
(6, 'Детский мир'),
(7, 'Хобби'),
(8, 'Стройка и ремонт'),
(9, 'Книги'),
(10, 'Сад и огород'),
(11, 'Запчасти');


--
-- Индексы таблицы `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для таблицы `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

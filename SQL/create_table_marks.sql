--
-- Структура таблицы `marks`
--

CREATE TABLE `marks` (
  `receipent_id` int(11) NOT NULL COMMENT 'id пользователя, которому ставят оценку',
  `sender_id` int(11) NOT NULL COMMENT 'Id пользователя, который ставит оценку.',
  `mark` tinyint(5) DEFAULT NULL COMMENT 'оценка от 1 до 5'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Таблица с оценками которые пользователи выставляют друг другу, если пользователь выставляет другому пользователю оценку повторно, она перезаписывается.';

--
-- Индексы таблицы `marks`
--
ALTER TABLE `marks`
  ADD PRIMARY KEY (`receipent_id`,`sender_id`),
  ADD KEY `fk_marks_user1_idx` (`receipent_id`),
  ADD KEY `fk_marks_user1_idx1` (`sender_id`);

--
-- Ограничения внешнего ключа таблицы `marks`
--
ALTER TABLE `marks`
  ADD CONSTRAINT `fk_marks_sender_user` FOREIGN KEY (`sender_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_marks_user_receipent` FOREIGN KEY (`receipent_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;



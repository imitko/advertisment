--
-- Структура таблицы `message`
--

CREATE TABLE `message` (
  `id` int(11) NOT NULL,
  `sender_id` int(11) DEFAULT NULL COMMENT 'Id пользователя-отправителя',
  `receiver_id` int(11) DEFAULT NULL COMMENT 'Id пользователя-получателя',
  `message_text` varchar(2000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `advertisment_id` int(11) DEFAULT NULL COMMENT 'Id объявления, по которому ведется переписка',
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Сообщения, которые пользователи отправляют друг другу по конкретному объявлению. Если объявление удалено пользователь видит сообщение, но вместо объявления отображается что оно в архиве.';

--
-- Индексы таблицы `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_message_user1_idx1` (`receiver_id`),
  ADD KEY `fk_message_sender_user_idx` (`sender_id`),
  ADD KEY `fk_message_advertisment1_idx` (`advertisment_id`);

--
-- AUTO_INCREMENT для таблицы `message`
--
ALTER TABLE `message`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Ограничения внешнего ключа таблицы `message`
--
ALTER TABLE `message`
  ADD CONSTRAINT `fk_message_advertisment` FOREIGN KEY (`advertisment_id`) REFERENCES `advertisment` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_message_receiver_user` FOREIGN KEY (`receiver_id`) REFERENCES `user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_message_sender_user` FOREIGN KEY (`sender_id`) REFERENCES `user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

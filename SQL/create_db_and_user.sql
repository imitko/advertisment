CREATE DATABASE Advertisments;
CREATE USER 'advuser'@'localhost' IDENTIFIED BY '123';
GRANT SELECT, INSERT, UPDATE, DELETE ON advertisments.advertisment TO 'advuser'@'localhost';
GRANT SELECT, INSERT, UPDATE, DELETE ON advertisments.user TO 'advuser'@'localhost';
GRANT SELECT, INSERT, UPDATE, DELETE ON advertisments.images TO 'advuser'@'localhost';
GRANT SELECT, INSERT, UPDATE, DELETE ON advertisments.categories TO 'advuser'@'localhost';
GRANT SELECT, INSERT, UPDATE, DELETE ON advertisments.marks TO 'advuser'@'localhost';
GRANT SELECT, INSERT, UPDATE, DELETE ON advertisments.message TO 'advuser'@'localhost';
FLUSH PRIVILEGES;
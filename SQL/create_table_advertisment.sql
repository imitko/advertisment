--
-- Структура таблицы `advertisment`
--

CREATE TABLE `advertisment` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL COMMENT 'Id пользователя, подавшего объявление',
  `category_id` int(11) NOT NULL COMMENT 'Id категории, к которой принадлежит объявление',
  `title` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Заголовок объявления',
  `text` varchar(5000) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'текст объявления. Не более 5000 символов',
  `price` decimal(10,0) UNSIGNED DEFAULT NULL COMMENT 'цена товара или услуги',
  `start_date` date NOT NULL COMMENT 'Дата публикации объявления',
  `end_date` date DEFAULT NULL COMMENT 'дата окончания размещения объявления,\nпо умолчанию устанавливается +1 месяц к start_date, но если пользователь снимает объявление сам, устанавливается датой снятия\nКак только end_date <= сегодняшней дате, объявление переносится в таблицу archiveadv, а связанные с ним картинки - в таблицу archiveimages',
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `is_moderated` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AVG_ROW_LENGTH=1000 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Таблица объявления - для размещения всех активных объявлений всех пользователей системы по всем категориям.';

--
-- Индексы таблицы `advertisment`
--
ALTER TABLE `advertisment`
  ADD PRIMARY KEY (`id`,`category_id`,`user_id`),
  ADD KEY `fk_advertisment_categories_idx` (`category_id`),
  ADD KEY `fk_advertisment_user_idx` (`user_id`),
  ADD KEY `price` (`price`) COMMENT 'Индекс для поиска объявления по цене';

--
-- Ограничения внешнего ключа таблицы `advertisment`
--
ALTER TABLE `advertisment`
  ADD CONSTRAINT `fk_advertisment_categories` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_advertisment_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- AUTO_INCREMENT для таблицы `advertisment`
--
ALTER TABLE `advertisment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
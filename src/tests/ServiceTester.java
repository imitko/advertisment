package tests;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import com.epam.advertisments.service.PasswordChecker;


/**
 * Класс для тестирования вспомогательных методов для работы логики приложения.
 * @author Irina Mitsko
 */
public class ServiceTester {


    /**
     * Подготовка данных для тестирования валидации цены.
     * @return двумерный массив с исходными данными: строка в виде
     * вводимого пользователем пароля и строка в шифрованом виде.
     */
    @DataProvider(name = "passwordCheck")
    public Object[][] priceProvider() {
        return new Object[][] {{"222222", "e3ceb5881a0a1fdaad01296d7554868d"},
            {"123456", "e10adc3949ba59abbe56e057f20f883e"},
            {"admin1", "e00cf25ad42683b3df678c61f42c6bda"},
            {"moderator123", "c24717a136791ac824c311d04b0c79cf"}};
    }

    /**
     * Тестирование корректности проверки совпадения паролей.
     * @param input - пароль, вводимый пользователем
     * @param encripted - шифрованный пароль
     */
    @Test(dataProvider = "passwordCheck")
    public void priceValidateTest(final String input,
            final String encripted) {
        Assert.assertEquals(encripted,
                PasswordChecker.hashPassword(input));
    }

}

package tests;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import com.epam.advertisments.validator.EmailValidator;
import com.epam.advertisments.validator.PhoneValidator;
import com.epam.advertisments.validator.PriceValidator;

/**
 * Класс проверяет работу методов-валидаторов введенных в формы данных.
 * @author Irina Mitsko
 */
public class FormsTester {
    /**
     * Ссылка на поле типа PhoneValidator для доступа
     *  к методу валидации телефона.
     */
    private PhoneValidator phoneValidator;
    /**
     * Ссылка на поле типа EmailValidator для доступа
     *  к методу валидации емейла.
     */
    private EmailValidator emailValidator;

    /**
     * Ссылка на поле типа PriceValidator для доступа
     *  к методу валидации цены.
     */
    private PriceValidator priceValidator;

    /**
     * Инициализация переменных для тестов.
     */
    @BeforeClass
    public void initData() {
        emailValidator = new EmailValidator();
        phoneValidator = new PhoneValidator();
        priceValidator = new PriceValidator();
    }

    /**
     * Подготовка данных для тестирования валидации емейла.
     * @return двумерный массив с исходными данными: строка, содержащая
     *  email и ожидаемый результат теста.
     */
    @DataProvider(name = "emailTest")
    public Object[][] emailProvider() {
        return new Object[][] {{"ira@yandex.ru", true}, {"@@cghgs@tit.by", false},
                {"ew44450))@yandex.ru", false}, {"123^^^@dnail.com", false},
                {"1@sag-cd.com", true}, {"avv@tut.by.by", false},
                {"alef+2q@gmail", false}, {"ert@sdfsdf=dfg.com", false},
                {"rewd1@gmail.by", true}};
    }

    /**
     * Подготовка данных для тестирования валидации цены.
     * @return двумерный массив с исходными данными: строка, содержащая
     *  цену и ожидаемый результат теста.
     */
    @DataProvider(name = "priceTest")
    public Object[][] priceProvider() {
        return new Object[][] {{"-100", false}, {"dsssd12", false},
            {"%e98+", false}, {"100.65", true},
            {"345", true}, {"_.65", false}, {"0.50", true},
            {"234 434", false}, {"23342423424", true}};
    }

    /**
     * Тестирования валидации цены.
     * @param price - строка с введенным значением цены
     * @param result - true если цена валидна
     */
    @Test(dataProvider = "priceTest")
    public void priceValidateTest(final String price,
            final boolean result) {
        Assert.assertEquals(result,
                priceValidator.validatePrice(price));
    }

    /**
     * Тестирование валидации емейла.
     * @param email - строка, содержащая емейл
     */
    @Test(dataProvider = "emailTest")
    public void mailValidateTest(final String email,
            final boolean result) {
        Assert.assertEquals(result,
                emailValidator.validateEmail(email));
    }

    /**
     * Метод тестирует корректность валидации поля "телефон" на положительный
     * результат.
     */
    @Test
    public void phoneValidateTrueTest() {
        final String phone = "+374 34 3444444";
        boolean actual = phoneValidator.validatePhone(phone);
        Assert.assertEquals(actual, true);
    }

    /**
     * Метод тестирует корректность валидации поля "телефон" на отрицательный
     * результат.
     */
    @Test
    public void phoneValidateFalseTest() {
        final String phone = "+374ds3444444";
        boolean actual = phoneValidator.validatePhone(phone);
        Assert.assertEquals(actual, false);
    }
}

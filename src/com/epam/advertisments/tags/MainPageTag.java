package com.epam.advertisments.tags;

import java.io.IOException;
import java.util.List;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import com.epam.advertisments.dao.ImageDAO;
import com.epam.advertisments.model.Advertisment;
import com.epam.advertisments.model.Image;
import com.epam.advertisments.resource.ConfigurationManager;
import com.epam.advertisments.resource.ElementManager;
/**
 * Класс с пользовательским тегом для формирования списка последних
 * размещенных объявлений на титульной странице.
 * @author Irina Mitsko
 *
 */
@SuppressWarnings("serial")
public class MainPageTag extends TagSupport {
    /**
     * Логгер для класса.
     */
    protected static final Logger LOGGER
        = (Logger) LogManager.getLogger(MainPageTag.class);
    
    /**
     * Метод вызывается со страницы main.jsp когда
     *  обнаруживается начальный элемент тега.
     */
    @Override
    public int doStartTag() throws JspException {
        final ElementManager manager = ElementManager.INSTANCE;
        final List<Advertisment> advs = (List<Advertisment>) pageContext.getSession().getAttribute("lastAdvs");
        final ImageDAO imgDao = (ImageDAO) pageContext.getSession().getAttribute("imgdao");
        if (advs != null) {
            for (final Advertisment advertisment : advs) {
                final List<Image> images = imgDao.findAll(advertisment.getId());
                try {
                    pageContext.getOut().write("<table><tr>");
                    if (images.size() != 0) {
                        pageContext.getOut().write("<td><img height=\"200\""
                                + " width=\"200\" src=\"data:image/jpeg;base64,"
                                + images.get(0).getEncoding() + "\" id='image0'"
                                        + " style = \"zoom: 1\" /></td>");
                    } else {
                        pageContext.getOut()
                        .write("<td><img height=\"200\""
                        + " width=\"200\" src=\"" + pageContext
                        .getServletContext()
                        .getContextPath() + "/images/no_product.png"
                        + "\"/></td>");
                    }
                    pageContext.getOut()
                            .write("<td style=\"width: 100%;\"><a href =\""
                            + pageContext.getServletContext().getContextPath()
                            + ConfigurationManager
                            .getProperty("path.page.viewlist")
                            + advertisment.getId() + "\" >");
                    pageContext.getOut().write("<h3>" + advertisment.getTitle()
                    + "</h3></a><br>");
                    pageContext.getOut().write(
                            advertisment.getText() + "<br>"
                    + manager.getString("adv.price")
                            + advertisment.getPrice());
                    pageContext.getOut().write("</td><tr></table>");

                } catch (IOException e) {
                    LOGGER.error("Ошибка ввода-вывода " + e);
                    final StackTraceElement[] trace = e.getStackTrace();
                    for (final StackTraceElement el : trace) {
                        LOGGER.error("\n " + el);
                    }
                }
            }
        }
        return SKIP_BODY;
    }
}

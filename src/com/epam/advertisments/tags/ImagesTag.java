package com.epam.advertisments.tags;

import java.io.IOException;
import java.util.List;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import com.epam.advertisments.model.Advertisment;
import com.epam.advertisments.model.Image;
import com.epam.advertisments.resource.ConfigurationManager;
import com.epam.advertisments.resource.ElementManager;
/**
 * Класс с пользовательским тегом для формирования страницы
 * загрузки/просмотра картинок к объявлению.
 * @author Irina Mitsko
 */
@SuppressWarnings("serial")
public class ImagesTag extends TagSupport {
    /**
     * Логгер для класса.
     */
    protected static final Logger LOGGER = (Logger) LogManager
            .getLogger(ImagesTag.class);
    /**
     * Возможное количество изображений, которое может загрузить
     * пользователь.
     */
    private static final int NUM_OF_IMAGES = 3;
    /**
     * Аттрибут объявление.
     */
    private Advertisment adv1;

    /**
     * Геттер для получения значения аттрибута.
     * @return adv1 - объект "Объявление"
     */
    public Advertisment getAdv1() {
        return adv1;
    }

    /**
     * Сеттер для усрановки значения аттрибута.
     * @param newAdv1 - ноый объект "Объявление"
     */
    public void setAdv1(final Advertisment newAdv1) {
        this.adv1 = newAdv1;
    }

    /**
     * Метод вызывается со страницы загрузки или просмотра/редактирования
     * изображений к объявлению, когда найден начальный тег.
     */
    @Override
    public int doStartTag() throws JspException {
        final ElementManager manager = ElementManager.INSTANCE;
        try {
            if (adv1 != null) {
                final List<Image> images
                = (List<Image>) pageContext.getRequest().getAttribute("images");
                //final ImageDAO dao = new ImageDAO();
                //final List<Image> images = dao.findAll(adv1.getId());
                final Image[] arr = new Image[NUM_OF_IMAGES];
                images.toArray(arr);
                for (int i = 0; i < arr.length; i++) {
                    if (arr[i] != null) {
                        pageContext.getOut().write(
                        "<img height=\"200\" src=\"data:image/jpeg;base64,"
                        + arr[i].getEncoding() + "\" />");
                        pageContext.getOut()
                        .write("<a href=\"" + pageContext.getServletContext()
                        .getContextPath()
                        + ConfigurationManager.getProperty("path.page.delimg")
                        + ConfigurationManager.getProperty("path.page.photoid")
                        + arr[i].getId()
                        + ConfigurationManager.getProperty("path.page.advid")
                        + adv1.getId()
                        + "\" onClick=\"return window.confirm('"
                        + manager.getString("img.confirmdel")
                        + "');\">" + manager.getString("link.delete")
                        + "</a><br><p></p>");
                    } else {
                        pageContext.getOut().write("<label>"
                    + manager.getString("img.add")
                                + "</label><input type=\"file\""
                                + " name=\"file\"  />");
                    }
                }
                if (images.size() < NUM_OF_IMAGES) {
                    pageContext.getOut().write("<p></p>");
                    if (pageContext.getSession().getAttribute("role").
                            equals("USER")) {
                        pageContext.getOut()
                                .write("<input type=\"submit\" value=\""
                        + manager.getString("img.upload")
                                        + "\" /> <a href =\""
                        + pageContext.getServletContext().getContextPath()
                        + ConfigurationManager.getProperty("path.page.advs")
                        + "\">"
                        + manager.getString("img.tomyads") + "</a>");
                    } else if (pageContext.getSession().getAttribute("role")
                            .equals("MODERATOR")) {
                        pageContext.getOut()
                                .write("<input type=\"submit\" value=\""
                        + manager.getString("img.upload")
                                        + "\" /> <a href =\""
                        + pageContext.getServletContext().getContextPath()
                        + ConfigurationManager
                        .getProperty("path.page.moderation") + "\">"
                        + manager.getString("img.tomoderation") + "</a>");
                    } else {
                        pageContext.getOut()
                                .write("<input type=\"submit\" value=\""
                        + manager.getString("img.upload")
                                + "\" /> <a href =\""
                        + pageContext.getServletContext().getContextPath()
                                + ConfigurationManager
                                .getProperty("path.page.advs")
                                + ConfigurationManager
                                .getProperty("path.page.userid")
                                + adv1.getUserId()
                                + "\">" + manager.getString("img.toallads")
                                + "</a>");
                    }

                } else {
                    pageContext.getOut()
                            .write("<a href = \""
                            + pageContext.getServletContext().getContextPath()
                            + ConfigurationManager.getProperty("path.page.advs")
                            + "\">"
                            + manager.getString("img.skip") + "</a>");
                }

            }
        } catch (IOException e) {
            LOGGER.error("Ошибка ввода-вывода " + e);
            final StackTraceElement[] trace = e.getStackTrace();
            for (final StackTraceElement el : trace) {
                LOGGER.error("\n " + el);
            }
        }
        return SKIP_BODY;

    }

}

package com.epam.advertisments.tags;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.epam.advertisments.dao.AdvertismentDAO;
import com.epam.advertisments.dao.CategoryDAO;
import com.epam.advertisments.model.Advertisment;
import com.epam.advertisments.model.Category;
import com.epam.advertisments.model.Message;
import com.epam.advertisments.resource.ConfigurationManager;
import com.epam.advertisments.resource.ElementManager;
/**
 * 
 * @author Irina Mitsko
 *
 */
@SuppressWarnings("serial")
public class ManageCategoriesTag extends TagSupport {
    /**
     * Логгер для класса.
     */
    protected static final Logger LOGGER
        = (Logger) LogManager.getLogger(ManageCategoriesTag.class);
    /**
     * Метод вызывается со страницы редактирования категорий
     * администратором (categoriesmanage.jsp) когда
     * обнаруживается начальный элемент тега.
     */
    @Override
    public int doStartTag() throws JspException {
        Map<Category, List<Advertisment>> allCategories
        = (Map<Category, List<Advertisment>>) pageContext.getRequest()
        .getAttribute("categoriesMap");
        final ElementManager manager = ElementManager.INSTANCE;
        try {
            pageContext.getOut().write("<table>");
            pageContext.getOut()
                    .write("<tr><th>" + manager.getString("table.section")
                    + "</th><th>"
                            + manager.getString("table.active") + "</th><th>"
                    + manager.getString("table.management")
                            + "</th></tr>");
            for (final Entry<Category, List<Advertisment>> entry
                    : allCategories.entrySet()) {
                final Category category = entry.getKey();
                final List<Advertisment> advertisments = entry.getValue();
                pageContext.getRequest().setAttribute("category", category);
                pageContext.getOut().write("<tr>");
                pageContext.getOut()
                        .write("<td><a href =\""
                + pageContext.getServletContext().getContextPath()
                                + ConfigurationManager
                                .getProperty("path.page.categorycom")
                                + ConfigurationManager
                                .getProperty("path.page.catid")
                                + category.getId() + "\" >" + category.getName()
                                + "</a></td>");
                pageContext.getOut().write("<td>" + advertisments.size()
                    + "</td>");
                pageContext.getOut().write("<td>");
                pageContext.getOut()
                        .write("<form name=\"editcategory\" method=\"POST\" action=\""
                              + pageContext.getServletContext().getContextPath()
                              + ConfigurationManager.getProperty("path.page.controller")
                              + "\"> ");
                pageContext.getOut().write("<input type=\"hidden\""
                        + " name=\"command\""
                        + " value=\"newcategory\" />");
                pageContext.getOut().write("<input type=\"hidden\""
                        + " name=\"categoryid\" value=\""
                        + category.getId()
                        + "\" class=\"form-control input-sm\">");

                pageContext.getOut()
                        .write("<input type=\"submit\" value=\""
                + manager.getString("label.edit") + "\"/>");
                pageContext.getOut().write("</form>");
                pageContext.getOut().write("<form name=\"deletecategory\""
                      + " method=\"POST\" action=\""
                      + pageContext.getServletContext().getContextPath()
                      + ConfigurationManager.getProperty("path.page.controller")
                      + "\" onClick=\"return window.confirm('"
                      + manager.getString("warning.delsection") + "');\"> ");
                pageContext.getOut().write("<input type=\"hidden\""
                      + " name=\"command\" value=\"deletecategory\" />");
                pageContext.getOut().write("<input type=\"hidden\""
                        + " name=\"categoryid\""
                        + " value=\"" + category.getId()
                        + "\" class=\"form-control input-sm\">");
                pageContext.getOut().write("<input type=\"submit\" value=\""
                        + manager.getString("label.del") + "\"/>");
                pageContext.getOut().write("</form>");
                pageContext.getOut().write("</td>");
                pageContext.getOut().write("</tr>");

            }
            pageContext.getOut().write("</table>");
        } catch (IOException e) {
            LOGGER.error("Ошибка ввода-вывода " + e);
            final StackTraceElement[] trace = e.getStackTrace();
            for (final StackTraceElement el : trace) {
                LOGGER.error("\n " + el);
            }
        }

        return SKIP_BODY;

    }

}

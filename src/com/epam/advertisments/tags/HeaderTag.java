package com.epam.advertisments.tags;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import com.epam.advertisments.model.User;
import com.epam.advertisments.resource.ConfigurationManager;
import com.epam.advertisments.resource.ElementManager;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map.Entry;

/**
 * Класс с пользовательским тегом для вывода пользовательского
 * меню в правом верхнем углу каждой страницы.
 * @author Irina Mitsko
 *
 */
@SuppressWarnings("serial")
public class HeaderTag extends TagSupport {

    /**
     * Аттрибут для роли пользователя.
     */
    private String role;

    /**
     * Коллекция элементов для пользовательского меню.
     */
    private HashMap<String, String> menu;

    /**
     * Сеттер для аттрибута role.
     * @param newRole - - новое значение аттрибута
     */
    public void setRole(final String newRole) {
        this.role = newRole;
    }

    /**
     * Метод вызывается со страницы header.jsp (которая вложена в
     * верхнюю часть каждой страницы), когда
     *  обнаруживается начальный элемент тега.
     */
    @Override
    public int doStartTag() throws JspException {
        final User loggedUser
            = ((User) pageContext.getSession().getAttribute("loggeduser"));
        final ElementManager manager = ElementManager.INSTANCE;
        try {
            String to = null;
            menu = new HashMap<String, String>();
            menu.put(manager.getString("usermenu.registration"),
                    ConfigurationManager.getProperty("path.page.registration"));
            menu.put(manager.getString("usermenu.enter"),
                    ConfigurationManager.getProperty("path.page.auth"));
            if ("user".equalsIgnoreCase(role)) {
                to = manager.getString("usermenu.greeting")
                        + loggedUser.getName()
                + "!";
                menu.clear();
                menu.put(manager.getString("usermenu.personal"),
                        ConfigurationManager
                        .getProperty("path.page.registration"));
                menu.put(manager.getString("usermenu.myadverts"),
                        ConfigurationManager.getProperty("path.page.advs"));
                menu.put(manager.getString("usermenu.messages"),
                        ConfigurationManager.getProperty("path.page.mail"));
                menu.put(manager.getString("usermenu.logout"),
                        ConfigurationManager.getProperty("path.page.logout"));
            } else if ("admin".equalsIgnoreCase(role)) {
                to = manager.getString("usermenu.greeting")
                        + loggedUser.getName()
                + "!";
                menu.clear();
                menu.put(manager.getString("usermenu.personal"),
                        ConfigurationManager
                        .getProperty("path.page.registration"));
                menu.put(manager.getString("usermenu.users"),
                        ConfigurationManager
                        .getProperty("path.page.manageusers"));
                menu.put(manager.getString("usermenu.categories"),
                        ConfigurationManager
                        .getProperty("path.page.managecategories"));
                menu.put(manager.getString("usermenu.logout"),
                        ConfigurationManager.getProperty("path.page.logout"));
            } else if ("moderator".equalsIgnoreCase(role)) {
                to = manager.getString("usermenu.greeting")
                        + loggedUser.getName() + "!";
                menu.clear();
                menu.put(manager.getString("usermenu.personal"),
                        ConfigurationManager
                        .getProperty("path.page.registration"));
                menu.put(manager.getString("usermenu.newadvs"),
                        ConfigurationManager
                        .getProperty("path.page.moderation"));
                menu.put(manager.getString("usermenu.logout"),
                        ConfigurationManager.getProperty("path.page.logout"));
            } else {
                to = manager.getString("usermenu.greetingguest");
            }
            pageContext.getOut().write(to + "<br>");
            for (Entry<String, String> entry : menu.entrySet()) {
                final String link = entry.getKey();
                final String value = entry.getValue();
                pageContext.getOut().write("<a href=\""
                + pageContext.getServletContext().getContextPath() + value
                        + "\"/>" + link + "</a><br>");
            }
        } catch (IOException e) {
            throw new JspException(e.getMessage());
        }
        return SKIP_BODY;
    }
}

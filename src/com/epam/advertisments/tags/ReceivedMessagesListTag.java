package com.epam.advertisments.tags;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.epam.advertisments.dao.AdvertismentDAO;
import com.epam.advertisments.dao.UserDAO;
import com.epam.advertisments.model.Advertisment;
import com.epam.advertisments.model.Message;
import com.epam.advertisments.model.User;
import com.epam.advertisments.resource.ConfigurationManager;
import com.epam.advertisments.resource.ElementManager;

/**
 * Класс с пользовательским тегом для формирования списка
 * полученных сообщений.
 * @author Irina Mitsko
 *
 */
@SuppressWarnings("serial")
public class ReceivedMessagesListTag extends TagSupport {
    /**
     * Логгер для класса.
     */
    protected static final Logger LOGGER
        = (Logger) LogManager.getLogger(ReceivedMessagesListTag.class);
    /**
     * Ссылка на объект ElementManager для доступа к языковым константам.
     */
    private ElementManager manager = ElementManager.INSTANCE;

    /**
     * Метод вызывается со страницы showmessages.jsp когда
     *  обнаруживается начальный элемент тега.
     */
    @Override
    public int doStartTag() throws JspException {
        if (pageContext.getSession().getAttribute("receivedmap") != null) {
            final Map<Message, List<String>> messagesMap
                = (Map<Message, List<String>>) pageContext.getSession()
                .getAttribute("receivedmap");
            for (Entry<Message, List<String>> entry : messagesMap.entrySet()) {
                final Message message = entry.getKey();
                final List<String> details = entry.getValue();
                String senderName = manager.getString("message.usernotfound");
                String advTitle = manager.getString("message.notfoundtitle");
                if(details.get(1) != null) {
                    advTitle = details.get(1);
                } 
                if(details.get(0) != null) {
                    senderName = details.get(0);
                }
                try {
                    pageContext.getOut()
                    .write(((com.epam.advertisments.model.Message) message)
                            .getDate() + " <i> "
                            + manager.getString("message.from")
                            + senderName + "</i>");
                    pageContext.getOut().write("<br>"
                            + manager.getString("message.adv") + advTitle);
                    pageContext.getOut()
                            .write("<br><a href=\""
                            + pageContext.getServletContext().getContextPath()
                            + ConfigurationManager.getProperty("path.page.read")
                            + ConfigurationManager
                            .getProperty("path.page.messageid")
                            + ((com.epam.advertisments.model.Message) message)
                            .getId() + "\">" + manager.getString("link.read")
                            + "</a> <hr>");
                } catch (IOException e) {
                    LOGGER.error("Ошибка ввода-вывода " + e);
                    final StackTraceElement[] trace = e.getStackTrace();
                    for (final StackTraceElement el : trace) {
                        LOGGER.error("\n " + el);
                    }
                }
            }
        }
        return SKIP_BODY;
    }
}

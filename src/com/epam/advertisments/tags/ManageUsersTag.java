package com.epam.advertisments.tags;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import com.epam.advertisments.model.Advertisment;
import com.epam.advertisments.model.User;
import com.epam.advertisments.resource.ConfigurationManager;
import com.epam.advertisments.resource.ElementManager;
/**
 * Класс с пользовательским тегом для формирования списка всех
 * пользователей для редактирования администратором.
 * @author Irina Mitsko
 *
 */
@SuppressWarnings("serial")
public class ManageUsersTag extends TagSupport {
    /**
     * Логгер для класса.
     */
    protected static final Logger LOGGER
        = (Logger) LogManager.getLogger(ManageUsersTag.class);
    /**
     * Метод вызывается со страницы usersmanage.jsp когда
     *  обнаруживается начальный элемент тега.
     */
    @Override
    public int doStartTag() throws JspException {
        final ElementManager manager = ElementManager.INSTANCE;
        try {
            pageContext.getOut()
                    .write("<form name=\"search\" method=\"POST\" action=\""
                            + pageContext.getServletContext().getContextPath()
                            + ConfigurationManager
                            .getProperty("path.page.controller")
                            + "\"> ");
            pageContext.getOut().write("<input type=\"hidden\""
                    + " name=\"command\" value=\"manageusers\" />");
            pageContext.getOut().write("<label>"
                    + manager.getString("label.search") + "</label>");
            pageContext.getOut().write("<input type=\"text\""
                    + " name=\"email\" value=\"\" />");
            pageContext.getOut().write("<input type=\"submit\" value=\""
                    + manager.getString("button.search") + "\"/>");
            pageContext.getOut().write("</form>");

            pageContext.getOut().write("<table>");
            pageContext.getOut()
                    .write("<tr><th>" + manager.getString("label.name")
                    + "</th><th>" + manager.getString("label.email")
                           + "</th><th>" + manager.getString("label.phone")
                           + "</th><th>"
                           + manager.getString("table.role") + "</th>" + "<th>"
                           + manager.getString("table.count")
                           + "</th><th>"
                           + manager.getString("table.management")
                           + "</th></tr>");
            final Map <User, List<Advertisment>> map
            = (Map<User, List<Advertisment>>) pageContext.getRequest().getAttribute("usersmap");
            for (Entry<User, List<Advertisment>> entry : map.entrySet()) {
                final User user = entry.getKey();
                final List<Advertisment> userAdvs = entry.getValue();
                pageContext.getOut().write("<tr>");
                pageContext.getOut().write("<td>" + user.getName() + "</td>");
                pageContext.getOut().write("<td>" + user.getEmail() + "</td>");
                pageContext.getOut().write("<td>" + user.getPhone() + "</td>");
                pageContext.getOut().write("<td>" + user.getRole() + "</td>");
                pageContext.getOut()
                        .write("<td><a href=\""
                + pageContext.getServletContext()
                        .getContextPath()
                        + ConfigurationManager.getProperty("path.page.advs")
                        + ConfigurationManager.getProperty("path.page.userid")
                        + user.getId() + "\"/>"
                        + userAdvs.size() + "</a> </td>");
                pageContext.getOut().write("<td>");
                pageContext.getOut()
                        .write("<form name=\"editcategory\" method=\"POST\""
                                + " action=\""
                                + pageContext.getServletContext()
                                .getContextPath()
                                + ConfigurationManager
                                .getProperty("path.page.controller")
                                + "\"> ");
                pageContext.getOut().write("<input type=\"hidden\""
                        + " name=\"command\""
                        + " value=\"edituser\" />");
                pageContext.getOut().write("<input type=\"hidden\""
                        + " name=\"userId\""
                        + " value=\"" + user.getId()
                        + "\" class=\"form-control input-sm\">");
                pageContext.getOut()
                        .write("<input type=\"submit\" value=\""
                + manager.getString("button.edit")
                        + "\"/>");
                pageContext.getOut().write("</form>");
                pageContext.getOut().write("<form name=\"delete\""
                        + " method=\"POST\" action=\""
                        + pageContext.getServletContext().getContextPath()
                        + ConfigurationManager
                        .getProperty("path.page.controller")
                        + "\" onClick=\"return window.confirm('"
                        + manager.getString("warning.deluser") + "');\"> ");
                pageContext.getOut().write("<input type=\"hidden\""
                        + " name=\"command\""
                        + " value=\"deleteuser\" />");
                pageContext.getOut().write("<input type=\"hidden\""
                        + " name=\"userId\""
                        + " value=\"" + user.getId()
                        + "\" class=\"form-control input-sm\">");
                pageContext.getOut()
                        .write("<input type=\"submit\" value=\""
                + manager.getString("button.delete") + "\"/>");
                pageContext.getOut().write("</form>");
                pageContext.getOut().write("</td>");
                pageContext.getOut().write("</tr>");

            }
            pageContext.getOut().write("</table>");
        } catch (IOException e) {
            LOGGER.error("Ошибка ввода-вывода " + e);
            final StackTraceElement[] trace = e.getStackTrace();
            for (final StackTraceElement el : trace) {
                LOGGER.error("\n " + el);
            }
        }
        return SKIP_BODY;

    }
}

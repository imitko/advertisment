package com.epam.advertisments.tags;

import java.io.IOException;
import java.util.List;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.epam.advertisments.dao.AdvertismentDAO;
import com.epam.advertisments.dao.CategoryDAO;
import com.epam.advertisments.model.Advertisment;
import com.epam.advertisments.model.Category;
import com.epam.advertisments.resource.ConfigurationManager;
import com.epam.advertisments.resource.ElementManager;
/**
 * Класс с пользовательским тегом для формирования cписка
 * объявлений, размещенных одним пользователем.
 * @author Irina Mitsko
 *
 */
@SuppressWarnings("serial")
public class UserAdvertismentsTag extends TagSupport {
    /**
     * Логгер для класса.
     */
    protected static final Logger LOGGER
    = (Logger) LogManager.getLogger(UserAdvertismentsTag.class);
    /**
     * Ссылка на объект ElementManager для доступа к языковым константам.
     */
    private ElementManager manager = ElementManager.INSTANCE;
    /**
     * Аттрибут для роли пользователя.
     */
    private String role;

    /**
     * Геттер для аттрибута role.
     * @return role
     */
    public String getRole() {
        return role;
    }

    /**
     * Сеттер для аттрибута role.
     * @param newRole - новое значение аттрибута
     */
    public void setRole(final String newRole) {
        this.role = newRole;
    }

    /**
     * Метод вызывается со страницы showUserAdv.jsp когда
     *  обнаруживается начальный элемент тега.
     */
    @Override
    public int doStartTag() throws JspException {
        List<Advertisment> advertisments;
        if (role.equals("MODERATOR")) {
            advertisments = (List<Advertisment>) pageContext.getRequest()
                    .getAttribute("formoderation");
        } else {
            advertisments
            = (List<Advertisment>) pageContext.getRequest()
            .getAttribute("listOfAdv");
        }
        try {
            if (advertisments != null) {
                pageContext.getOut()
                        .write("<table><tr><th>"
                + manager.getString("table.adv") + "</th><th>"
                                + manager.getString("table.text") + "</th><th>"
                + manager.getString("table.term")
                                + "</th><th>"
                + manager.getString("table.section") + "</th><th>"
                                + manager.getString("table.management")
                                + "</th></tr>");
                for (final Advertisment advertisment : advertisments) {
                    pageContext.getOut()
                            .write("<td><a href =\""
                    + pageContext.getServletContext().getContextPath()
                             + ConfigurationManager
                             .getProperty("path.page.viewlist")
                             + advertisment.getId() + "\" >"
                             + advertisment.getTitle() + "</a><br>");
                    if (!advertisment.isModerated()) {
                        pageContext.getOut().write("<i>"
                    + manager.getString("table.moderating") + "</i>");
                    } else if (advertisment.isModerated()
                            && !advertisment.isActive()) {
                        pageContext.getOut().write("<i>"
                    + manager.getString("table.exp") + "</i>");
                    } else {
                        pageContext.getOut().write("<i>"
                    + manager.getString("table.valid") + "</i>");
                    }
                    pageContext.getOut().write("</td>");
                    pageContext.getOut().write("<td>" + advertisment.getText()
                    + "</td>");
                    pageContext.getOut()
                            .write("<td>" + advertisment.getStartDate() + " - "
                    + advertisment.getEndDate() + "</td>");
                    CategoryDAO catDao
                        = (CategoryDAO) pageContext.getRequest().getAttribute("catDao");
                    final Category category
                        = catDao.findEntity(advertisment.getCategoryId());
                    pageContext.getOut().write("<td>"
                    + category.getName() + "</td>");
                    pageContext.getOut().write("<td>");

                    pageContext.getOut()
                            .write("<form name=\"editadv\" method=\"POST\""
                                    + " action=\""
                                    + pageContext.getServletContext()
                                    .getContextPath() + ConfigurationManager
                                    .getProperty("path.page.controller")
                                    + "\"> ");
                    pageContext.getOut().write("<input type=\"hidden\""
                            + " name=\"command\"" + " value=\"newadv\" />");
                    pageContext.getOut().write("<input type=\"hidden\""
                            + " name=\"advId\" value=\"" + advertisment.getId()
                            + "\" class=\"form-control input-sm\">");
                    pageContext.getOut()
                            .write("<input type=\"submit\" value=\""
                    + manager.getString("button.edit") + "\"/>");
                    pageContext.getOut().write("</form>");
                    pageContext.getOut().write("<form name=\"deleteadv\""
                            + " method=\"POST\" action=\""
                            + pageContext.getServletContext().getContextPath()
                            + ConfigurationManager
                            .getProperty("path.page.controller")
                            + "\" onClick=\"return window.confirm('"
                            + manager.getString("warning.deladv") + "');\"> ");
                    pageContext.getOut().write("<input type=\"hidden\""
                            + " name=\"command\" value=\"deleteadv\" />");
                    pageContext.getOut().write("<input type=\"hidden\""
                            + " name=\"advId\" value=\"" + advertisment.getId()
                            + "\" class=\"form-control input-sm\">");
                    pageContext.getOut()
                            .write("<input type=\"submit\" value=\""
                    + manager.getString("button.delete") + "\"/>");
                    pageContext.getOut().write("</form>");

                    if ("user".equalsIgnoreCase(role)) {
                        pageContext.getOut()
                                .write("<form name=\"extend\" method=\"POST\""
                                + " action=\""
                                + pageContext.getServletContext()
                                .getContextPath()
                                + ConfigurationManager
                                .getProperty("path.page.controller")
                                + "\" onClick=\"return window.confirm('"
                                + manager.getString("warning.extandadv")
                                + "');\">");
                        pageContext.getOut().write("<input type=\"hidden\""
                                + " name=\"command\" value=\"extendadv\" />");
                        pageContext.getOut().write("<input type=\"hidden\""
                                + " name=\"advId\" value=\""
                                + advertisment.getId() + "\""
                                        + " class=\"form-control input-sm\">");
                        pageContext.getOut()
                                .write("<input type=\"submit\" value=\""
                        + manager.getString("button.extand") + "\"/>");
                        pageContext.getOut().write("</form>");
                    } else if ("moderator".equalsIgnoreCase(role)) {

                        pageContext.getOut()
                                .write("<form name=\"activation\""
                                        + " method=\"POST\" action=\""
                                        + pageContext.getServletContext()
                                        .getContextPath() + ConfigurationManager
                                        .getProperty("path.page.controller")
                                        + "\"> ");
                        pageContext.getOut().write("<input type=\"hidden\""
                                + " name=\"command\" value=\"activation\" />");
                        pageContext.getOut().write("<input type=\"hidden\""
                                + " name=\"advId\" value=\""
                                + advertisment.getId()
                                + "\" class=\"form-control input-sm\">");
                        pageContext.getOut().write(
                                "<input type=\"submit\" value=\""
                        + manager.getString("button.activate") + "\"/>");
                        pageContext.getOut().write("</form>");
                    }
                    pageContext.getOut().write("</td></tr>");
                }
                pageContext.getOut().write("</table>");
            }
        } catch (IOException e) {
            LOGGER.error("Ошибка ввода-вывода " + e);
            final StackTraceElement[] trace = e.getStackTrace();
            for (final StackTraceElement el : trace) {
                LOGGER.error("\n " + el);
            }
        }
        return SKIP_BODY;
    }
}

package com.epam.advertisments.tags;

import java.io.IOException;
import java.util.List;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import com.epam.advertisments.model.Advertisment;
import com.epam.advertisments.model.Image;
import com.epam.advertisments.model.User;
import com.epam.advertisments.resource.ConfigurationManager;
import com.epam.advertisments.resource.ElementManager;

/**
 * Класс с пользовательским тегом для формирования страницы объявления.
 * @author Irina Mitsko
 */
@SuppressWarnings("serial")
public class AdverticmentTag extends TagSupport {
    /**
     * Логгер для класса.
     */
    protected static final Logger LOGGER = (Logger) LogManager
            .getLogger(AdverticmentTag.class);
    /**
     * Метод вызывается во время запроса, когда обнаруживается
     * начальный элемент тега на странице advertisment.jsp.
     */
    @Override
    public int doStartTag() throws JspException {
        int loggedUserID = 0;
        Advertisment adv = (Advertisment) pageContext.getRequest()
                .getAttribute("adv");
        final User user = (User) pageContext.getRequest().getAttribute("user");
        final List<Image> images = (List<Image>) pageContext.getRequest()
                .getAttribute("images");
        final double rating = (double) pageContext.getRequest()
                .getAttribute("rating");
        final ElementManager manager = ElementManager.INSTANCE;
        if(adv.getId() != 0) {
            try {
                pageContext.getOut().write("<h1>" + adv.getTitle()
                + "</h1><hr>");
                pageContext.getOut().write(manager.getString("adv.rate")
                        + rating + "</p>");
                pageContext.getOut().write(
                        "<p class = \"italic\">" + manager.getString("label.start")
                        + ": " + adv.getStartDate() + "</p>");
                for (int i = 0; i < images.size(); i++) {
                    pageContext.getOut()
                    .write("<tr><td><img height=\"200\""
                            + " src=\"data:image/jpeg;base64,"
                            + images.get(i).getEncoding() + "\" id='image" + i
                            + "' style = \"zoom: 1\" /></td>");
                }
                pageContext.getOut().write("<br>" + adv.getText());
                pageContext.getOut().write("<br>" + manager.getString("adv.price")
                + " " + adv.getPrice() + "<hr>");
                pageContext.getOut().write(manager.getString("adv.contacts")
                        + user.getPhone() + ", " + user.getEmail()
                        + ". " + user.getName() + "<p></p>");
                if (pageContext.getSession().getAttribute("loggeduser") != null) {
                    final User loggedInUser = (User) pageContext.getSession()
                            .getAttribute("loggeduser");
                    loggedUserID = loggedInUser.getId();
                }
                if (pageContext.getSession().getAttribute("loggeduser") != null
                        && user.getId() != loggedUserID) {
                    pageContext.getOut().write("<p>"
                        + manager.getString("adv.message") + "</p>");
                    pageContext.getOut().write("<form name=\"message\""
                            + " method=\"POST\" action=\""
                            + pageContext.getServletContext()
                            .getContextPath() + ConfigurationManager
                            .getProperty("path.page.controller")
                            + "\" class=\"form-horizontal\"> ");
                    pageContext.getOut().write("<input type=\"hidden\""
                            + " name=\"command\" value=\"sendmessage\" />");
                    pageContext.getOut().write("<input type=\"text\""
                            + " required oninput=\"currentDate()\""
                            + " name=\"message\" class=\"form-control input-sm\">");
                    pageContext.getOut().write("<input type=\"hidden\""
                            + " name=\"advertisment_id\" value=\"" + adv.getId()
                            + "\" class=\"form-control input-sm\">");
                    pageContext.getOut().write("<input type=\"hidden\""
                            + " name=\"receiver_id\" value=\"" + adv.getUserId()
                            + "\" class=\"form-control input-sm\">");
                    pageContext.getOut().write("<input type=\"hidden\""
                            + " name=\"sender_id\" value=\""
                            + pageContext.getSession().getAttribute("userId")
                            + "\" class=\"form-control input-sm\">");
                    pageContext.getOut().write("<br><input class=\"col-md-7\""
                            + " type=\"submit\" value=\""
                            + manager.getString("label.send") + "\"/>");
                    pageContext.getOut().write("</form><br>");
                    pageContext.getOut()
                            .write("<br><form name=\"raiting\""
                                    + " method=\"POST\" action=\""
                                    + pageContext.getServletContext()
                                    .getContextPath() + ConfigurationManager
                                    .getProperty("path.page.controller") + "\"> ");
                    pageContext.getOut().write("<div id=\"reviewStars-input\">");
                    pageContext.getOut().write("<input type=\"hidden\""
                            + " name=\"command\" value=\"rating\" />");
                    pageContext.getOut().write("<input id=\"star-4\" type=\"radio\""
                            + " name=\"reviewStars\" value=\"5\"/>");
                    pageContext.getOut().write("<label title=\"gorgeous\""
                            + " for=\"star-4\"></label>");
                    pageContext.getOut().write("<input id=\"star-3\" type=\"radio\""
                            + " name=\"reviewStars\" value=\"4\"/>");
                    pageContext.getOut().write("<label title=\"good\""
                            + " for=\"star-3\"></label>");
                    pageContext.getOut().write("<input id=\"star-2\" type=\"radio\""
                            + " name=\"reviewStars\" value=\"3\"/>");
                    pageContext.getOut().write("<label title=\"regular\""
                            + " for=\"star-2\"></label>");
                    pageContext.getOut().write("<input id=\"star-1\" type=\"radio\""
                            + " name=\"reviewStars\" value=\"2\" />");
                    pageContext.getOut().write("<label title=\"poor\""
                            + " for=\"star-1\"></label>");
                    pageContext.getOut().write("<input id=\"star-0\""
                            + " type=\"radio\" name=\"reviewStars\" value=\"1\"/>");
                    pageContext.getOut().write("<label title=\"bad\""
                            + " for=\"star-0\"></label>");
                    pageContext.getOut().write("</div>");
                    pageContext.getOut()
                            .write("<br><input type=\"submit\" value=\""
                    + manager.getString("label.rate") + "\"/>");
                    pageContext.getOut().write("<input type=\"hidden\""
                            + " name=\"sender_id\" value=\""
                            + pageContext.getSession().getAttribute("userId")
                            + "\" class=\"form-control input-sm\">");
                    pageContext.getOut().write("<input type=\"hidden\""
                            + " name=\"receiver_id\" value=\"" + adv.getUserId()
                            + "\" class=\"form-control input-sm\">");
                    pageContext.getOut().write("</form> ");
                } else if (user.getId() == loggedUserID) {
                    pageContext.getOut()
                            .write("<form name=\"message\" method=\"POST\""
                                    + " action=\"" + pageContext.getServletContext()
                                    .getContextPath() + ConfigurationManager
                                    .getProperty("path.page.controller") + "\">");
                    pageContext.getOut().write("<input type=\"hidden\""
                            + " name=\"command\" value=\"newadv\" />");
                    pageContext.getOut().write("<input type=\"hidden\""
                            + " name=\"advId\""
                            + " value=\"" + adv.getId() + "\"/>");
                    pageContext.getOut()
                            .write("<input type=\"submit\" value=\""
                    + manager.getString("label.edit") + "\"/>");
                    pageContext.getOut().write("</form>");
                } else {
                    pageContext.getOut().write("<p><a href=\""
                            + pageContext.getServletContext()
                            .getContextPath() + ConfigurationManager
                            .getProperty("path.page.auth")
                            + "\">" + manager.getString("adv.auth")
                            + "</a> " + "или <a href=\"" + pageContext.getServletContext()
                            .getContextPath()
                            + ConfigurationManager
                            .getProperty("path.page.registration") + "\">"
                            + manager.getString("adv.registration")
                            + "</a>" + manager.getString("adv.warning") + "</p>");
                }
            } catch (IOException e) {
                LOGGER.error("Ошибка ввода-вывода " + e);
                final StackTraceElement[] trace = e.getStackTrace();
                for (final StackTraceElement el : trace) {
                    LOGGER.error("\n " + el);
                }
            } 
        } else {
            try {
                pageContext.getOut().write(manager.getString("error.advnotfound"));
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        
        return SKIP_BODY;
    }

}

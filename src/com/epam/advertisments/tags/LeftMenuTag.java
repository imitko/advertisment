package com.epam.advertisments.tags;

import java.io.IOException;
import java.util.List;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import com.epam.advertisments.dao.CategoryDAO;
import com.epam.advertisments.model.Category;
import com.epam.advertisments.resource.ConfigurationManager;

/**
 * Класс с пользовательским тегом для вывода левого меню.
 * @author Irina Mitsko
 *
 */
@SuppressWarnings("serial")
public class LeftMenuTag extends TagSupport {

    /**
     * Логгер для класса.
     */
    protected static final Logger LOGGER = (Logger) LogManager
            .getLogger(LeftMenuTag.class);
    /**
     * Метод вызывается, когда обнаруживается
     * начальный элемент тега на всех страницах для вывода левого меню.
     */
    @Override
    public int doStartTag() throws JspException {
        final List<Category> categories = (List<Category>) pageContext.getSession().getAttribute("categories");
        if (categories != null) {
            for (final Category category : categories) {
                try {
                    pageContext.getOut()
                    .write("<a class=\"leftmenu\" href=\""
                     + pageContext.getServletContext().getContextPath()
                     + ConfigurationManager.getProperty("path.page.categorycom")
                     + ConfigurationManager.getProperty("path.page.catid")
                     + category.getId() + "\"/>"
                     + category.getName() + "</a><br>");
                } catch (IOException e) {
                    LOGGER.error("Ошибка ввода-вывода " + e);
                    final StackTraceElement[] trace = e.getStackTrace();
                    for (final StackTraceElement el : trace) {
                        LOGGER.error("\n " + el);
                    }
                }
            }
        }
        return SKIP_BODY;
    }

}

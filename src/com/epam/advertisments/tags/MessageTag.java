package com.epam.advertisments.tags;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import com.epam.advertisments.dao.UserDAO;
import com.epam.advertisments.model.Message;
import com.epam.advertisments.model.User;
import com.epam.advertisments.resource.ConfigurationManager;
import com.epam.advertisments.resource.ElementManager;
/**
 * Класс с пользовательским тегом для формирования страницы сообщения
 * и истории переписки.
 * @author Irina Mitsko
 *
 */
@SuppressWarnings("serial")
public class MessageTag extends TagSupport {
    /**
     * Логгер для класса.
     */
    protected static final Logger LOGGER
        = (Logger) LogManager.getLogger(MessageTag.class);
    /**
     * Ссылка на объект ElementManager для доступа к языковым константам.
     */
    private ElementManager manager = ElementManager.INSTANCE;

    /**
     * Метод вызывается со страницы messagepage.jsp когда
     *  обнаруживается начальный элемент тега.
     */
    @Override
    public int doStartTag() throws JspException {
        Message correspondence;
        if (pageContext.getRequest().getAttribute("receivedmessage") != null) {
            correspondence
            = (com.epam.advertisments.model.Message) pageContext.getRequest()
                    .getAttribute("receivedmessage");
        } else {
            correspondence
            = (com.epam.advertisments.model.Message) pageContext.getRequest()
                    .getAttribute("sentmessage");
        }
        final String subject = (String) pageContext.getRequest()
                .getAttribute("subject");
        String advTitle = null;
        if(subject != null) {
            advTitle = subject;
        } else {
            advTitle = manager.getString("message.notfoundtitle");
        }
        final Map<Message,String> history = (Map<Message, String>) pageContext.getRequest()
                .getAttribute("history");
        try {
            pageContext.getOut().write("<br><h3>"
        + manager.getString("message.adv") + advTitle + "</h3>");
            pageContext.getOut().write("<hr>"
        + ((com.epam.advertisments.model.Message) correspondence).getDate());
            pageContext.getOut().write("<br>"
        + ((com.epam.advertisments.model.Message) correspondence).getMessage()
                    + "<br> <hr>" + manager.getString("message.history")
                    + "<br><br> ");
            for (Entry<Message, String> entry : history.entrySet()) {
                final Message message = entry.getKey();
                final String userName = entry.getValue();
                pageContext.getOut().write(message.getDate() + ", "
                        + userName + ":");
                pageContext.getOut().write(
                        "<br>" + manager.getString("message.wrote")
                        + message.getMessage() + "<br><p>-------</p>");
            }
            if (pageContext.getRequest().getAttribute("receivedmessage")
                    != null) {
                pageContext.getOut().write("<br><form name=\"message\""
                        + " method=\"POST\" action=\""
                        + pageContext.getServletContext().getContextPath()
                        + ConfigurationManager
                        .getProperty("path.page.controller")
                        + "\" class=\"form-horizontal\">");
                pageContext.getOut().write("<input type=\"hidden\""
                        + " name=\"command\""
                        + " value=\"sendmessage\" />");
                pageContext.getOut().write(
                        "<label>Ответить</label><input type=\"text\""
                        + " required oninput=\"currentDate()\""
                        + " name=\"message\" class=\"form-control input-sm\">");
                pageContext.getOut()
                        .write("<input type=\"hidden\" name=\"advertisment_id\""
                                + " value=\""
                        + ((com.epam.advertisments.model.Message)
                                correspondence)
                        .getAdvertismentId() + "\" class=\"form-control"
                                + " input-sm\">");
                pageContext.getOut()
                       .write("<input type=\"hidden\" name=\"receiver_id\""
                               + " value=\""
                       + ((com.epam.advertisments.model.Message) correspondence)
                       .getSenderId() + "\" class=\"form-control input-sm\">");
                pageContext.getOut()
                        .write("<input type=\"hidden\" name=\"sender_id\""
                                + " value=\""
                         + ((com.epam.advertisments.model.Message)
                                 correspondence)
                        .getReceiverId() + "\" class=\"form-control"
                                + " input-sm\">");
                pageContext.getOut().write("<br><input class=\"col-md-7\""
                        + " type=\"submit\" value=\""
                        + manager.getString("button.send") + "\"/>");
                pageContext.getOut().write("</form><br>");
            }
            pageContext.getOut().write("<br>");

        } catch (IOException e) {
            LOGGER.error("Ошибка ввода-вывода " + e);
            final StackTraceElement[] trace = e.getStackTrace();
            for (final StackTraceElement el : trace) {
                LOGGER.error("\n " + el);
            }
        }
        return SKIP_BODY;

    }

}

package com.epam.advertisments.comand;


import javax.servlet.http.HttpServletRequest;
import com.epam.advertisments.logic.AdvertismentLogic;
import com.epam.advertisments.resource.ConfigurationManager;

/**
 * Класс, отвечающий за поиск объявления по ключевому слову.
 * @author Irina Mitsko
 *
 */
public class SearchAdvCommand implements ActionCommand {

    /**
     * Метод обрабатывает команду поиска объявлений по ключевым словам.
     *  @return строка с адресом страницы с найденными объявлениями.
     */
    @Override
    public String execute(final HttpServletRequest request) {
        new AdvertismentLogic().searchAds(request);
        return ConfigurationManager.getProperty("path.page.category");
    }

}

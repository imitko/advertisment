package com.epam.advertisments.comand.client;

import com.epam.advertisments.comand.ActionCommand;
import com.epam.advertisments.comand.ActivateAdvertCommand;
import com.epam.advertisments.comand.AddImageCommand;
import com.epam.advertisments.comand.ChangeLocaleCommand;
import com.epam.advertisments.comand.DeleteAdvCommand;
import com.epam.advertisments.comand.DeleteCategoryCommand;
import com.epam.advertisments.comand.DeleteImageCommand;
import com.epam.advertisments.comand.DeleteUserCommand;
import com.epam.advertisments.comand.EditUserCommand;
import com.epam.advertisments.comand.ExtendCommand;
import com.epam.advertisments.comand.LoginCommand;
import com.epam.advertisments.comand.LogoutCommand;
import com.epam.advertisments.comand.ManageCategoriesCommand;
import com.epam.advertisments.comand.ManageUsersCommand;
import com.epam.advertisments.comand.ModerationCommand;
import com.epam.advertisments.comand.NewAdvCommand;
import com.epam.advertisments.comand.NewCategoryCommand;
import com.epam.advertisments.comand.RaitingCommand;
import com.epam.advertisments.comand.ReadMessageCommand;
import com.epam.advertisments.comand.RegistrationCommand;
import com.epam.advertisments.comand.SearchAdvCommand;
import com.epam.advertisments.comand.SendMessageCommand;
import com.epam.advertisments.comand.ShowAdvCommand;
import com.epam.advertisments.comand.ShowCategoryCommand;
import com.epam.advertisments.comand.ShowMessagesCommand;
import com.epam.advertisments.comand.ShowUsersAdvsCommand;
import com.epam.advertisments.comand.UpdateAdvCommand;
import com.epam.advertisments.comand.UpdateCategoryCommand;
import com.epam.advertisments.comand.UpdateCommand;
/**
 * Enum со списком команд пользователей.
 * @author Irina Mitsko
 *
 */
public enum CommandEnum {
    /**
     * Команда для входа в личный кабинет.
     */
    LOGIN {
        {
            this.command = new LoginCommand();
        }
    },
    /**
     * Команда для выхода из системы.
     */
    LOGOUT {
        {
            this.command = new LogoutCommand();
        }
    },
    /**
     * Команда обновления персональных данных пользователя.
     */
    UPDATEUSER {
        {
            this.command = new UpdateCommand();
        }
    },
    /**
     * Команда добавления нового объявления.
     */
    NEWADV {
        {
            this.command = new NewAdvCommand();
        }
    },
    /**
     * Команда сохранения изменений в объявлении.
     */
    UPDATEADV {
        {
            this.command = new UpdateAdvCommand();
        }
    },
    /**
     * Команда вывода страницы с объявлением.
     */
    SHOWADV {
        {
            this.command = new ShowAdvCommand();
        }
    },
     /**
      * Команда удаления объявления.
      */
    DELETEADV {
        {
            this.command = new DeleteAdvCommand();
        }
    },
    /**
     * Команда добавления изображения к объявлению.
     */
    ADDIMAGE {
        {
            this.command = new AddImageCommand();
        }
    },
    /**
     * команда удаления изрбражения.
     */
    DELETEIMG {
        {
            this.command = new DeleteImageCommand();
        }
    },
    /**
     * Команда вывода объявлений пользователя.
     */
    SHOWUSERSADV {
        {
            this.command = new ShowUsersAdvsCommand();
        }
    },
    /**
     * Команда продления объявления.
     */
    EXTENDADV {
        {
            this.command = new ExtendCommand();
        }
    },
    /**
     * Команда вывода всех объявлений из раздела.
     */
    CATEGORY {
        {
            this.command = new ShowCategoryCommand();
        }
    },
    /**
     * Команда для отправки сообщения пользователю.
     */
    SENDMESSAGE {
        {
            this.command = new SendMessageCommand();
        }
    },
    /**
     * Команда вывода станицы с почтой.
     */
    SHOWMESSAGES {
        {
            this.command = new ShowMessagesCommand();
        }
    },
    /**
     * Команда просмотра сообщения.
     */
    READMESSAGE {
        {
            this.command = new ReadMessageCommand();
        }
    },
    /**
     * Команда сохранения выставленной оценки пользователю.
     */
    RATING {
        {
            this.command = new RaitingCommand();
        }
    },
    /**
     * Команда регистрации пользователя.
     */
    REGISTRATION {
        {
            this.command = new RegistrationCommand();
        }
    },
    /**
     * Команда для вывода списка объявлений для модерации.
     */
    MODERATION {
        {
            this.command = new ModerationCommand();
        }
    },
    /**
     * Команда для активациии объявления модератором.
     */
    ACTIVATION {
        {
            this.command = new ActivateAdvertCommand();
        }
    },
    /**
     * Команда вывода списка всех разделов для редактирования.
     */
    MANAGECATEGORIES {
        {
            this.command = new ManageCategoriesCommand();
        }
    },
    /**
     * Команда для создания нового раздела.
     */
    NEWCATEGORY {
        {
            this.command = new NewCategoryCommand();
        }
    },
    /**
     * Команда для обновления данных раздела.
     */
    UPDATECATEGORY {
        {
            this.command = new UpdateCategoryCommand();
        }
    },
    /**
     * Команда для удаления раздела.
     */
    DELETECATEGORY {
        {
            this.command = new DeleteCategoryCommand();
        }
    },
    /**
     * Команда для вывода списка всех пользователй
     *  для редактирования.
     */
    MANAGEUSERS {
        {
            this.command = new ManageUsersCommand();
        }
    },
    /**
     * Команда для редактирования пользователя администратором.
     */
    EDITUSER {
        {
            this.command = new EditUserCommand();
        }
    },
    /**
     * Команда для удаления пользователя.
     */
    DELETEUSER {
        {
            this.command = new DeleteUserCommand();
        }
    },
    /**
     * Команда переключения языка.
     */
    LOCALE {
        {
            this.command = new ChangeLocaleCommand();
        }
    },
    /**
     * Команда поиска объявлений по емейлу.
     */
    SEARCHADV {
        {
            this.command = new SearchAdvCommand();
        }
    };

    /**
     *  ССылка на экземпляр ActionCommand.
     */
    protected ActionCommand command;

    /**
     * Ссылка на класс, реализующий интерфейс ActionCommand.
     * @return command - экземпляр класса, реализующего выполнение
     * конкретной команды.
     */
    public ActionCommand getCurrentCommand() {
        return command;
    }
}

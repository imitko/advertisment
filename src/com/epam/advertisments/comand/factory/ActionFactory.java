package com.epam.advertisments.comand.factory;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import com.epam.advertisments.comand.ActionCommand;
import com.epam.advertisments.comand.EmptyCommand;
import com.epam.advertisments.comand.client.CommandEnum;
import com.epam.advertisments.resource.MessageManager;

/**
 * Класс для определения команды пользователя в запросе.
 * @author Irina Mitsko
 *
 */
public class ActionFactory {
    /**
     * Логгер для класса.
     */
    private static final Logger LOGGER = (Logger) LogManager
            .getLogger(ActionFactory.class);
    /**
     * Метод извлекает из коммандной строки значение параметра command
     * и в зависимости от полученного значения выбирает команду.
     * @param request - запрос пользователя.
     * @return current - экземпляр класса для обработки полученной команды.
     */
    public ActionCommand defineCommand(final HttpServletRequest request) {
        ActionCommand current = new EmptyCommand();
        final String action = request.getParameter("command");
        if (action == null || action.isEmpty()) {
            return current;
        }
        try {
            final CommandEnum currentEnum
                = CommandEnum.valueOf(action.toUpperCase());
            current = currentEnum.getCurrentCommand();
        } catch (IllegalArgumentException e) {
            LOGGER.error("Ошибка! Комманда не найдена "
        + e);
            request.setAttribute("wrongAction", MessageManager
                    .INSTANCE.getString("message.nullpage"));
        }
        return current;
    }
}

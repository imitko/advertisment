/**
 * Пакет с классом, отвечающим за определения типа
 * команды пользователя.
 */
/**
 * @author Irina Mitsko
 *
 */
package com.epam.advertisments.comand.factory;

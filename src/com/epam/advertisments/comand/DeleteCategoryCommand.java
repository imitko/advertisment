package com.epam.advertisments.comand;


import javax.servlet.http.HttpServletRequest;
import com.epam.advertisments.logic.CategoryLogic;
import com.epam.advertisments.resource.ConfigurationManager;
/**
 * Класс обрабатывающий команду по удалению раздела каталога.
 * @author Irina Mitsko
 *
 */
public class DeleteCategoryCommand implements ActionCommand {

    /**
     * Метод, выполняющий команду пользователя по удалению
     *  объявления.
     *  @return строка с адресом страницы с категориями
     */
    @Override
    public String execute(final HttpServletRequest request) {
        new CategoryLogic().delete(request);
        return ConfigurationManager.getProperty("path.page.managecategories");
    }

}

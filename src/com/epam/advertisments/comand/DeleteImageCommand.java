package com.epam.advertisments.comand;



import javax.servlet.http.HttpServletRequest;
import com.epam.advertisments.logic.ImageLogic;
import com.epam.advertisments.resource.ConfigurationManager;

/**
 * Класс обрабатывающий команду по удалению картинки у объявления.
 * @author Irina Mitsko
 *
 */
public class DeleteImageCommand implements ActionCommand {

    /**
     * Метод, выполняющий команду пользователя по удалению фото у
     * объявления.
     * @return строка с адресом страницы просмотра картинок к объявлению
     */
    @Override
    public String execute(final HttpServletRequest request) {
        if (request.getParameter("fid") != null) {
           new ImageLogic().delete(request);
        }
        return ConfigurationManager.getProperty("path.page.addimage");
    }

}

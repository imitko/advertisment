package com.epam.advertisments.comand;

import javax.servlet.http.HttpServletRequest;
import com.epam.advertisments.logic.CategoryLogic;
import com.epam.advertisments.resource.ConfigurationManager;

/**
 * Класс обрабатывающий команду по по доступу к странице
 * управления категориями.
 * @author Irina Mitsko
 *
 */
public class ManageCategoriesCommand implements ActionCommand {

    /**
     * Метод, выполняющий доступ к странице списка и редактирования
     * категорий для администратора.
     *  @return строка с адресом страницы редактирования категорий.
     */
    @Override
    public String execute(final HttpServletRequest request) {
        String page = ConfigurationManager.getProperty("path.page.auth");
        if (request.getSession().getAttribute("userId") != null
                && ("ADMIN").equals(request.getSession().getAttribute("role"))) {
            page = new CategoryLogic().manageCategories(request);
            LOGGER.info("Вход на страницу редактирования категорий");
        }
        return page;
    }
}

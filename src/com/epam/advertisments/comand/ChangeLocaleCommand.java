package com.epam.advertisments.comand;


import javax.servlet.http.HttpServletRequest;
import com.epam.advertisments.logic.LanguageLogic;
import com.epam.advertisments.resource.ConfigurationManager;

/**
 * Класс обрабатывающий команду по изменению языка приложения.
 * @author Irina Mitsko
 *
 */
public class ChangeLocaleCommand implements ActionCommand {

    /**
     * Метод, выполняющий команду пользователя по смене языковой локали.
     * @return строка с адресом титульной страницы
     */
    @Override
    public String execute(final HttpServletRequest request) {
        new LanguageLogic().switchLanguage(request);
        return ConfigurationManager.getProperty("path.page.empty");
    }

}

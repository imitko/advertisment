package com.epam.advertisments.comand;

import javax.servlet.http.HttpServletRequest;
import com.epam.advertisments.logic.UserLogic;
import com.epam.advertisments.resource.ConfigurationManager;

/**
 * Класс обрабатывающий команду выхода пользователя из системы.
 * @author Irina Mitsko
 *
 */
public class LogoutCommand implements ActionCommand {

    /**
     * Метод, выполняющий выход пользователя из личного кабинета.
     *  @return строка с адресом страницы авторизации.
     */
    @Override
    public String execute(final HttpServletRequest request) {
        new UserLogic().logout(request);
        return ConfigurationManager.getProperty("path.page.main");
    }

}

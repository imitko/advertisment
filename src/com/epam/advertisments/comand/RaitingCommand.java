package com.epam.advertisments.comand;


import javax.servlet.http.HttpServletRequest;
import com.epam.advertisments.logic.RatingLogic;

/**
 * Класс обрабатывающий команду по выставлению оценок
 *  пользователями.
 * @author Irina Mitsko
 *
 */
public class RaitingCommand implements ActionCommand {

    /**
     * Метод, выполняющий фиксирование оценок пользователей.
     *  @return строка с подтверждением о принятии оценки.
     */
    @Override
    public String execute(final HttpServletRequest request) {
        final String referer = request.getHeader("referer");
        request.setAttribute("back", referer);
        return new RatingLogic().add(request);
    }

}

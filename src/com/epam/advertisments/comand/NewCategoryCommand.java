package com.epam.advertisments.comand;



import javax.servlet.http.HttpServletRequest;
import com.epam.advertisments.logic.CategoryLogic;

import com.epam.advertisments.resource.ConfigurationManager;
/**
 * Класс обрабатывающий команду по добавлению или редактированию
 * раздела каталога.
 * @author Irina Mitsko
 *
 */
public class NewCategoryCommand implements ActionCommand {

    /**
     * Метод, выполняющий команду по созданию нового раздела каталога.
     *  @return строка с адресом страницы.
     */
    @Override
    public String execute(final HttpServletRequest request) {
        String page = null;
        if (request.getParameter("categoryid") == null) {
            page = ConfigurationManager.getProperty("path.page.addcat");
            LOGGER.info("Вход на страницу добавления категории.");
        } else {
            page = new CategoryLogic().add(request);
        }
        return  page;
    }

}

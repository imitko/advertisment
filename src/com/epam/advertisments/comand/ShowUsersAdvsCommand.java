package com.epam.advertisments.comand;

import javax.servlet.http.HttpServletRequest;
import com.epam.advertisments.logic.AdvertismentLogic;

/**
 * Класс обрабатывающий команду по выводу страницы со всеми
 *  объявлениями пользователя.
 * @author Irina Mitsko
 *
 */
public class ShowUsersAdvsCommand implements ActionCommand {

    /**
     * Метод обрабатывает запрос пользователя на вывод всех размещенных
     * им объявлений.
     *  @return строка с адресом страницы с объявлениями или страницы
     *  с формой авторизации (в случае если пользователь разлогинился).
     */
    @Override
    public String execute(final HttpServletRequest request) {
        return new AdvertismentLogic().showUsersAds(request);
    }
}

package com.epam.advertisments.comand;


import javax.servlet.http.HttpServletRequest;
import com.epam.advertisments.logic.UserLogic;

/**
 * Класс, отвечающий за регистрацию пользователя.
 * @author Irina Mitsko
 *
 */
public class RegistrationCommand implements ActionCommand {

    /**
     * Метод обрабатывает команду по регистрации нового пользователя.
     *  @return строка с адресом страницы для перенаправления запроса
     */
    @Override
    public String execute(final HttpServletRequest request) {
        return new UserLogic().add(request);
    }

}

package com.epam.advertisments.comand;

import javax.servlet.http.HttpServletRequest;

import com.epam.advertisments.logic.UserLogic;

/**
 * Класс обрабатывающий команду по редактированию регистрационных
 *  данных пользователя (своих для роли USER или других пользователей
 *  для роли ADMIN).
 * @author Irina Mitsko
 *
 */
public class UpdateCommand implements ActionCommand {

    /**
     * Метод выполняет поиск пользователя в базе данных по id и сохраняет
     *  для этого пользователя новые данные, полученные из формы.
     * @return адрес страницы с данными всех пользователей (для администратора)
     * или страницу с регистрационными данными (для пользователя).
     */
    @Override
    public String execute(final HttpServletRequest request) {
        return new UserLogic().update(request);
    }

}

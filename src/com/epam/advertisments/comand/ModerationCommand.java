package com.epam.advertisments.comand;


import javax.servlet.http.HttpServletRequest;
import com.epam.advertisments.logic.AdvertismentLogic;
import com.epam.advertisments.resource.ConfigurationManager;
/**
 * Класс обрабатывающий команду по доступу на страницу
 *  модерирования новых объявлений.
 * @author Irina Mitsko
 *
 */
public class ModerationCommand implements ActionCommand {

    /**
     * Метод, выполняющий переход на страницу модерации
     *  объявлений (только для модераторов).
     *  @return строка с адресом страницы.
     */
    @Override
    public String execute(final HttpServletRequest request) {
        String page = ConfigurationManager.getProperty("path.page.auth");
        final String role = (String) request.getSession().getAttribute("role");
        if (request.getSession().getAttribute("userId")
                != null && role.equals("MODERATOR")) {
            new AdvertismentLogic().showModeration(request);
            page = ConfigurationManager.getProperty("path.page.usersadv");
            LOGGER.info("Вход на страницу модерации объявлений.");
        }
        
        return page;
    }

}

package com.epam.advertisments.comand;



import javax.servlet.http.HttpServletRequest;
import com.epam.advertisments.logic.AdvertismentLogic;

/**
 * Класс обрабатывающий команду по обновлению данных объявления.
 * @author Irina Mitsko
 *
 */
public class UpdateAdvCommand implements ActionCommand {

    /**
     * Метод, обрабатывающий команду редактированию объявления.
     *  @return строка с адресом страницы для добавления фото
     *  или страницы с ошибкой (если обновление прошло с ошибкой).
     */
    @Override
    public String execute(final HttpServletRequest request) {
        return new AdvertismentLogic().update(request);
    }

}

package com.epam.advertisments.comand;


import javax.servlet.http.HttpServletRequest;
import com.epam.advertisments.logic.AdvertismentLogic;
import com.epam.advertisments.resource.ConfigurationManager;

/**
 * Класс обрабатывающий команду по продлению
 *  пользователем объявления.
 * @author Irina Mitsko
 *
 */
public class ExtendCommand implements ActionCommand {
    /**
     * Метод, выполняющий команду пользователя по продлению
     *  объявления на 1 мес с текущей даты.
     *  @return адрес строки со списком объявлений, доступных
     *   для редактирования.
     */
    @Override
    public String execute(final HttpServletRequest request) {
        new AdvertismentLogic().extendAdvertisment(request);
        return ConfigurationManager.getProperty("path.page.advs");
    }

}

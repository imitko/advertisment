package com.epam.advertisments.comand;


import javax.servlet.http.HttpServletRequest;
import com.epam.advertisments.logic.UserLogic;
import com.epam.advertisments.resource.ConfigurationManager;

/**
 * Класс обрабатывающий команду по доступу на страницу
 *  редактирования всех пользователей приложения.
 * @author Irina Mitsko
 *
 */
public class ManageUsersCommand implements ActionCommand {

    /**
     * Метод, выполняющий переход на страницу редактирования
     *  пользователей.
     *  @return строка с адресом страницы.
     */
    @Override
    public String execute(final HttpServletRequest request) {
        String page = ConfigurationManager.getProperty("path.page.auth");
        if (request.getSession().getAttribute("userId") != null
                && ("ADMIN").equals(request.getSession().getAttribute("role"))) {
           page = new UserLogic().manageUsers(request);
            LOGGER.info("Вход на страницу редактирования пользователей.");
        }
        return page;
    }

}

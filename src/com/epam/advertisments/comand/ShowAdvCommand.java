package com.epam.advertisments.comand;


import javax.servlet.http.HttpServletRequest;
import com.epam.advertisments.logic.AdvertismentLogic;


/**
 * Класс обрабатывающий команду по выводу страницы с объявлением.
 * @author Irina Mitsko
 *
 */
public class ShowAdvCommand implements ActionCommand {

    /**
     *  Метод, выполняющий команду по выводу страницы с объявлением.
     *  @return адрес строки с объявлением или строка с ошибкой в
     *   случае ксли объявление не найдено.
     */
    @Override
    public String execute(final HttpServletRequest request) {
        return new AdvertismentLogic().showAds(request);
    }

}

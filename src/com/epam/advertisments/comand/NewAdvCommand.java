package com.epam.advertisments.comand;


import javax.servlet.http.HttpServletRequest;
import com.epam.advertisments.logic.AdvertismentLogic;

/**
 * Класс обрабатывающий команду по добавлению или редактированию
 * объявления.
 * @author Irina Mitsko
 *
 */
public class NewAdvCommand implements ActionCommand {

    /**
     * Метод, выполняющий создание нового объявления
     * или сохранение изменений в существующем.
     *  @return строка с адресом страницы.
     */
    @Override
    public String execute(final HttpServletRequest request) {
        return new AdvertismentLogic().add(request);
    }

}

package com.epam.advertisments.comand;



import javax.servlet.http.HttpServletRequest;
import com.epam.advertisments.logic.MessageLogic;
import com.epam.advertisments.resource.ConfigurationManager;

/**
 * Класс, отвечающий за пересылку сообщений между пользователями
 * по выбранному объявлению.
 * @author Irina Mitsko
 *
 */
public class SendMessageCommand implements ActionCommand {

    /**
     * Метод сохраняет сообщение пользователя в базе данных.
     * @return строка с адресом страницы подтверждения.
     */
    @Override
    public String execute(final HttpServletRequest request) {
        new MessageLogic().send(request);
        final String referer = request.getHeader("referer");
        request.setAttribute("back", referer);
        return ConfigurationManager.getProperty("path.page.success");

    }

}

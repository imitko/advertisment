package com.epam.advertisments.comand;


import javax.servlet.http.HttpServletRequest;
import com.epam.advertisments.logic.ImageLogic;



/**
 * Класс обрабатывающий команду по добавлению изображений
 * к объявлению пользователя.
 * @author Irina Mitsko
 *
 */
public class AddImageCommand implements ActionCommand {

    /**
     *  Метод, выполняющий команду пользователя по загрузке
     *   картинок к объявлению.
     *   @return строка с адресом страницы просмотра
     *    картинок к объявлению
     */
    @Override
    public String execute(final HttpServletRequest request) {
        return new ImageLogic().add(request);
    }


}

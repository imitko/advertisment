package com.epam.advertisments.comand;


import javax.servlet.http.HttpServletRequest;
import com.epam.advertisments.logic.AdvertismentLogic;
import com.epam.advertisments.resource.ConfigurationManager;
/**
 * Класс, обрабатывающий комманду модератора по автивации объявления.
 * @author Irina Mitsko
 *
 */
public class ActivateAdvertCommand implements ActionCommand {

    /**
     * Метод, который вызывает активацию объявления пользователя по
     *  id объявления.
     * @return адрес страницы сосписком объявлений для модерации
     */
    @Override
    public String execute(final HttpServletRequest request) {

        new AdvertismentLogic().activateAdvertisment(request);

        return ConfigurationManager.getProperty("path.page.moderation");
    }

}

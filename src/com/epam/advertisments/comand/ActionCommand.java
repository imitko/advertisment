package com.epam.advertisments.comand;


import javax.servlet.http.HttpServletRequest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import com.epam.advertisments.resource.ElementManager;
import com.epam.advertisments.resource.MessageManager;

/**
 * Абстрактный класс, определяющий метод для выполнения пользовательчкого
 * запроса.
 * @author Irina Mitsko
 *
 */
public interface ActionCommand {
    /**
     * Получение объекта ConnectionPool.
     */
    //ConnectionPool pool = ConnectionPool.getInstance();
    /**
     * Логгер для класса и классов-наследников.
     */
    Logger LOGGER
        = (Logger) LogManager.getLogger(ActionCommand.class);
    /**
     * Объект MessageManager для вызова языковых констант в классах-наследниках.
     */
    MessageManager MESSAGE_MANAGER = MessageManager.INSTANCE;
    /**
     * Объект ElementManager для вызова языковых констант статического
     *  контента в классах-наследниках.
     */
    ElementManager EL_MANAGER = ElementManager.INSTANCE;
    /**
     * Абстрактный метод для обработки команды пользователя
     *  и возврата нужной страницы.
     * @param request - запрос поользователя
     * @return - строка, содержащая адрес страницы перехода
     */
    String execute(HttpServletRequest request);
}

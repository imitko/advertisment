package com.epam.advertisments.comand;



import javax.servlet.http.HttpServletRequest;
import com.epam.advertisments.logic.CategoryLogic;
import com.epam.advertisments.resource.ConfigurationManager;
/**
 * Класс обрабатывающий команду по выводу всех объявлений категории.
 * @author Irina Mitsko
 *
 */
public class ShowCategoryCommand implements ActionCommand {

    /**
     * Метод находит разде сайта по id и затем строит список объявлений
     * относящихся к этому разделу.
     *  @return строка с адресом страницы объявлений по конкретному
     *  разделу сайта.
     */
    @Override
    public String execute(final HttpServletRequest request) {
        String page = null;
        if (request.getParameter("catid")
                != null && request.getParameter("catid").matches("\\d+")) {
            page = new CategoryLogic().showCategoryAds(request);
        } else {
            LOGGER.info("Попытка просмотра несуществующего раздела");
            page = ConfigurationManager.getProperty("path.page.error");
        }
        return page;
    }

}

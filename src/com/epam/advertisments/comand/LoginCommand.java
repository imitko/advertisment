package com.epam.advertisments.comand;


import javax.servlet.http.HttpServletRequest;
import com.epam.advertisments.logic.UserLogic;



/**
 * Класс обрабатывающий команду по логированию пользователя.
 * @author Irina Mitsko
 *
 */
public class LoginCommand implements ActionCommand {

    /**
     * Метод, выполняющий вход пользователя на сайт.
     *  @return строка с адресом титульной страницы.
     */
    @Override
    public String execute(final HttpServletRequest request) {
        return new UserLogic().login(request);
    }

}

package com.epam.advertisments.comand;


import javax.servlet.http.HttpServletRequest;
import com.epam.advertisments.logic.MessageLogic;
import com.epam.advertisments.resource.ConfigurationManager;
/**
 * Класс обрабатывающий команду по выводу всех сообщений пользователя.
 * @author Irina Mitsko
 *
 */
public class ShowMessagesCommand implements ActionCommand {

    /**
     * Метод находит список входящих и исходящих сообщений пользователя.
     *  @return строка с адресом страницы сообщений или страница
     *   авторизации если пользователь разлогинился.
     */
    @Override
    public String execute(final HttpServletRequest request) {
        String page = ConfigurationManager.getProperty("path.page.auth");
        MessageLogic logic = new MessageLogic();
        if (request.getSession().getAttribute("loggeduser") != null) {
            logic.findReceivedMessages(request);
            logic.findSentMessages(request);
            page = ConfigurationManager.getProperty("path.page.msgslist");
        }
        return page;
    }

}

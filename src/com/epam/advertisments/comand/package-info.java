/**
 * Пакет с классами, определяющими типы комманд,
 * полученных от пользователя.
 */
/**
 * @author Irina Mitsko
 *
 */
package com.epam.advertisments.comand;

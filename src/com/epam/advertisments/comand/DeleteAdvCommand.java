package com.epam.advertisments.comand;



import javax.servlet.http.HttpServletRequest;
import com.epam.advertisments.logic.AdvertismentLogic;

import com.epam.advertisments.resource.ConfigurationManager;
/**
 * Класс обрабатывающий команду по удалению объявления.
 * @author Irina Mitsko
 *
 */
public class DeleteAdvCommand implements ActionCommand {

    /**
     * ОБработка команды по удалению объявления поьзователем.
     *  @return адрес строки со списком объявлений, доступных
     *   для редактирования.
     */
    @Override
    public String execute(final HttpServletRequest request) {
        new AdvertismentLogic().delete(request);
        return ConfigurationManager.getProperty("path.page.advs");
    }

}

package com.epam.advertisments.comand;


import javax.servlet.http.HttpServletRequest;
import com.epam.advertisments.logic.UserLogic;
import com.epam.advertisments.resource.ConfigurationManager;
/**
 * Класс обрабатывающий команду по изменению данных
 *  пользователя администратором.
 * @author Irina Mitsko
 *
 */
public class EditUserCommand implements ActionCommand {

    /**
     * Метод, выполняющий команду по получению доступа к данным
     *  пользователя администратором для редактирования.
     * объявления.
     * @return адрес страницы с данными пользователя доступными
     *  для редактирования
     */
    @Override
    public String execute(final HttpServletRequest request) {
        String page = ConfigurationManager.getProperty("path.page.auth");
        if (("ADMIN").equals(request.getSession().getAttribute("role"))) {
           page = new UserLogic().editUser(request);
        }
        LOGGER.info("Попытка получения доступа к редактированию"
                + " пользователей.");
        return page;

    }

}

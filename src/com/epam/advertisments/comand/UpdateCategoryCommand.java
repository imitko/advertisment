package com.epam.advertisments.comand;



import javax.servlet.http.HttpServletRequest;
import com.epam.advertisments.logic.CategoryLogic;

/**
 * Класс обрабатывающий команду по обновлению данных категории.
 * @author Irina Mitsko
 *
 */
public class UpdateCategoryCommand implements ActionCommand {

    /**
     * Метод, выполняюший команду по обновлению названия раздела катлога.
     *  @return строка с адресом страницы редактирования списка категорий.
     */
    @Override
    public String execute(final HttpServletRequest request) {
        return new CategoryLogic().update(request);
    }

}

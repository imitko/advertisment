package com.epam.advertisments.comand;


import javax.servlet.http.HttpServletRequest;
import com.epam.advertisments.logic.UserLogic;
import com.epam.advertisments.resource.ConfigurationManager;

/**
 * Класс обрабатывающий команду по удалению пользователя из системы.
 * @author Irina Mitsko
 *
 */
public class DeleteUserCommand implements ActionCommand {

    /**
     * Метод, выполняющий команду администратора по удалению пользователя.
     * @return строка с адресом страницы списка пользователей
     */
    @Override
    public String execute(final HttpServletRequest request) {
       new UserLogic().delete(request);
       return ConfigurationManager.getProperty("path.page.manageusers");
    }

}

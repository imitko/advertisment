package com.epam.advertisments.comand;


import javax.servlet.http.HttpServletRequest;
import com.epam.advertisments.logic.MessageLogic;
import com.epam.advertisments.resource.ConfigurationManager;

/**
 * Класс обрабатывающий команду по открытию страницы с сообщением.
 * @author Irina Mitsko
 *
 */
public class ReadMessageCommand implements ActionCommand {

    /**
     * Метод вызывает страницу с читаемым сообщением и историей переписки.
     *  @return строка с адресом страницы переписки или страница авторизации (если
     *  пользователь разлогинился).
     */
    @Override
    public String execute(final HttpServletRequest request) {
        String page = ConfigurationManager.getProperty("path.page.auth");
        if (request.getSession().getAttribute("loggeduser") != null) {
            new MessageLogic().read(request);
            page = ConfigurationManager.getProperty("path.page.message");
        }
        return page;
    }

}

package com.epam.advertisments.comand;

import java.sql.Connection;
import java.util.List;
import javax.servlet.http.HttpServletRequest;

import com.epam.advertisments.controller.Controller;
import com.epam.advertisments.dao.AdvertismentDAO;
import com.epam.advertisments.dao.CategoryDAO;
import com.epam.advertisments.dao.ImageDAO;
import com.epam.advertisments.model.Advertisment;
import com.epam.advertisments.model.Category;
import com.epam.advertisments.resource.ConfigurationManager;
/**
 * Класс обрабатывающий команду по переходй на
 *  титульную страницу сайта.
 * @author Irina Mitsko
 *
 */
public class EmptyCommand implements ActionCommand {

    /**
     * Метод, формирующий титульную страницу сайта.
     *  @return строка с пустой командой соответствующая
     *   титульной странице.
     */
    @Override
    public String execute(final HttpServletRequest request) {
        final Connection conn = Controller.pool.getConnection();
        final CategoryDAO catDao = new CategoryDAO(conn);
        final AdvertismentDAO advDao = new AdvertismentDAO(conn);
        final List<Category> categories = catDao.findAll();
        request.getSession().setAttribute("categories", categories);
        final List<Advertisment> lastAdvs = advDao.findLastTen();
        request.getSession().setAttribute("lastAdvs", lastAdvs);
        final ImageDAO imgDao = new ImageDAO(conn);
        request.getSession().setAttribute("imgdao", imgDao);
        LOGGER.info("Переход на титульную страницу сайта.");
        Controller.pool.freeConnection(conn);
        return ConfigurationManager.getProperty("path.page.empty");
    }

}

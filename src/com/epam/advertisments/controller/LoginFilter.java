package com.epam.advertisments.controller;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.epam.advertisments.resource.ConfigurationManager;
import com.epam.advertisments.resource.MessageManager;

/**
 * Фильтр для доступа на страницу добавления объявлений.
 * @author Irina Mitsko
 *
 */
@WebFilter(urlPatterns = { "/jsps/addAdvertisment.jsp" })
public class LoginFilter implements Filter {

    /**
     * Метод проверяет, если пользователь не залогинен, при попытке
     *  добавить объявление перенаправляет на страницу авторизации.
     */
    @Override
    public void doFilter(final ServletRequest request,
            final ServletResponse response, final FilterChain chain)
            throws IOException, ServletException {
        final HttpServletRequest servletRequest = (HttpServletRequest) request;
        final HttpServletResponse servletResponse = (HttpServletResponse) response;
        final HttpSession session = servletRequest.getSession(true);
        final MessageManager manager = MessageManager.INSTANCE;
        if (session.getAttribute("role") != null
                && session.getAttribute("role").equals("USER")) {
            if (session.getAttribute("error") != null) {
                session.removeAttribute("error");
            }
            chain.doFilter(servletRequest, servletResponse);
        } else {
            request.setAttribute("error", manager.getString("error.addadv"));
            RequestDispatcher dispatcher = request.getServletContext()
                    .getRequestDispatcher(ConfigurationManager
                            .getProperty("path.page.auth"));
            dispatcher.forward(servletRequest, servletResponse);
        }
    }

}

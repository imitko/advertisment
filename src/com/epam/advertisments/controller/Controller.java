package com.epam.advertisments.controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.epam.advertisments.comand.ActionCommand;
import com.epam.advertisments.comand.factory.ActionFactory;
import com.epam.advertisments.connection.ConnectionPool;
import com.epam.advertisments.resource.ConfigurationManager;
import com.epam.advertisments.resource.MessageManager;

/**
 * Сервлет приложения.
 * @author Irina Mitsko
 *
 */
@WebServlet(urlPatterns = { "/index.isp", "/controller" })
@MultipartConfig(fileSizeThreshold = 1024 * 1024 * 2, // 2MB
        maxFileSize = 1024 * 1024 * 10, // 10MB
        maxRequestSize = 1024 * 1024 * 50) // 50MB
public class Controller extends HttpServlet {
    /**
     * Получение объекта ConnectionPool.
     */
    public static ConnectionPool pool;
    /**
     */
    private static final long serialVersionUID = 478163198867535774L;

    /**
     * Метод init с получением соединения из пула.
     */
    @Override
    public void init(final ServletConfig config) throws ServletException {
        pool = ConnectionPool.getInstance();
        super.init(config);
    }

    /**
     * Метод doPost.
     */
    @Override
    public void doPost(final HttpServletRequest request,
            final HttpServletResponse response)
                    throws ServletException,
            IOException {
        processRequest(request, response);
    }

    /**
     * Метод doGet.
     */
    @Override
    public void doGet(final HttpServletRequest request,
            final HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Метод определяет команду пользователя и назначает страницу, на которую
     *  перенаправить пользователя.
     * @param request - запрос пользователя
     * @param response = ответ пользователю
     * @throws ServletException - объект ошибки сервлета
     * @throws IOException - объект ошибки ввода-вывода
     */
    private void processRequest(final HttpServletRequest request,
            final HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html; charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        String page = null;
        final ActionFactory client = new ActionFactory();
        final ActionCommand command = client.defineCommand(request);
        page = command.execute(request);
        if (page != null) {
            final RequestDispatcher dispatcher = getServletContext()
                    .getRequestDispatcher(page);
            dispatcher.forward(request, response);
        } else {
            page = ConfigurationManager.getProperty("path.page.error");
            request.getSession().setAttribute("nullPage",
                    MessageManager.INSTANCE.getString("message.nullpage"));
            response.sendRedirect(request.getContextPath() + page);
        }
    }

    /**
     * переопределение метода destroy
     *  - возвращение соединения в очередь.
     */
    @Override
    public void destroy() {
        pool.closeFreeConnections();
        pool.closeUsedConnections();
        super.destroy();
    }
}

package com.epam.advertisments.model;
/**
 * Класс, описывающий сущность "Объявление".
 * @author Irina Mitsko
 *
 */
public class Advertisment extends Entity {

    /**
     * Уникальный идентификатор версии сериализованного класса.
     */
    private static final long serialVersionUID = -8728443763089359825L;
    /**
     * Уникальный идентификатор объявления.
     */
    private int userId;
    /**
     * Идентификатор категории, к которому относится объявление.
     */
    private int categoryId;
    /**
     * Заголовок объявления.
     */
    private String title;
    /**
     * Текст объявления.
     */
    private String text;
    /**
     * Цена для объявления.
     */
    private double price;
    /**
     * Дата начала объявления.
     */
    private String startDate;
    /**
     * Дата окончания объявления.
     */
    private String endDate;
    /**
     * Флаг, показывающий активно ли объявление.
     */
    private boolean isActive;
    /**
     * Флаг, показывающий прошло ли объявление
     * модерацию.
     */
    private boolean isModerated;

    /**
     * Конструктор без параметров.
     */
    public Advertisment() {
        super();
    }

    /**
     * Конструктор с параметрами.
     * @param id - уникальный идентификатор объявления
     * @param newUserId - идентификатор пользователя
     * @param newCategoryId = идентификатор категории
     * @param newTitle - заголовок
     * @param newText - текст объявления
     * @param newPrice - цена
     * @param newStartDate - дата тачала объявления
     * @param newEndDate - дата окончания объявления
     * @param newIsActive - флаг активности объявления
     * @param newIsModerated - флаг модерации
     */
    public Advertisment(final int id, final int newUserId,
            final int newCategoryId, final String newTitle,
            final String newText, final double newPrice,
            final String newStartDate, final String newEndDate,
            final boolean newIsActive, final boolean newIsModerated) {
        super(id);
        this.userId = newUserId;
        this.categoryId = newCategoryId;
        this.title = newTitle;
        this.text = newText;
        this.startDate = newStartDate;
        this.endDate = newEndDate;
        this.isActive = newIsActive;
        this.isModerated = newIsModerated;
    }

    /**
     * Геттер для идентификаторра объявления.
     * @return userId - идентификатор
     */
    public int getUserId() {
        return userId;
    }

    /**
     * Сеттер для идентификатора объявления.
     * @param newUserId - новый идентификатор
     */
    public void setUserId(final int newUserId) {
        this.userId = newUserId;
    }

    /**
     * Геттер для идентификатора категории объявления.
     * @return categoryId - идентификатор объявления
     */
    public int getCategoryId() {
        return categoryId;
    }

    /**
     * Сеттер для идентификатора объявления.
     * @param newCategoryId - новы йдентификаор объявления
     */
    public void setCategoryId(final int newCategoryId) {
        this.categoryId = newCategoryId;
    }

    /**
     * Геттер для заголовка объявления.
     * @return title - заголовок
     */
    public String getTitle() {
        return title;
    }

    /**
     * Сеттре для заголовка объявления.
     * @param newTitle - новый заголовок объявления
     */
    public void setTitle(final String newTitle) {
        this.title = newTitle;
    }

    /**
     * Геттер для текста объявления.
     * @return text - текст объявления
     */
    public String getText() {
        return text;
    }

    /**
     * Сеттер для текста объявления.
     * @param newText - текст объявления
     */
    public void setText(final String newText) {
        this.text = newText;
    }

    /**
     * Геттрер для поля цена.
     * @return price - значение цены
     */
    public double getPrice() {
        return price;
    }

    /**
     * Сеттер для поля цена.
     * @param newPrice - новое значение цены
     */
    public void setPrice(final double newPrice) {
        this.price = newPrice;
    }

    /**
     * Геттер для поля дата начала объявления.
     * @return startDate - значение, соответствующее дате
     */
    public String getStartDate() {
        return startDate;
    }

    /**
     * Сеттер для даты начала объявления.
     * @param newStartDate = новое значение даты
     */
    public void setStartDate(final String newStartDate) {
        this.startDate = newStartDate;
    }

    /**
     * Геттер для поля дата окончания объявления.
     * @return endDate - значение, соответствующее дате
     */
    public String getEndDate() {
        return endDate;
    }

    /**
     * Сеттер для поля дата окончания объявления.
     * @param newEndDate - новое значение поля
     */
    public void setEndDate(final String newEndDate) {
        this.endDate = newEndDate;
    }

    /**
     * Геттер для флага, обозначающего активность объявления.
     * @return isActive значение true если активно
     */
    public boolean isActive() {
        return isActive;
    }

    /**
     * Сеттер для флага активности объявления.
     * @param newIsActive - новое значение поля
     */
    public void setActive(final boolean newIsActive) {
        this.isActive = newIsActive;
    }

    /**
     * Геттер для флага, обозначающего модерацию объявления.
     * @return isModerated  значение true если прошло модерацию
     */
    public boolean isModerated() {
        return isModerated;
    }

    /**
     * Сеттер для флага модерации.
     * @param newIsModerated - новое значение флага
     */
    public void setModerated(final boolean newIsModerated) {
        this.isModerated = newIsModerated;
    }

    /**
     * Переопределение метода расчета хэш-кода объекта.
     */
    @Override
    public int hashCode() {
        final int digit = 32;
        final int prime = 31;
        final int magicNum1 = 1231;
        final int magicNum2 = 1237;
        int result = super.hashCode();
        result = prime * result + categoryId;
        result = prime * result
                + ((endDate == null) ? 0 : endDate.hashCode());
        result = prime * result + (isActive ? magicNum1 : magicNum2);
        result = prime * result + (isModerated ? magicNum1 : magicNum2);
        long temp;
        temp = Double.doubleToLongBits(price);
        result = prime * result + (int) (temp ^ (temp >>> digit));
        result = prime * result
                + ((startDate == null) ? 0 : startDate.hashCode());
        result = prime * result
                + ((text == null) ? 0 : text.hashCode());
        result = prime * result
                + ((title == null) ? 0 : title.hashCode());
        result = prime * result + userId;
        return result;
    }

    /**
     * Переопределение метода сравнения двух объектов
     *  класса Advertisment.
     */
    @Override
    public boolean equals(final Object obj) {
        boolean result = true;
        if (this == obj) {
            result = true;
        }
        if (!super.equals(obj)) {
            result = false;
        }
        if (getClass() != obj.getClass()) {
            result = false;
        }
        final Advertisment other = (Advertisment) obj;
        if (categoryId != other.categoryId) {
            result = false;
        }
        if (endDate == null) {
            if (other.endDate != null)
                result = false;
        } else if (!endDate.equals(other.endDate)) {
            result = false;
        }
        if (isActive != other.isActive) {
            result = false;
        }
        if (isModerated != other.isModerated) {
            result = false;
        }
        if (Double.doubleToLongBits(price)
                != Double.doubleToLongBits(other.price)) {
            result = false;
        }
        if (startDate == null) {
            if (other.startDate != null) {
                result = false;
            }
        } else if (!startDate.equals(other.startDate)) {
            result = false;
        }
        if (text == null) {
            if (other.text != null) {
                result = false;
            }
        } else if (!text.equals(other.text)) {
            result = false;
        }
        if (title == null) {
            if (other.title != null) {
                result = false;
            }
        } else if (!title.equals(other.title)) {
            result = false;
        }
        if (userId != other.userId) {
            result = false;
        }
        return result;
    }

}

package com.epam.advertisments.model;
/**
 * Класс, описывающий сущность Mark.
 * @author Irina Mitsko
 *
 */
public class Mark extends Entity {

    /**
    * Уникальный идентификатор версии сериализованного класса.
    */
    private static final long serialVersionUID = 7263884970453086229L;
    /**
     * Идентификатор отправителя оценки.
     */
    private int senderId;
    /**
     * Идентификатор получателя оценки.
     */
    private int receipentId;
    /**
     * Оценка пользователя.
     */
    private int markValue;

    /**
     * Конструктор без параметров.
     */
    public Mark() {
        super();
    }

    /**
     * Геттер для поля идентификатор отправителя.
     * @return senderId - значение идентификатора отправителя
     */
    public int getSenderId() {
        return senderId;
    }

    /**
     * Сеттер для поля идентификатор отправителя.
     * @param newSenderId - устанавливапмое значение
     *  идентификатора отправителя
     */
    public void setSenderId(final int newSenderId) {
        this.senderId = newSenderId;
    }

    /**
     * Геттер для поля идентификатор получателя.
     * @return receipentId - значение поля идентификатора
     */
    public int getReceipentId() {
        return receipentId;
    }

    /**
     * Сеттер для установки значения поля идентификатор получателя.
     * @param newReceipentId - устанавливаемое значение идентификатора
     */
    public void setReceipentId(final int newReceipentId) {
        this.receipentId = newReceipentId;
    }

    /**
     * Геттер для получения значения оценки.
     * @return mark - значение поля "оценка"
     */
    public int getMarkValue() {
        return markValue;
    }

    /**
     * Сеттер для установки нового значения оценки.
     * @param newMark - eустанавливаемое значение поля
     */
    public void setMarkValue(final int newMark) {
        this.markValue = newMark;
    }

    /**
     * Переопределение метода расчета хэш-кода объекта.
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + markValue;
        result = prime * result + receipentId;
        result = prime * result + senderId;
        return result;
    }

    /**
     * Переопределение метода сравнения двух объектов
     *  класса.
     */
    @Override
    public boolean equals(final Object obj) {
        boolean result = true;
        if (this == obj) {
            result = true;
        }
        if (!super.equals(obj)) {
            result = false;
        }
        if (getClass() != obj.getClass()) {
            result = false;
        }
        final Mark other = (Mark) obj;
        if (markValue != other.markValue) {
            result = false;
        }
        if (receipentId != other.receipentId) {
            result = false;
        }
        if (senderId != other.senderId) {
            result = false;
        }
        return result;
    }

}

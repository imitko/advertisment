package com.epam.advertisments.model;

/**
 * 
 * @author Irina Mitsko
 *
 */
public class Message extends Entity implements Comparable<Message> {

    /**
    * 
    */
    private static final long serialVersionUID = -7771800760574243647L;
    /**
     * 
     */
    private int senderId;
    /**
     * 
     */
    private int receiverId;
    /**
     * 
     */
    private int advertismentId;
    /**
     * 
     */
    private String message;
    /**
     * 
     */
    private String date;

    /**
     * 
     */
    public Message() {
    }

    /**
     * 
     * @return
     */
    public int getSenderId() {
        return senderId;
    }

    /**
     * 
     * @param senderId
     */
    public void setSenderId(int senderId) {
        this.senderId = senderId;
    }

    /**
     * 
     * @return
     */
    public int getReceiverId() {
        return receiverId;
    }

    /**
     * 
     * @param receiverId
     */
    public void setReceiverId(int receiverId) {
        this.receiverId = receiverId;
    }

    /**
     * 
     * @return
     */
    public int getAdvertismentId() {
        return advertismentId;
    }

    /**
     * 
     * @param advertismentId
     */
    public void setAdvertismentId(int advertismentId) {
        this.advertismentId = advertismentId;
    }

    /**
     * 
     * @return
     */
    public String getMessage() {
        return message;
    }

    /**
     * 
     * @param message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * 
     * @return
     */
    public String getDate() {
        return date;
    }

    /**
     * 
     * @param date
     */
    public void setDate(String date) {
        this.date = date;
    }

    /**
     * 
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + advertismentId;
        result = prime * result + ((date == null) ? 0 : date.hashCode());
        result = prime * result + ((message == null) ? 0 : message.hashCode());
        result = prime * result + receiverId;
        result = prime * result + senderId;
        return result;
    }

    /**
     * 
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        Message other = (Message) obj;
        if (advertismentId != other.advertismentId)
            return false;
        if (date == null) {
            if (other.date != null)
                return false;
        } else if (!date.equals(other.date))
            return false;
        if (message == null) {
            if (other.message != null)
                return false;
        } else if (!message.equals(other.message))
            return false;
        if (receiverId != other.receiverId)
            return false;
        if (senderId != other.senderId)
            return false;
        return true;
    }
    
    /**
     * Переопределение метода для сортировки сообщений по ТreeMap.
     */
    @Override
    public int compareTo(final Message entity) {
        int result;
        if (id > entity.getId()) {
            result = -1;
        } else if (id < entity.getId()) {
            result = 1;
        } else {
            result = 0;
        }
        return result;
    }
        

}

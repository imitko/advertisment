package com.epam.advertisments.model;
/**
 * Класс, описывающий сущность Категория.
 * @author Irina Mitsko
 *
 */
public class Category extends Entity implements Comparable<Category>{

    /**
     * Уникальный идентификатор версии сериализованного класса.
     */
    private static final long serialVersionUID = -3259975138746092573L;
    /**
     * Назваение категории.
     */
    private String name;

    /**
     * Конструктор без параметров.
     */
    public Category() {
        super();
    }

    /**
     * Конструктор с параметрами.
     * @param newId - уникальный идентификатор
     * @param newName = название категории
     */
    public Category(final int newId, final String newName) {
        super(newId);
        this.name = newName;
    }

    /**
     * Геттер для поля название.
     * @return name - значение поля название
     */
    public String getName() {
        return name;
    }

    /**
     * Сеттер для поля название.
     * @param newName - новое значение поля название
     */
    public void setName(final String newName) {
        this.name = newName;
    }

    /**
     * Переопределение метода расчета хэш-кода объекта.
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        int index;
        if (name == null) {
            index = 0;
        } else {
            index = name.hashCode();
        }
        result = prime * result + index;
        return result;
    }

    /**
     * Переопределение метода сравнения двух объектов
     *  класса.
     */
    @Override
    public boolean equals(final Object obj) {
        boolean result = true;
        if (this == obj) {
            result = true;
        }
        if (!super.equals(obj)) {
            result = false;
        }
        if (getClass() != obj.getClass()) {
            result = false;
        }
        final Category other = (Category) obj;
        if (name == null) {
            if (other.name != null) {
                result = false;
            }
        } else if (!name.equals(other.name)) {
            result = false;
        }
        return result;
    }

    /**
     * Переопределение метода для сортировкаи по алфавиту.
     */
    @Override
    public int compareTo(final Category newCategory) {
    return name.charAt(0)-newCategory.getName().charAt(0);
    }

    

}

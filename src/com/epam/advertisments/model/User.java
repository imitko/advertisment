package com.epam.advertisments.model;
/**
 * Класс, описывающий сущность User.
 * @author Irina Mitsko
 *
 */
public class User extends Entity {

    /**
    * Уникальный идентификатор версии сериализованного класса.
    */
    private static final long serialVersionUID = 7281264242709256538L;
    /**
     * Имя пользователя.
     */
    private String name;
    /**
     * Емейд пользователя.
     */
    private String email;
    /**
     * телефон пользователя.
     */
    private String phone;
    /**
     * Пароль пользователя.
     */
    private String password;
    /**
     * Роль пользователя.
     */
    private UserRole role;

    /**
     * Конструктор без параметров.
     */
    public User() {
        super();
    }

    /**
     * параметризированный конструктор.
     * @param newId - уникальный идентификатор пользователя
     * @param newName - имя пользователя
     * @param newEmail - емейл пользователя
     * @param newPhone - телефон пользователя
     * @param newPassword - пароль пользователя
     * @param newRole = роль пользователя
     */
    public User(final int newId, final String newName, final String newEmail,
            final String newPhone, final String newPassword,
            final UserRole newRole) {
        super(newId);
        this.name = newName;
        this.email = newEmail;
        this.phone = newPhone;
        this.password = newPassword;
        this.role = newRole;
    }

    /**
     * Получение значения поля имя пользователя.
     * @return name - значение поля имя
     */
    public String getName() {
        return name;
    }

    /**
     * Установка нового значения поля имя.
     * @param name - устанавливаемое значение поля
     */
    public void setName(final String newName) {
        this.name = newName;
    }

    /**
     * Получение значения поля емейл.
     * @return email - значение поля емейл
     */
    public String getEmail() {
        return email;
    }

    /**
     * Установка значения поля емейд.
     * @param newEmail - устанавливаемое значение поля
     */
    public void setEmail(final String newEmail) {
        this.email = newEmail;
    }

    /**
     * Получение значения поля phone.
     * @return phone = значение поля
     */
    public String getPhone() {
        return phone;
    }

    /**
     * Установка значения поля phone.
     * @param newPhone - новое значение поля
     */
    public void setPhone(final String newPhone) {
        this.phone = newPhone;
    }

    /**
     * Получение значения поля Пароль.
     * @return password - значение поля
     */
    public String getPassword() {
        return password;
    }

    /**
     * Установка значения поля пароль.
     * @param newPassword - значение поля
     */
    public void setPassword(final String newPassword) {
        this.password = newPassword;
    }

    /**
     * Получение значения поля роли пользователя.
     * @return role - значение роли
     */
    public UserRole getRole() {
        return role;
    }

    /**
     * Установка значения поля роли пользователя.
     * @param newRole - pyfxtybt gjkz
     */
    public void setRole(final UserRole newRole) {
        this.role = newRole;
    }

    /**
     * Переопределение метода расчета хэш-кода объекта.
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((email == null) ? 0 : email.hashCode());
        result = prime * result + id;
        result = prime * result
                + ((name == null) ? 0 : name.hashCode());
        result = prime * result
                + ((password == null) ? 0 : password.hashCode());
        result = prime * result
                + ((phone == null) ? 0 : phone.hashCode());
        result = prime * result
                + ((role == null) ? 0 : role.hashCode());
        return result;
    }

    /**
     * Переопределение метода сравнения двух объектов
     *  класса.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final User other = (User) obj;
        if (email == null) {
            if (other.email != null) {
                return false;
            }
        } else if (!email.equals(other.email)) {
            return false;
        }
        if (id != other.id) {
            return false;
        }
        if (name == null) {
            if (other.name != null) {
                return false;
            }
        } else if (!name.equals(other.name)) {
            return false;
        }
        if (password == null) {
            if (other.password != null) {
                return false;
            }
        } else if (!password.equals(other.password)) {
            return false;
        }
        if (phone == null) {
            if (other.phone != null) {
                return false;
            }
        } else if (!phone.equals(other.phone)) {
            return false;
        }
        if (role != other.role) {
            return false;
        }
        return true;
    }

}

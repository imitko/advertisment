package com.epam.advertisments.model;

import java.io.Serializable;
/**
 * Базовый класс для всех классов, описывающих сущностей.
 * @author Irina Mitsko
 * @param <T>
 *
 */
public class Entity implements Serializable, Cloneable{
    /**
    * Уникальный идентификатор версии сериализованного класса.
    */
    private static final long serialVersionUID = -7761212785617033648L;
    /**
     * Уникальный идентификатор.
     */
    int id;

    /**
     * Конструктор без параметров.
     */
    public Entity() {
    }

    /**
     * Конструктор с параметром.
     * @param newId - уникальный идентификатор объекта.
     */
    public Entity(final int newId) {
        this.id = newId;
    }

    /**
     * Геттер для поля идентификатор.
     * @return id - значение поля идентификатор
     */
    public int getId() {
        return id;
    }

    /**
     * Сеттер для поля идентификатор.
     * @param newId - новое значение поля идентификатор
     */
    public void setId(final int newId) {
        this.id = newId;
    }

    /**
     * Переопределение метода расчета хэш-кода объекта.
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + id;
        return result;
    }

    /**
     * Переопределение метода сравнения двух объектов.
     */
    @Override
    public boolean equals(final Object obj) {
        boolean result = true;
        if (this == obj) {
            result = true;
        }
        if (obj == null) {
            result = false;
        }
        if (getClass() != obj.getClass()) {
            result = false;
        }
        final Entity other = (Entity) obj;
        if (id != other.id) {
            result = false;
        }
        return result;
    }
    
   

}

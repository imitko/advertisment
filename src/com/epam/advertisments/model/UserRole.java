package com.epam.advertisments.model;

/**
 * Класс для определения списка ролей пользователей.
 * @author irina Mitsko
 */
public enum UserRole {
    /**
     * Роль администратора.
     */
    ADMIN("ADMIN"),
    /**
     * Роль пользователя.
     */
    USER("USER"),
    /**
     * Роль модератора.
     */
    MODERATOR("MODERATOR");
    /**
     * Строковое предстваление типа роли пользователя.
     */
    private String userProfileType;

    /**
     * Параметризированный конструктор.
     * @param newUserProfileType
     *            - значение поля
     */
    UserRole(final String newProfileType) {
        this.userProfileType = newProfileType;
    }

    /**
     * Геттер для получения текстового значения роли пользователя.
     * @return userProfileType - значение поля userProfileType
     */
    public String getUserProfileType() {
        return userProfileType;
    }
}

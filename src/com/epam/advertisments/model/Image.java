package com.epam.advertisments.model;

import java.sql.Blob;
/**
 * Класс, описывающий сущность Image.
 * @author Irina Mitsko
 *
 */
public class Image extends Entity {

    /**
    * Уникальный идентификатор версии сериализованного класса.
    */
    private static final long serialVersionUID = -644049115005423376L;
    /**
     * Идентификатор объявления, к которой относится картинка.
     */
    private int advertismentId;
    /**
     * Имя файла картинки.
     */
    private String fileName;
    /**
     * Картинка.
     */
    private Blob image;
    /**
     * Картинка в шифрованом формате.
     */
    private String encoding;

    /**
     * Конструктор без параметров.
     */
    public Image() {
        super();
    }

    /**
     * Конструктор с параметрами.
     * @param newId - идентификатр картинки
     * @param newAdvertismentId - идентификатор объявления
     * @param newFileName - имя файла
     * @param newImage - картинка
     * @param newEncoding - кодировка картинки
     */
    public Image(final int newId, final int newAdvertismentId,
            final String newFileName, final Blob newImage,
            final String newEncoding) {
        super(newId);
        this.advertismentId = newAdvertismentId;
        this.fileName = newFileName;
        this.image = newImage;
        this.encoding = newEncoding;
    }

    /**
     * Геттер для поля идентификатор объявления.
     * @return advertismentId - значение идентификатора
     */
    public int getAdvertismentId() {
        return advertismentId;
    }

    /**
     * Сеттер для поля идентификатор объявления.
     * @param newAdvertismentId - значение поля идентификатор
     */
    public void setAdvertismentId(final int newAdvertismentId) {
        this.advertismentId = newAdvertismentId;
    }

    /**
     * Геттер для поля имя файла.
     * @return fileName - значение поля имя файла
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * Сеттер для поля имя файла.
     * @param newFileName - новое значение поля имя файла
     */
    public void setFileName(final String newFileName) {
        this.fileName = newFileName;
    }

    /**
     * Геттер для поля Image.
     * @return image - pзначение поля Image
     */
    public Blob getImage() {
        return image;
    }

    /**
     * Сеттер для поля Image.
     * @param newImage - новое значение поля Image
     */
    public void setImage(final Blob newImage) {
        this.image = newImage;
    }

    /**
     * Геттер для поля кодировка изображения.
     * @return encoding - значение поля кодировка
     */
    public String getEncoding() {
        return encoding;
    }

    /**
     * Сеттер для поля Кодировка.
     * @param newEncoding - новое значение поля кодировка
     */
    public void setEncoding(final String newEncoding) {
        this.encoding = newEncoding;
    }

    /**
     * Переопределение метода расчета хэш-кода объекта.
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + advertismentId;
        int encodingKoef = 0;
        if (encoding != null) {
            encodingKoef = encoding.hashCode();
        }
        result = prime * result + encodingKoef;
        int fileNameKoef = 0;
        if (fileName != null) {
            fileNameKoef = fileName.hashCode();
        }
        result = prime * result + fileNameKoef;
        return result;
    }

    /**
     * Переопределение метода сравнения двух объектов
     *  класса.
     */
    @Override
    public boolean equals(final Object obj) {
        boolean result = true;
        if (this == obj) {
            result = true;
        }
        if (!super.equals(obj)) {
            result = false;
        }
        if (getClass() != obj.getClass()) {
            result = false;
        }
        final Image other = (Image) obj;
        if (advertismentId != other.advertismentId) {
            result = false;
        }
        if (encoding == null) {
            if (other.encoding != null) {
                result = false;
            }
        } else if (!encoding.equals(other.encoding)) {
            result = false;
        }
        if (fileName == null) {
            if (other.fileName != null) {
                result = false;
            }
        } else if (!fileName.equals(other.fileName)) {
            result = false;
        }
        return result;
    }


}

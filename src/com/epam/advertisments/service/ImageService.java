package com.epam.advertisments.service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.Base64;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;
import javax.sql.rowset.serial.SerialBlob;
import javax.sql.rowset.serial.SerialException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.epam.advertisments.dao.ImageDAO;
import com.epam.advertisments.model.Image;

/**
 * Класс с методами для считывания и обработки изображений.
 * @author Irina Mitsko
 *
 */
public final class ImageService {
    /**
     * Логгер для класса и классов-наследников.
     */
    private static final Logger LOGGER = (Logger) LogManager
            .getLogger(ImageService.class);
    /**
     * Размер массива байтов при считывании картинки.
     */
    private static final int BUFFER_SIZE = 1024;
    /**
     * Закрытый конструктор.
     */
    private ImageService() {
    }
    /**
     * Метод извлекает название файла загружаемой картинки.
     * @param part картинка, которая была получена в результате загрузки.
     * @return fileName - строка с названием файла
     */
    private static String extractFileName(final Part part) {
        final String contentDisp = part.getHeader("content-disposition");
        final String[] items = contentDisp.split(";");
        String fileName = "";
        for (final String s : items) {
            if (s.trim().startsWith("filename")) {
                String clientFileName = s.substring(s.indexOf("=")
                        + 2, s.length() - 1);
                clientFileName = clientFileName.replace("\\", "/");
                final int index = clientFileName.lastIndexOf('/');
                fileName = clientFileName.substring(index + 1);
            }
        }
        return fileName;
    }

    /**
     * Метод сохраняет картинку в массив байтов.
     * @param stream - объект InputStream
     * @return array - картинка в виде массива байтов
     * @throws IOException - ошибка чтения файла
     */
    private static byte[] readBytes(final InputStream stream)
            throws IOException {
        byte[] array = null;
        if (stream == null) {
            array = new byte[] {};
        }
        final byte[] buffer = new byte[BUFFER_SIZE];
        final ByteArrayOutputStream output = new ByteArrayOutputStream();
        try {
            int numRead = 0;
            while ((numRead = stream.read(buffer)) > -1) {
                output.write(buffer, 0, numRead);
                array = output.toByteArray();
            }
        } catch (IOException e) {
            LOGGER.error("Ошибка чтения данных" + e);
            final StackTraceElement[] trace = e.getStackTrace();
            for (final StackTraceElement el : trace) {
                LOGGER.error("\n " + el);
            }
        } finally {
            try {
                stream.close();
            } catch (IOException e) {
                LOGGER.error("Ошибка чтения данных" + e);
                final StackTraceElement[] trace = e.getStackTrace();
                for (final StackTraceElement el : trace) {
                    LOGGER.error("\n " + el);
                }
            }
        }
        output.flush();
        return array;

    }

    /**
     * Метод сохраняет картинки, загружаемые пользователями в базе данных.
     * @param request - объект HttpServletRequest
     * @param advertismrntId - уникальный ключ объявления,
     *  для которого загружаются картинки
     * @param dao - объект ImageDAO
     * @return result - количество загруженных и сохраненных в базе
     * данных картинок
     */
    public static int saveImage(final HttpServletRequest request,
            final int advertismrntId, final ImageDAO dao) {
        int result = 0;
        try {
            for (final Part part : request.getParts()) {
                final Image image = new Image();
                image.setAdvertismentId(advertismrntId);
                final String fileName = extractFileName(part);
                image.setFileName(fileName);
                if (fileName != null && fileName.length() > 0) {
                    final byte[] array = readBytes(part.getInputStream());
                    final String encode = Base64.getEncoder()
                            .encodeToString(array);
                    image.setEncoding(encode);
                    Blob blob = null;
                    try {
                        blob = new SerialBlob(array);
                    } catch (SerialException e) {
                        LOGGER.error("Ошибка" + e);
                        final StackTraceElement[] trace = e.getStackTrace();
                        for (final StackTraceElement el : trace) {
                            LOGGER.error("\n " + el);
                        }
                    } catch (SQLException e) {
                        LOGGER.error("Ошибка" + e);
                        final StackTraceElement[] trace = e.getStackTrace();
                        for (final StackTraceElement el : trace) {
                            LOGGER.error("\n " + el);
                        }
                    }
                    image.setImage(blob);
                    dao.save(image);
                    result++;
                }
            }
        } catch (IOException e) {
            LOGGER.error("Ошибка при работе с базой данных!" + e);
            final StackTraceElement[] trace = e.getStackTrace();
            for (final StackTraceElement el : trace) {
                LOGGER.error("\n " + el);
            }
        } catch (ServletException e) {
            LOGGER.error("Ошибка сервлета." + e);
            final StackTraceElement[] trace = e.getStackTrace();
            for (final StackTraceElement el : trace) {
                LOGGER.error("\n " + el);
            }
        }
        return result;
    }

}

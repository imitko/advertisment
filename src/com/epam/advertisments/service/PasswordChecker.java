package com.epam.advertisments.service;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


/**
 * Класс для шифрования строки в MD5.
 * @author Irina Mitsko
 */
public final class PasswordChecker {
    /**
     * Логгер для класса и классов-наследников.
     */
    private static final Logger LOGGER = (Logger) LogManager
            .getLogger(PasswordChecker.class);
    /**
     * Основание шестнадцатиричной системы.
     */
    private static final int HEX_BASE = 16;
    /**
     * Количество бит.
     */
    private static final int DIGITS = 32;

    /**
     * Закрытый конструктор.
     */
    private PasswordChecker() {
    }
    /**
     * Метод шифрует строку в кодировку MD5.
     * @param typedPassword - пароль, вводимый пользователем без кодировки.
     * @return hashtext - строка с шифрованным паролем
     */
    public static String hashPassword(final String typedPassword) {
        try {
            final MessageDigest mDigits = MessageDigest.getInstance("MD5");
            final byte[] messageDigest = mDigits.digest(typedPassword.getBytes());
            final BigInteger num = new BigInteger(1, messageDigest);
            String hashtext = num.toString(HEX_BASE);
            while (hashtext.length() < DIGITS) {
                hashtext = "0" + hashtext;
            }
            return hashtext;
        } catch (NoSuchAlgorithmException e) {
            LOGGER.error("Ошибка сервлета." + e);
            final StackTraceElement[] trace = e.getStackTrace();
            for (final StackTraceElement el : trace) {
                LOGGER.error("\n " + el);
            }
        }
        return typedPassword;

    }
}

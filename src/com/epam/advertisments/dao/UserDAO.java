package com.epam.advertisments.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import com.epam.advertisments.builder.Director;
import com.epam.advertisments.builder.UserBuilder;
import com.epam.advertisments.connection.MySQLConnUtils;
import com.epam.advertisments.model.User;

/**
 * Класс для взаимодействия объектов типа User с базой данных.
 * @author Irina Mitsko
 *
 */
public class UserDAO extends AbstractDAO<String, User> {
    /**
     * Запрос на добавление пользователя в базу данных.
     */
    private static final String INSERT_USER = "insert into USER (name,"
            + " email, phone, password, role) values(?,?,?,MD5(?),?)";
    /**
     * Запрос на поиск пользователя по его id.
     */
    private static final String SELECT_BY_ID = "SELECT id, email, name,"
            + " phone, role from USER WHERE id = ";
    /**
     * Запрос на поиск пользователя по емейлу.
     */
    private static final String SELECT_BY_EMAIL = "SELECT id, name, phone,"
            + " password, role from USER WHERE email = ?";
    /**
     * Запрос на выборку всех пользователей из базы данных.
     */
    private static final String SELECT_ALL = "SELECT id, email, name, phone,"
            + " password, role from USER";
    /**
     * Запрос на удаление пользователя.
     */
    private static final String DELETE_USER = "DELETE FROM user WHERE id =";

    /**
     * Запрос для обновления своих персональных данных пользователем.
     */
    private static final String UPDATE = "UPDATE user SET name =?, phone=?,"
            + " password= MD5(?), email=? WHERE id = ?";
    /**
     * Запрос на обновление персональных данных пользователя администратором.
     */
    private static final String UPDATE_ADMIN = "UPDATE user SET name =?, phone=?, email=? WHERE id = ?";
    /**
     * Конструктор с параметром.
     * @param conn - сcылка на объект соединения.
     */
    public UserDAO(final Connection conn) {
        super(conn);
    }
    /**
     * Метод сохраняет нового пользователя в базе данных.
     */
    @Override
    public int save(final User user) {
        int result = 0;
        PreparedStatement prStatement = null;
        try {
            final User finduser = findEntity(user.getEmail());
            if (finduser.getEmail() == null) {
                prStatement = connection.prepareStatement(INSERT_USER);
                prStatement.setString(1, user.getName());
                prStatement.setString(2, user.getEmail());
                prStatement.setString(3, user.getPhone());
                prStatement.setString(4, user.getPassword());
                prStatement.setString(5, user.getRole().getUserProfileType());
                result = prStatement.executeUpdate();
            }
        } catch (SQLException e) {
            LOGGER.error("Ошибка!" + e);
            final StackTraceElement[] trace = e.getStackTrace();
            for (final StackTraceElement el : trace) {
                LOGGER.error("\n " + el);
            }
        } finally {
            MySQLConnUtils.closeStatement(prStatement);
        }
        return result;
    }

    /**
     * Метод ищет пользователя по емейлу.
     */
    @Override
    public User findEntity(final String email) {
        final User user = new User();
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(SELECT_BY_EMAIL);
            statement.setString(1, email);
            resultSet = statement.executeQuery();
            while(resultSet.next()) {
                user.setEmail(email);
                Director.createEntity(new UserBuilder(), user, resultSet);
            }
        } catch (SQLException e) {
            LOGGER.error("Ошибка!" + e);
            final StackTraceElement[] trace = e.getStackTrace();
            for (final StackTraceElement el : trace) {
                LOGGER.error("\n " + el);
            }
        } finally {
            MySQLConnUtils.closeStatement(statement);
        }
        return user;
    }

    /**
     * Метод удаляет пользователя по его id.
     */
    @Override
    public User delete(final int userId) {
        User user = findEntityById(userId);
        Statement statement = null;
        try {
            statement = connection.createStatement();
            statement.executeUpdate(DELETE_USER + userId);
        } catch (SQLException e) {
            LOGGER.error("Ошибка!" + e);
            final StackTraceElement[] trace = e.getStackTrace();
            for (final StackTraceElement el : trace) {
                LOGGER.error("\n " + el);
            }
        } finally {
            MySQLConnUtils.closeStatement(statement);
        }
        return user;
    }

    /**
     * Реализация этого метода не нужна для логики работы с
     *  классом User.
     */
    @Override
    public boolean delete(final User entity) {
        return false;
    }
    
    /**
     * Метод обновляет данные существующего пользователя в базе данных.
     */
    @Override
    public boolean update(final User entity) {
        int result = 0;
        boolean isUpdated = false;
        PreparedStatement statement = null;
        try {
            if (entity.getPassword() != null) {
                statement = connection.prepareStatement(UPDATE);
                statement.setString(1, entity.getName());
                statement.setString(2, entity.getPhone());
                statement.setString(3, entity.getPassword());
                statement.setString(4, entity.getEmail());
                statement.setInt(5, entity.getId());
                result = statement.executeUpdate();
            } else {
                statement = connection.prepareStatement(UPDATE_ADMIN);
                statement.setString(1, entity.getName());
                statement.setString(2, entity.getPhone());
                statement.setString(3, entity.getEmail());
                statement.setInt(4, entity.getId());
                result = statement.executeUpdate();
            }
            if(result > 0) {
                isUpdated = true;
            }
        } catch (SQLException e) {
            LOGGER.error("Ошибка!" + e);
            final StackTraceElement[] trace = e.getStackTrace();
            for (final StackTraceElement el : trace) {
                LOGGER.error("\n " + el);
            }
        } finally {
            MySQLConnUtils.closeStatement(statement);
        }
        return isUpdated;
    }

    /**
     * Метод находит всех пользователей в базе данных.
     */
    @Override
    public List<User> findAll() {
        final List<User> allUsers = new CopyOnWriteArrayList<>();
        Statement statement = null;
        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery(SELECT_ALL);
            while (resultSet.next()) {
                final User user = new User();
                Director.createEntity(new UserBuilder(), user, resultSet);
                allUsers.add(user);
            }
        } catch (SQLException e) {
            LOGGER.error("Ошибка!" + e);
            final StackTraceElement[] trace = e.getStackTrace();
            for (final StackTraceElement el : trace) {
                LOGGER.error("\n " + el);
            }
        } finally {
            MySQLConnUtils.closeStatement(statement);
        }
        return allUsers;
    }

    /**
     * Реализация этого метода не нужна для логики работы с
     *  классом User.
     */
    @Override
    public List<User> findAll(final String parameter) {
        return new ArrayList<>();
    }

    /**
     * Метод находит пользователя в базе данных по его id.
     * @param userId - уникальный ключ пользователя
     * @return user - полученный объект User
     */
    public User findEntityById(final int userId) {
        final User user = new User();
        Statement statement = null;
        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery(SELECT_BY_ID
                    + userId);
            while (resultSet.next()) {
                user.setId(userId);
                Director.createEntity(new UserBuilder(), user, resultSet);
            }
        } catch (SQLException e) {
            LOGGER.error("Ошибка!" + e);
            final StackTraceElement[] trace = e.getStackTrace();
            for (final StackTraceElement el : trace) {
                LOGGER.error("\n " + el);
            }
        } finally {
            MySQLConnUtils.closeStatement(statement);
        }
        return user;

    }
    
   

}

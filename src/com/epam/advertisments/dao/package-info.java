/**
 * Пакет с классами для взаимодействия между базой данных
 *  и классами-сущностями.
 */
/**
 * @author Irina Mitsko
 *
 */
package com.epam.advertisments.dao;

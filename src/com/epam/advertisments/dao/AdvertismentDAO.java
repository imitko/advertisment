package com.epam.advertisments.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import com.epam.advertisments.builder.AdvertismentBuilder;
import com.epam.advertisments.builder.Director;
import com.epam.advertisments.connection.MySQLConnUtils;
import com.epam.advertisments.model.Advertisment;

/**
 * Класс для взаимодействия объектов типа Advertisment с базой данных.
 * @author Irina Mitsko
 */
public class AdvertismentDAO extends AbstractDAO<Integer, Advertisment> {
    /**
     * Запрос для сохранения нового объявления в базе.
     */
    private static final String INSERT_ADVERTISMENT = "INSERT INTO advertisment"
            + " (user_id, category_id, title, text, price, start_date,"
            + " end_date) VALUES (?,?,?,?,?,?,?)";
    /**
     * Запрос на поиск объявления по id.
     */
    private static final String SELECT_BY_ID = "SELECT id, user_id,"
            + " category_id, title, text, price, start_date, end_date,"
            + " is_active, is_moderated from advertisment WHERE id = ";
    /**
     * Запрос на удаления объявления.
     */
    private static final String DELETE = "DELETE FROM advertisment WHERE id = ";
    /**
     * Запрос на поиск всех объявлений пользователя.
     */
    private static final String SELECT_ALL_BY_USER = "SELECT id, user_id,"
            + " category_id, title, text, price, start_date, end_date,"
            + " is_active, is_moderated FROM advertisment WHERE user_id =";
    /**
     * Запрос на вывод всех объявлений из раздела.
     */
    private static final String SELECT_BY_CATEGORY = "SELECT id, user_id,"
            + " category_id, title, text, price, start_date, end_date,"
            + " is_active, is_moderated FROM advertisment WHERE "
            + "is_active = 1 AND is_moderated = 1 AND category_id =";
    /**
     * запрос на поиск последнего добавленного пользователем объявления.
     */
    private static final String LAST_UPLOADED = "SELECT max(id) as maximum"
            + " FROM advertisment WHERE user_id = ";
    /**
     * Запрос на выборку всех объявлений ожидающих модерацию.
     */
    private static final String SELECT_NOT_MODERATED = "SELECT id,"
            + " user_id, category_id, title, text, price, start_date,"
            + " end_date, is_moderated, is_active FROM advertisment WHERE"
            + " is_moderated = 0";
    /**
     * Запрос на активирование объявления модератором.
     */
    private static final String MODERATE = "UPDATE advertisment SET"
            + " is_active= 1, is_moderated=1 WHERE id = ";
    /**
     * Запрос на выборку последних 10 добавленных объявлений для
     *  титульной страницы.
     */
    private static final String LAST_TEN = "SELECT id, user_id, category_id,"
            + " title, text, price, start_date, end_date, is_moderated,"
            + " is_active FROM advertisment WHERE is_active=1 ORDER BY id"
            + " DESC LIMIT 10";
    /**
     * Запрос на поиск объявления по ключевому слову в заголовке
     *  или тексте начало.
     */
    private static final String SEARCH = "SELECT id, user_id,"
            + " category_id, title, text, price, start_date, end_date,"
            + " is_moderated, is_active FROM advertisment WHERE"
            + " category_id =? AND title LIKE ? OR text LIKE ?";

    /**
     * Часть запроса на сортировку по убыванию id.
     */
    private static final String ORDER = " ORDER BY id DESC";
    
    /**
     * Конструктор с параметром.
     * @param conn - сылка на объект соединения.
     */
    public AdvertismentDAO(Connection conn) {
        super(conn);
    }
    
    /**
     *  Метод, сохраняющий объявление в базе данных.
     *  @param advertisment - объявление для сохранения
     *  @return result - если объявление сохранено, возвращается 1,
     *  в противном случае - 0
     */
    @Override
    public int save(final Advertisment advertisment) {
        PreparedStatement ps = null;
        int result = 0;
        try {
            ps = connection.prepareStatement(INSERT_ADVERTISMENT);
            ps.setString(1, advertisment.getUserId() + "");
            ps.setString(2, advertisment.getCategoryId() + "");
            ps.setString(3, advertisment.getTitle());
            ps.setString(4, advertisment.getText());
            ps.setString(5, advertisment.getPrice() + "");
            ps.setString(6, advertisment.getStartDate() + "");
            ps.setString(7, advertisment.getEndDate() + "");
            result = ps.executeUpdate();
        } catch (SQLException e) {
            LOGGER.error("Ошибка!" + e);
            final StackTraceElement[] trace = e.getStackTrace();
            for (final StackTraceElement el : trace) {
                LOGGER.error("\n " + el);
            }
        } finally {
            MySQLConnUtils.closeStatement(ps);
        }
        return result;
    }

    /**
     * Метод для поиска объявления в базе по его id.
     * @param advId - id объявления, по которому ищется объявление
     */
    @Override
    public Advertisment findEntity(final Integer advId) {
        final Advertisment advertisment = new Advertisment();
        Statement statement = null;
        try {
            statement = connection.createStatement();
            resultSet
                = statement.executeQuery(SELECT_BY_ID + advId);
            while (resultSet.next()) {
                advertisment.setId(advId);
                Director.createEntity(new AdvertismentBuilder(),
                        advertisment, resultSet);
            }
        } catch (SQLException e) {
            LOGGER.error("Ошибка!" + e);
            final StackTraceElement[] trace = e.getStackTrace();
            for (final StackTraceElement el : trace) {
                LOGGER.error("\n " + el);
            }
        } finally {
            MySQLConnUtils.closeStatement(statement);
        }
        return advertisment;
    }

    /**
     * Метод удаляет объявление из бд по его id.
     * @param advId - id объявления, которое будет удалено
     * @return adv - объявление, которое удалено, а если не
     *  удалено - то пустое объявление
     */
    @Override
    public Advertisment delete(final int advId) {
        Advertisment adv = new Advertisment();
        Statement statement = null;
        try {
            statement = connection.createStatement();
            adv = findEntity(advId);
            statement.executeUpdate(DELETE + advId);
        } catch (SQLException e) {
            LOGGER.error("Ошибка!" + e);
            final StackTraceElement[] trace = e.getStackTrace();
            for (final StackTraceElement el : trace) {
                LOGGER.error("\n " + el);
            }
        } finally {
            MySQLConnUtils.closeStatement(statement);
        }
        return adv;
    }

    /**
     * Метод обновления данных существующего объявления.
     * @param entity - объявление, которое надо удалить
     * @return - true
     */
    @Override
    public boolean update(final Advertisment entity) {
        int result = 0;
        boolean isUpdated = false;
        String updateSQL = "UPDATE advertisment SET title = \""
        + entity.getTitle() + "\",text=\"" + entity.getText()
                + "\", price= \"" + entity.getPrice() + "\","
                + " category_id= \"" + entity.getCategoryId()
                + "\", is_active = 0, is_moderated = 0 WHERE id = "
                + entity.getId();
        Statement statement = null;
        try {
            statement = connection.createStatement();
            locker.lock();
            result = statement.executeUpdate(updateSQL);
            if (result > 0) {
                isUpdated = true;
            }
        } catch (SQLException e) {
            LOGGER.error("Ошибка!" + e);
            final StackTraceElement[] trace = e.getStackTrace();
            for (final StackTraceElement el : trace) {
                LOGGER.error("\n " + el);
            }
        } finally {
            locker.unlock();
            MySQLConnUtils.closeStatement(statement);
        }
        return isUpdated;
    }

    /**
     * Метод для поиска всех объявлений, ожидающих модерацию.
     * @return advertisments - список объявлений
     */
    @Override
    public List<Advertisment> findAll() {
        final List<Advertisment> advertisments
            = new ArrayList<>();
        Statement statement = null;
        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery(SELECT_NOT_MODERATED);
            while (resultSet.next()) {
                final Advertisment advert = new Advertisment();
                final int advId = Integer.parseInt(resultSet.getString("id"));
                advert.setId(advId);
                Director.createEntity(new AdvertismentBuilder(),
                        advert, resultSet);
                advertisments.add(advert);
            }
        } catch (SQLException e) {
            LOGGER.error("Ошибка!" + e);
            final StackTraceElement[] trace = e.getStackTrace();
            for (final StackTraceElement el : trace) {
                LOGGER.error("\n " + el);
            }
        } finally {
            MySQLConnUtils.closeStatement(statement);
        }
        return advertisments;
    }

    /**
     * Метод выбирает все объявления по конкоретному пользователю.
     * @param userId - id пользователя, по которому делается выборка
     *  размещенных объявлений
     *  @return advertisments - список объявлений
     */
    @Override
    public List<Advertisment> findAll(final Integer userId) {
        final List<Advertisment> advertisments = new ArrayList<>();
        Statement statement = null;
        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery(SELECT_ALL_BY_USER
                    + userId);
            while (resultSet.next()) {
                final Advertisment advert = new Advertisment();
                final int advId = Integer.parseInt(resultSet.getString("id"));
                advert.setId(advId);
                Director.createEntity(new AdvertismentBuilder(),
                        advert, resultSet);
                advertisments.add(advert);
            }
        } catch (SQLException e) {
            LOGGER.error("Ошибка!" + e);
            final StackTraceElement[] trace = e.getStackTrace();
            for (final StackTraceElement el : trace) {
                LOGGER.error("\n " + el);
            }
        } finally {
            MySQLConnUtils.closeStatement(statement);
        }
        return advertisments;
    }

    /**
     * Метод находит последнее загруженное пользователем объявление.
     * @param userId - id пользователя
     * @return id - уникальный ключ объявления
     */
    public int findLastUploadedEntity(final int userId) {
        int idMax = 0;
        Statement statement = null;
        try {
            statement = connection.createStatement();
            resultSet
                = statement.executeQuery(LAST_UPLOADED + userId);
            while (resultSet.next()) {
                idMax = Integer.parseInt(resultSet.getString("maximum"));
            }
        } catch (SQLException e) {
            LOGGER.error("Ошибка!" + e);
            final StackTraceElement[] trace = e.getStackTrace();
            for (final StackTraceElement el : trace) {
                LOGGER.error("\n " + el);
            }
        } finally {
            MySQLConnUtils.closeStatement(statement);
        }
        return idMax;
    }

    /**
     * Метод находит все объявления раздела.
     * @param ctegoryId - ункальный id раздела
     * @return advertisments - список объявлений
     */
    public List<Advertisment> findAllFromCategory(final Integer
            ctegoryId) {
        final List<Advertisment> advertisments
            = new ArrayList<>();
        Statement statement = null;
        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery(SELECT_BY_CATEGORY
                    + ctegoryId + ORDER);
            while (resultSet.next()) {
                final Advertisment advert = new Advertisment();
                final int newId = Integer.parseInt(resultSet.getString("id"));
                advert.setId(newId);
                Director.createEntity(new AdvertismentBuilder(),
                        advert, resultSet);
                advertisments.add(advert);
            }
        } catch (SQLException e) {
            LOGGER.error("Ошибка!" + e);
            final StackTraceElement[] trace = e.getStackTrace();
            for (final StackTraceElement el : trace) {
                LOGGER.error("\n " + el);
            }
        } finally {
            MySQLConnUtils.closeStatement(statement);
        }
        return advertisments;
    }

    /**
     * Метод для продления срока размещения объявления.
     * @param id - уникальный ключ продлеваемого объявления
     * @param startDate - дата с которой продлевается объявления (текущая)
     * @param endDate - дата до которой продлевается объявление
     * @return result - результат операции: 1 - если продлено, 0 если нет
     */
    public int extend(final int id, final String startDate,
            final String endDate) {
        Statement stmt = null;
        int result = 0;
        final String sql = "UPDATE advertisment SET start_date=\""
        + startDate + "\", end_date= \"" + endDate
                + "\" WHERE id = " + id;
        try {
            stmt = connection.createStatement();
            result = stmt.executeUpdate(sql);
        } catch (SQLException e) {
            LOGGER.error("Ошибка!" + e);
            final StackTraceElement[] trace = e.getStackTrace();
            for (final StackTraceElement el : trace) {
                LOGGER.error("\n " + el);
            }
        } finally {
            MySQLConnUtils.closeStatement(stmt);
        }
        return result;
    }

    /**
     * Метод для активации объявления модератором.
     * @param advertismentId - уникальный ключ объявления
     * @return result - результат операции: 1 - если активировано,
     *  0 если нет
     */
    public int activate(final int advertismentId) {
        int result = 0;
        Statement statement = null;
        try {
            statement = connection.createStatement();
            result = statement.executeUpdate(MODERATE + advertismentId);
        } catch (SQLException e) {
            LOGGER.error("Ошибка!" + e);
            final StackTraceElement[] trace = e.getStackTrace();
            for (final StackTraceElement el : trace) {
                LOGGER.error("\n " + el);
            }
        } finally {
            MySQLConnUtils.closeStatement(statement);
        }
        return result;
    }

    /**
     * Метод поиска 10 новых объявлений в базе данных (по id).
     * @return advertisments - список объявлений
     */
    public List<Advertisment> findLastTen() {
        final List<Advertisment> advertisments
            = new ArrayList<>();
        Statement statement = null;
        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery(LAST_TEN);
            while (resultSet.next()) {
                final Advertisment advert = new Advertisment();
                final int advId = Integer.parseInt(resultSet.getString("id"));
                advert.setId(advId);
                Director.createEntity(new AdvertismentBuilder(),
                        advert, resultSet);
                advertisments.add(advert);
            }
        } catch (SQLException e) {
            LOGGER.error("Ошибка!" + e);
            final StackTraceElement[] trace = e.getStackTrace();
            for (final StackTraceElement el : trace) {
                LOGGER.error("\n " + el);
            }
        } finally {
            MySQLConnUtils.closeStatement(statement);
        }
        return advertisments;
    }
    
    /**
     * Метод для поиска объявления(ий) по ключевому слову в
     *  заголовке или тексте внутри раздела, в котором находится пользователь.
     * @param catId - уникальный ключ раздела
     * @param searchedText - ключевое слово или фраза
     * @return advertisments - список найденных объявлений
     */
    public List<Advertisment> findByKeyWords(final int catId,
            final String searchedText) {
        final List<Advertisment> advertisments
            = new ArrayList<>();
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(SEARCH);
            statement.setInt(1, catId);
            statement.setString(2, "%" + searchedText + "%");
            statement.setString(3, "%" + searchedText + "%");
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                final Advertisment advert = new Advertisment();
                final int advId = Integer.parseInt(resultSet.getString("id"));
                advert.setId(advId);
                Director.createEntity(new AdvertismentBuilder(),
                        advert, resultSet);
                advertisments.add(advert);
            }
        } catch (SQLException e) {
            LOGGER.error("Ошибка! Несоответствие типа данных полей." + e);
            final StackTraceElement[] trace = e.getStackTrace();
            for (final StackTraceElement el : trace) {
                LOGGER.error("\n " + el);
            }
        } finally {
            MySQLConnUtils.closeStatement(statement);
        }
        return advertisments;
    }

    /**
     * Этот метод не использован для данного класса.
     */
    @Override
    public boolean delete(final Advertisment entity) {
        throw new UnsupportedOperationException();
    }

}

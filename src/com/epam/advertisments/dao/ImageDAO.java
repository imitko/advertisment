package com.epam.advertisments.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import com.epam.advertisments.builder.Director;
import com.epam.advertisments.builder.ImageBuilder;
import com.epam.advertisments.connection.MySQLConnUtils;
import com.epam.advertisments.model.Image;

/**
 * Класс для взаимодействия объектов типа Image с базой данных.
 * @author Irina Mitsko
 *
 */
public class ImageDAO extends AbstractDAO<Integer, Image> {
    /**
     * Запрос на вставку картинки в базу данных.
     */
    private static final String INSERT_IMAGE = "Insert into images"
            + " (advertisment_id, file_name, image, encoding) "
            + " values (?,?,?,?) ";
    /**
     * Запрос на поиск картинки из базы данных по ее id.
     */
    private static final String SELECT_BY_ID = "SELECT id, advertisment_id,"
            + " image, file_name, encoding from images WHERE id =";
    /**
     * Запрос на удаление картинки из базы данных по ее id.
     */
    private static final String DELETE_IMG = "DELETE FROM images WHERE id = ";
    /**
     * Запрос на выбрку всех картинок по объявлению.
     */
    private static final String SELECT_BY_ADVERTISMENT = "SELECT id,"
            + " advertisment_id, image, file_name, encoding FROM images"
            + " WHERE advertisment_id =";

    /**
     * Конструктор с параметром.
     * @param conn - сылка на объект соединения.
     */
    public ImageDAO(final Connection conn) {
        super(conn);
    }
    /**
     * Метод сохранения картинки в базе данных.
     */
    @Override
    public int save(final Image entity) {
        int result = 0;
        PreparedStatement pstm = null;
        try {
            pstm = connection.prepareStatement(INSERT_IMAGE);
            pstm.setString(1, entity.getAdvertismentId() + "");
            pstm.setString(2, entity.getFileName());
            pstm.setBlob(3, entity.getImage());
            pstm.setString(4, entity.getEncoding());
            result = pstm.executeUpdate();
        } catch (SQLException e) {
            LOGGER.error("Ошибка!" + e);
            final StackTraceElement[] trace = e.getStackTrace();
            for (final StackTraceElement el : trace) {
                LOGGER.error("\n " + el);
            }
        } finally {
            MySQLConnUtils.closeStatement(pstm);
        }
        return result;
    }

    /**
     * Метод поиска картинки в базе по ее id.
     */
    @Override
    public Image findEntity(final Integer imgIg) {
        final Image image = new Image();
        Statement statement = null;
        try {
            statement = connection.createStatement();
            resultSet
                = statement.executeQuery(SELECT_BY_ID + imgIg);
            while (resultSet.next()) {
                image.setId(imgIg);
                Director.createEntity(new ImageBuilder(), image, resultSet);
            }
        } catch (SQLException e) {
            LOGGER.error("Ошибка!" + e);
            final StackTraceElement[] trace = e.getStackTrace();
            for (final StackTraceElement el : trace) {
                LOGGER.error("\n " + el);
            }
        } finally {
            MySQLConnUtils.closeStatement(statement);
        }

        return image;
    }

    /**
     * Метод удаления картинки из базы данных.
     */
    @Override
    public Image delete(final int imgId) {
        int result = 0;
        final Image img = findEntity(imgId);
        Statement statement = null;
        try {
            statement = connection.createStatement();
            result = statement.executeUpdate(DELETE_IMG + imgId);
            if (result > 0) {
                return img;
            }
        } catch (SQLException e) {
            LOGGER.error("Ошибка!" + e);
            final StackTraceElement[] trace = e.getStackTrace();
            for (final StackTraceElement el : trace) {
                LOGGER.error("\n " + el);
            }
        } finally {
            MySQLConnUtils.closeStatement(statement);
        }
        return new Image();

    }

    /**
     * Метод поиска всех картинок по объявлению.
     */
    @Override
    public List<Image> findAll(final Integer advertismentId) {
        List<Image> images = new CopyOnWriteArrayList<>();
        Statement statement = null;
        try {
            statement = connection.createStatement();
            resultSet
                = statement.executeQuery(SELECT_BY_ADVERTISMENT + advertismentId);
            while (resultSet.next()) {
                final Image image = new Image();
                image.setAdvertismentId(advertismentId);
                Director.createEntity(new ImageBuilder(), image, resultSet);
                images.add(image);
            }
        } catch (SQLException e) {
            LOGGER.error("Ошибка!" + e);
            final StackTraceElement[] trace = e.getStackTrace();
            for (final StackTraceElement el : trace) {
                LOGGER.error("\n " + el);
            }
        } finally {
            MySQLConnUtils.closeStatement(statement);
        }
        return images;
    }

    /**
     * Метод пока не используется.
     */
    @Override
    public boolean delete(final Image entity) {
        throw new UnsupportedOperationException();
    }

    /**
     * Метод пока не используется.
     */
    @Override
    public boolean update(final Image entity) {
        throw new UnsupportedOperationException();
    }

    /**
     * Метод пока не используется.
     */
    @Override
    public List<Image> findAll() {
        throw new UnsupportedOperationException();
    }

}

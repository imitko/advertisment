package com.epam.advertisments.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import com.epam.advertisments.builder.Director;
import com.epam.advertisments.builder.MessageBuilder;
import com.epam.advertisments.connection.MySQLConnUtils;
import com.epam.advertisments.model.Message;

/**
 * Класс для взаимодействия объектов типа Message с базой данных.
 * @author irina Mitsko
 *
 */
public class MessageDAO extends AbstractDAO<Integer, Message> {
    /**
     * Запрос на сохранение нового сообщения в базе.
     */
    private final static String INSERT_MESSAGE = "INSERT INTO message"
            + " (sender_id, receiver_id, message_text, advertisment_id, date) "
            + "VALUES (?,?,?,?,?)";
    /**
     * Запрос на поиск сообщения по id.
     */
    private static final String SELECT_BY_ID = "SELECT id, sender_id,"
            + " receiver_id, message_text, advertisment_id, date"
            + " from message WHERE id = ";
    /**
     * Запрос на поиск сообщения по id получателя.
     */
    private static final String SELECT_BY_RECEIV = "SELECT id, sender_id,"
            + " receiver_id, message_text, advertisment_id, date FROM message"
            + " WHERE receiver_id =";
    /**
     * Запрос на поиск сообщения по отправителю.
     */
    private static final String SELECT_BY_SENDER = "SELECT id, sender_id,"
            + " receiver_id, message_text, advertisment_id, date FROM message"
            + " WHERE sender_id =";

    /**
     * Запрос на вывод истории переписки к сообщению пользователя.
     */
    private static final String HISTORY = "SELECT id, date, message_text,"
            + " sender_id FROM message"
            + " WHERE advertisment_id = ? AND receiver_id = ? AND sender_id = ?"
            + " AND id < ? UNION SELECT id, date, message_text, sender_id FROM"
            + " message WHERE advertisment_id = ? AND sender_id = ?"
            + " and receiver_id = ? AND id < ? ORDER BY id DESC";
    /**
     * Конструктор с параметром.
     * @param conn - сcылка на объект соединения.
     */
    public MessageDAO(final Connection conn) {
        super(conn);
    }
    
    /**
     * Метод для сохранения нового сообщения в базе.
     */
    @Override
    public int save(final Message message) {
        PreparedStatement ps = null;
        int result = 0;
        try {
            ps = connection.prepareStatement(INSERT_MESSAGE);
            ps.setString(1, message.getSenderId() + "");
            ps.setString(2, message.getReceiverId() + "");
            ps.setString(3, message.getMessage());
            ps.setString(4, message.getAdvertismentId() + "");
            ps.setString(5, message.getDate() + "");
            result = ps.executeUpdate();
        } catch (SQLException e) {
            LOGGER.error("Ошибка!" + e);
            final StackTraceElement[] trace = e.getStackTrace();
            for (final StackTraceElement el : trace) {
                LOGGER.error("\n " + el);
            }
        } finally {
            MySQLConnUtils.closeStatement(ps);
        }
        return result;
    }

    /**
     * Метод поиска сообщения по его id.
     */
    @Override
    public Message findEntity(final Integer messageId) {
        final Message message = new Message();
        Statement statement = null;
        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery(SELECT_BY_ID
                    + messageId);
            while (resultSet.next()) {
                message.setId(messageId);
                Director.createEntity(new MessageBuilder(), message, resultSet);
            }
        } catch (SQLException e) {
            LOGGER.error("Ошибка! Несоответствие типа данных полей." + e);
            final StackTraceElement[] trace = e.getStackTrace();
            for (final StackTraceElement el : trace) {
                LOGGER.error("\n " + el);
            }
        } finally {
            MySQLConnUtils.closeStatement(statement);
        }
        return message;
    }

    /**
     *  Метод поиска всех объявлений.
     */
    @Override
    public List<Message> findAll(final Integer receiverId) {
        final List<Message> allMessages = new ArrayList<>();
        allMessages.addAll(findReceivedMessages(receiverId));
        allMessages.addAll(findSentMessages(receiverId));
        return allMessages;
    }

    /**
     * Метод поиска входящих сообщений пользователя.
     * @param receiverId - уникальный ключ получателя сообщения
     * @return usersMessages - список сообщений
     */
    public List<Message> findReceivedMessages(final Integer receiverId) {

        final List<Message> usersMessages = new ArrayList<>();
        final String sql = SELECT_BY_RECEIV + receiverId +
                " ORDER BY id DESC";
        Statement statement = null;
        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                final Message message = new Message();
                Director.createEntity(new MessageBuilder(), message, resultSet);
                usersMessages.add(message);
            }
        } catch (SQLException e) {
            LOGGER.error("Ошибка! Несоответствие типа данных полей." + e);
            final StackTraceElement[] trace = e.getStackTrace();
            for (final StackTraceElement el : trace) {
                LOGGER.error("\n " + el);
            }
        } finally {
            MySQLConnUtils.closeStatement(statement);
        }

        return usersMessages;
    }

    /**
     * Метод поиска исходящих сообщений пользователя.
     * @param senderId - уникальный ключ отправителя сообщения
     * @return usersMessages - список сообщений
     */
    public List<Message> findSentMessages(final Integer senderId) {
        Statement statement = null;
        final List<Message> usersMessages = new ArrayList<>();
        final String sql = SELECT_BY_SENDER + senderId + " ORDER BY id DESC";
        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                final Message message = new Message();
                Director.createEntity(new MessageBuilder(), message, resultSet);
                usersMessages.add(message);
            }
        } catch (SQLException e) {
            LOGGER.error("Ошибка! Несоответствие типа данных полей." + e);
            final StackTraceElement[] trace = e.getStackTrace();
            for (final StackTraceElement el : trace) {
                LOGGER.error("\n " + el);
            }
        } finally {
            MySQLConnUtils.closeStatement(statement);
        }
        return usersMessages;
    }
    
    /**
     * Метод поиска истории переписки для читаемого сообщения.
     * @param receivedMessage - уникальный ключ сообщения, по
     *  которому необходимо найти перениску
     * @return historyMessages - список сообщений из истории
     */
    public List<Message> findHistoryMessages(final Message receivedMessage) {
        final List<Message> historyMessages = new ArrayList<>();
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(HISTORY);
            statement.setInt(1, receivedMessage.getAdvertismentId());
            statement.setInt(2, receivedMessage.getReceiverId());
            statement.setInt(3, receivedMessage.getSenderId());
            statement.setInt(4, receivedMessage.getId());
            statement.setInt(5, receivedMessage.getAdvertismentId());
            statement.setInt(6, receivedMessage.getReceiverId());
            statement.setInt(7, receivedMessage.getSenderId());
            statement.setInt(8, receivedMessage.getId());
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                final Message message = new Message();
                message.setAdvertismentId(receivedMessage.getAdvertismentId());
                message.setReceiverId(receivedMessage.getReceiverId());
                Director.createEntity(new MessageBuilder(), message, resultSet);
                historyMessages.add(message);
            }
        } catch (SQLException e) {
            LOGGER.error("Ошибка! Несоответствие типа данных полей." + e);
            final StackTraceElement[] trace = e.getStackTrace();
            for (final StackTraceElement el : trace) {
                LOGGER.error("\n " + el);
            }
        } finally {
            MySQLConnUtils.closeStatement(statement);
        }
        return historyMessages;
    }

    /**
     * Реализация этого метода не нужна для логики работы с
     *  классом Message.
     */
    @Override
    public Message delete(final int messageId) {
        return new Message();
    }

    /**
     * Реализация этого метода не нужна для логики работы с
     *  классом Message.
     */
    @Override
    public boolean delete(final Message entity) {
        throw new UnsupportedOperationException();
    }

    /**
     * Реализация этого метода не нужна для логики работы с
     *  классом Message.
     */
    @Override
    public boolean update(final Message entity) {
        throw new UnsupportedOperationException();
    }

    /**
     * Реализация этого метода не нужна для логики работы с
     *  классом Message.
     */
    @Override
    public List<Message> findAll() {
        throw new UnsupportedOperationException();
    }

}

package com.epam.advertisments.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import com.epam.advertisments.builder.CategoryBuilder;
import com.epam.advertisments.builder.Director;
import com.epam.advertisments.connection.MySQLConnUtils;
import com.epam.advertisments.model.Category;
/**
 * Класс для взаимодействия объектов типа Сategory с базой данных.
 * @author Irina Mitsko
 *
 */
public class CategoryDAO extends AbstractDAO<Integer, Category> {
    /**
     * Запрос для поиска раздела по id.
     */
    private static final String SELECT_BY_ID = "SELECT id, category"
            + " from categories WHERE id = ";
    /**
     * Запрос для поиска списка всех разделов.
     */
    private static final String FIND_ALL = "SELECT id, category FROM"
            + " categories";
    /**
     * Запрос для добавления нового раздела.
     */
    private static final String NEW_CATEGORY = "insert into categories"
            + " (category) values(?)";
    /**
     * Запрос для удаления раздела.
     */
    private static final String DELETE_CAT = "DELETE FROM categories"
            + " WHERE id = ";

    /**
     * Конструктор с параметром.
     * @param conn - сылка на объект соединения.
     */
    public CategoryDAO(final Connection conn) {
        super(conn);
    }
    /**
     * Метод сохраняет новый раздел в базе данных.
     */
    @Override
    public int save(final Category category) {
        int result = 0;
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(NEW_CATEGORY);
            statement.setString(1, category.getName());
            result = statement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.error("Ошибка!" + e);
            final StackTraceElement[] trace = e.getStackTrace();
            for (final StackTraceElement el : trace) {
                LOGGER.error("\n " + el);
            }
        } finally {
            MySQLConnUtils.closeStatement(statement);
        }

        return result;
    }

    /**
     * Метод для поиска раздела по его уникальному ключу.
     */
    @Override
    public Category findEntity(final Integer categoryId) {
        final Category category = new Category();
        Statement statement = null;
        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery(SELECT_BY_ID
                    + categoryId);
            while (resultSet.next()) {
                category.setId(categoryId);
                Director.createEntity(new CategoryBuilder(),
                        category, resultSet);
            }
        } catch (SQLException e) {
            LOGGER.error("Ошибка!" + e);
            final StackTraceElement[] trace = e.getStackTrace();
            for (final StackTraceElement el : trace) {
                LOGGER.error("\n " + el);
            }
        }
        return category;
    }

    /**
     * Метод для удаления раздела из базы данных.
     */
    @Override
    public Category delete(final int categoryId) {
        int result = 0;
        Category category = findEntity(categoryId);
        Statement statement = null;
        try {
            statement = connection.createStatement();
            result = statement.executeUpdate(DELETE_CAT + categoryId);
        } catch (SQLException e) {
            LOGGER.error("Ошибка!" + e);
            final StackTraceElement[] trace = e.getStackTrace();
            for (final StackTraceElement el : trace) {
                LOGGER.error("\n " + el);
            }
        }
        if(result == 0) {
            category = new Category();
        }
        return category;
    }

    /**
     * Метод не неужен для работы логиги разделов.
     */
    @Override
    public boolean delete(final Category entity) {
        return false;
    }

    /**
     * Метод обновления названия раздела.
     */
    @Override
    public boolean update(final Category category) {
        int result = 0;
        boolean isUpdated = false;
        final String updateSQL = "UPDATE categories SET category = \""
        + category.getName() + "\" WHERE id ="
                + category.getId();
        Statement statement = null;
        try {
            statement = connection.createStatement();
            result = statement.executeUpdate(updateSQL);
            if(result > 0) {
                isUpdated = true;
            }
        } catch (SQLException e) {
            LOGGER.error("Ошибка!" + e);
            final StackTraceElement[] trace = e.getStackTrace();
            for (final StackTraceElement el : trace) {
                LOGGER.error("\n " + el);
            }
        } finally {
            MySQLConnUtils.closeStatement(statement);
        }
        return isUpdated;
    }

    /**
     * Метод находит список всех существующих в базе данных разделов.
     */
    @Override
    public List<Category> findAll() {
        final List<Category> mainCategories
            = new CopyOnWriteArrayList<>();
        Statement statement = null;
        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery(FIND_ALL);
            while (resultSet.next()) {
                final Category category = new Category();
                Director.createEntity(new CategoryBuilder(),
                        category, resultSet);
                mainCategories.add(category);
            }
        } catch (SQLException e) {
            LOGGER.error("Ошибка!" + e);
            final StackTraceElement[] trace = e.getStackTrace();
            for (final StackTraceElement el : trace) {
                LOGGER.error("\n " + el);
            }
        } finally {
            MySQLConnUtils.closeStatement(statement);
        }
        return mainCategories;
    }

    /**
     * Метод не используется.
     */
    @Override
    public List<Category> findAll(final Integer parameter) {
        throw new UnsupportedOperationException();
    }

}

package com.epam.advertisments.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import com.epam.advertisments.model.Entity;

/**
 * Абстрактный класс для определения методов взаимодействия
 *  с базой данных.
 * @author Irina Mitsko
 * @param <K> - поле клааса, наследуещегося от Entity
 * @param <T> - класс-потомок Entity
 */
public abstract class AbstractDAO<K, T extends Entity> {
    /**
     * Ссылка на объект ResultSet.
     */
    protected ResultSet resultSet;
    /**
     * Создание объекта ReentrantLock.
     */
    protected Lock locker = new ReentrantLock();
    /**
     * Логгер для класса и классов-наследников.
     */
    protected static final Logger LOGGER = (Logger) LogManager
            .getLogger(AbstractDAO.class);
    /**
     * ССылка на экземпляр Connection.
     */
    protected Connection connection;

    /**
     * Конструктор с параметром для инициализации Connection.
     * @param conn - ссылка на объект connrction.
     */
    public AbstractDAO(Connection conn) {
        this.connection = conn;
    }
    /**
     * Метод сохранения объекта в базе данных.
     * @param entity - сохраняемый объект.
     * @return 0 если не сохранено, 1 - если объект сохранен
     */
    protected abstract int save(T entity);

    /**
     * Поиск объекта в базе по параметру K.
     * @param parameter - параметр, по которому
     * выполняется поиск.
     * @return объект, полученный в результате поиска или пустой
     *  объект если такого объекта нет в базе
     */
    protected abstract T findEntity(K parameter);

    /**
     * Удаление объкта из базы данных с заданным id.
     * @param identity -  первичный ключ объекта в базе.
     * @return удаленный объект или пустой объект.
     *  если такого не найдено в базе.
     */
    protected abstract T delete(int identity);

    /**
     * Удаление объекта из базы данных.
     * @param entity - удаляемый объект
     * @return true усли объект удален
     */
    protected abstract boolean delete(T entity);

    /**
     * Обновление данных объекта в базе данных.
     * @param entity - обновляемый объект
     * @return true если объект удален
     */
    protected abstract boolean update(T entity);

    /**
     * Поиск всех объектов заданного типа.
     * @return - коллекция объектов
     */
    protected abstract List<T> findAll();

    /**
     * Поиск объектов по параметру К.
     * @param parameter - параметр по которому выполняется
     *  поиск
     * @return коллекция найденных объектов
     */
    protected abstract List<T> findAll(K parameter);

}

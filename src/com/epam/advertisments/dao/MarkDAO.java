package com.epam.advertisments.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import com.epam.advertisments.connection.MySQLConnUtils;
import com.epam.advertisments.model.Mark;

/**
 * Класс для взаимодействия объектов типа Mark с базой данных.
 * @author Irina Mitsko
 */
public class MarkDAO extends AbstractDAO<Integer, Mark> {
    /**
     * Запрос на добавление оценки пользователю в базу данных.
     */
    private static final String INSERT_MARK = "INSERT INTO marks"
            + " (receipent_id, sender_id, mark) \n" + "VALUES (?,?,?)";
    /**
     * Запрос на поиск средней оценки пользователя на основании
     *  всех полученных пользователем оценок.
     */
    private static final String AVG_MARK = "SELECT AVG(mark) AS rate"
            + " FROM marks WHERE receipent_id = ";

    /**
     * Конструктор с параметром.
     * @param conn - сылка на объект соединения.
     */
    public MarkDAO(final Connection conn) {
        super(conn);
    }
    
    /**
     * Метод сохраниения оценки в базе данных.
     */
    @Override
    public int save(final Mark mark) {
        PreparedStatement prStatement = null;
        int result = 0;
        try {
            prStatement = connection.prepareStatement(INSERT_MARK);
            prStatement.setString(1, mark.getReceipentId() + "");
            prStatement.setString(2, mark.getSenderId() + "");
            prStatement.setString(3, mark.getMarkValue() + "");
            result = prStatement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.error("Ошибка!" + e);
            final StackTraceElement[] trace = e.getStackTrace();
            for (final StackTraceElement el : trace) {
                LOGGER.error("\n " + el);
            }
        } finally {
            MySQLConnUtils.closeStatement(prStatement);
        }
        return result;
    }

    /**
     * Метод поиска оценки в базе.
     */
    @Override
    public Mark findEntity(final Integer receiverId) {
        final Mark avgMark = new Mark();
        Statement statement = null;
        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery(AVG_MARK
                    + receiverId);
            while (resultSet.next()) {
                final int rating
                    = Integer.parseInt(resultSet.getString("rate"));
                avgMark.setMarkValue(rating);
                avgMark.setReceipentId(receiverId);
            }
        } catch (SQLException e) {
            LOGGER.error("Ошибка!" + e);
            final StackTraceElement[] trace = e.getStackTrace();
            for (final StackTraceElement el : trace) {
                LOGGER.error("\n " + el);
            }
        } finally {
            MySQLConnUtils.closeStatement(statement);
        }
        return avgMark;
    }

    /**
     * Метод для удаления оценки.
     */
    @Override
    public boolean delete(final Mark mark) {
        int result = 0;
        final String deleteFirst = "DELETE FROM marks WHERE receipent_id = "
                + mark.getReceipentId() + " AND sender_id = "
                + mark.getSenderId();
        Statement statement = null;
        try {
            statement = connection.createStatement();
            result = statement.executeUpdate(deleteFirst);
        } catch (SQLException e) {
            LOGGER.error("Ошибка!" + e);
            final StackTraceElement[] trace = e.getStackTrace();
            for (final StackTraceElement el : trace) {
                LOGGER.error("\n " + el);
            }
        } finally {
            MySQLConnUtils.closeStatement(statement);
        }
        return result > 0;
    }


    /**
     * Метод для вывода рейтинга пользователя на основании
     *  всех полученных им оценок.
     * @param receiverId - id пользователя, для которого рассчитывается рейтинг
     * @return rating - рейтинг пользователя
     */
    public double findRating(final Integer receiverId) {
        double rating = 0;
        Statement statement = null;
        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery(AVG_MARK
                    + receiverId);
            while (resultSet.next()) {
                if (resultSet.getString("rate") != null) {
                    rating = Double.parseDouble(resultSet.getString("rate"));
                }
            }
        } catch (SQLException e) {
            LOGGER.error("Ошибка!" + e);
            final StackTraceElement[] trace = e.getStackTrace();
            for (final StackTraceElement el : trace) {
                LOGGER.error("\n " + el);
            }
        } finally {
            MySQLConnUtils.closeStatement(statement);
        }
        return rating;
    }

    /**
     * Реализация этого метода не нужна для логики работы с
     *  классом Mark.
     */
    @Override
    public Mark delete(final int id) {
        throw new UnsupportedOperationException();
    }

    /**
     * Реализация этого метода не нужна для логики работы с
     *  классом Mark.
     */
    @Override
    public boolean update(final Mark entity) {
        throw new UnsupportedOperationException();
    }

    /**
     * Реализация этого метода не нужна для логики работы с
     *  классом Mark.
     */
    @Override
    public List<Mark> findAll() {
        throw new UnsupportedOperationException();
    }

    /**
     * Реализация этого метода не нужна для логики работы с
     *  классом Mark.
     */
    @Override
    public List<Mark> findAll(final Integer parameter) {
        throw new UnsupportedOperationException();
    }


}

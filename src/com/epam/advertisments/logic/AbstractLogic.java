package com.epam.advertisments.logic;

import javax.servlet.http.HttpServletRequest;

import com.epam.advertisments.resource.ElementManager;
import com.epam.advertisments.resource.MessageManager;
/**
 * Абстрактный класс для определения основных методов логики.
 * @author Irina Mitsko
 *
 */
public abstract class AbstractLogic {
    /**
     * Объект MessageManager для вызова языковых констант
     *  в классах-наследниках.
     */
    protected static final MessageManager MESSAGE_MANAGER
        = MessageManager.INSTANCE;
    /**
     * Объект ElementManager для вызова языковых констант
     *  в классах-наследниках.
     */
    protected static final ElementManager EL_MANAGER = ElementManager.INSTANCE;;
    /**
     * Метод для добавления сущности в базу данных.
     * @param request - ссылка на объект запроса клиента
     * @return строка с адресом накоторый будет перенаправлен клиент
     */
    public abstract String add(HttpServletRequest request);
    /**
     * Метод для удаления объекта из базы данных.
     * @param request ссылка на объект запроса клиента
     */
    public abstract void delete(HttpServletRequest request);
    /**
     * Метод для обновления данных объекта.
     * @param request ссылка на объект запроса клиента
     * @return строка с адресом накоторый будет перенаправлен клиент
     */
    public abstract String update(HttpServletRequest request);


}

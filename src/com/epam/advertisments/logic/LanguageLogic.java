package com.epam.advertisments.logic;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.jstl.core.Config;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.epam.advertisments.comand.ActionCommand;
import com.epam.advertisments.resource.ElementManager;
import com.epam.advertisments.resource.MessageManager;

/**
 * Класс для выполнения логики переключения языковой
 * локали.
 * @author Itina Mitsko
 *
 */
public class LanguageLogic {
    /**
     * Логгер для класса и классов-наследников.
     */
    private static final Logger LOGGER
        = (Logger) LogManager.getLogger(ActionCommand.class);
    /**
     * Объект MessageManager для вызова языковых констант в классах-наследниках.
     */
    private static final MessageManager MESSAGE_MANAGER = MessageManager.INSTANCE;
    /**
     * Объект ElementManager для вызова языковых констант статического
     *  контента в классах-наследниках.
     */
    private static final ElementManager EL_MANAGER = ElementManager.INSTANCE;
    /**
     * Метод переключает языковую локаль в зависимости от
     * выбранной команды пользователя.
     */
    public void switchLanguage(final HttpServletRequest request) {
        if (request.getParameter("lang").equals("ru")) {
            EL_MANAGER.changeResource(new Locale("ru", "RU"));
            MESSAGE_MANAGER.changeResource(new Locale("ru", "RU"));
            Config.set(request.getSession(),
                    Config.FMT_LOCALE, new Locale("ru", "RU"));
            request.getSession().setAttribute("lang", new Locale("ru", "RU"));
            LOGGER.info("Язык изменен на русский");
        } else if (request.getParameter("lang").equals("en")) {
            EL_MANAGER.changeResource(new Locale("en", "US"));
            MESSAGE_MANAGER.changeResource(new Locale("en", "US"));
            Config.set(request.getSession(),
                    Config.FMT_LOCALE, new Locale("en", "US"));
            request.getSession().setAttribute("lang", new Locale("en", "US"));
            LOGGER.info("Язык изменен на английский");
        } else {
            EL_MANAGER.changeResource(new Locale("be", "BY"));
            MESSAGE_MANAGER.changeResource(new Locale("be", "BY"));
            Config.set(request.getSession(),
                    Config.FMT_LOCALE, new Locale("be", "BY"));
            request.getSession().setAttribute("lang", new Locale("be", "BY"));
            LOGGER.info("Язык изменен на белорусский");
        }
    }

}

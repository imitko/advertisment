package com.epam.advertisments.logic;

import java.sql.Connection;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import javax.servlet.http.HttpServletRequest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import com.epam.advertisments.controller.Controller;
import com.epam.advertisments.dao.AdvertismentDAO;
import com.epam.advertisments.dao.MessageDAO;
import com.epam.advertisments.dao.UserDAO;
import com.epam.advertisments.model.Advertisment;
import com.epam.advertisments.model.Message;
import com.epam.advertisments.model.User;
import com.epam.advertisments.resource.MessageManager;

/**
 * Класс, реализующий логику работы с объектами Message.
 * @author Irina Mitsko
 *
 */
public class MessageLogic {
    /**
     * Логгер для класса.
     */
    private static final Logger LOGGER
    = (Logger) LogManager.getLogger(MessageLogic.class);

    /**
     * Ссылка на объект MessageDAO.
     */
    private MessageDAO msgDao;
    /**
     * Объект MessageManager для вызова языковых констант.
     */
    private static final MessageManager MESSAGE_MANAGER
        = MessageManager.INSTANCE;
    /**
     * Метод отправки сообщения пользователю.
     * @param request - ссылка на объект request
     */
    public void send(final HttpServletRequest request) {
        final Connection conn = Controller.pool.getConnection();
        final Message message = new Message();
        final int advertismentId
            = Integer.parseInt((String) request
                    .getParameter("advertisment_id"));
        message.setAdvertismentId(advertismentId);
        final int senderId
            = Integer.parseInt((String) request
                    .getParameter("sender_id"));
        message.setSenderId(senderId);
        final int receiverId
            = Integer.parseInt((String) request.getParameter("receiver_id"));
        message.setReceiverId(receiverId);
        final String text = request.getParameter("message");
        message.setMessage(text);
        final UserDAO dao = new UserDAO(conn);
        final ZoneId zone = ZoneId.of("Europe/Minsk");
        final LocalDate today = LocalDate.now(zone);
        final String date = today.toString();
        message.setDate(date);
        if (request.getSession().getAttribute("loggeduser") != null) {
            msgDao = new MessageDAO(conn);
            if (msgDao.save(message) > 0) {
                request.setAttribute("sendmessage",
                        MESSAGE_MANAGER.getString("success.send"));
                LOGGER.info("Отправка сообщения от пользователя: "
                        + dao.findEntityById(senderId) + " для пользователя "
                        + dao.findEntityById(receiverId));
            } else {
                request.setAttribute("sendmessage",
                        MESSAGE_MANAGER.getString("error.send"));
            }
        }
        Controller.pool.freeConnection(conn);
    }

    /**
     * Метод выводит сообщение из списка входящих или отправленных для чтения.
     * @param request ссылка на объект request
     * @return строка с адресом для перенапрвления запроса пользователя
     */
    public void read(final HttpServletRequest request) {
        final Connection conn = Controller.pool.getConnection();
        final int messageId = Integer.parseInt(request
                .getParameter("messageId"));
        msgDao = new MessageDAO(conn);
        final UserDAO userDao = new UserDAO(conn);
        final Message message = msgDao.findEntity(messageId);
        final List<Message> historyMessages = msgDao.findHistoryMessages(message);
        Map<Message,String> historyMap = new TreeMap<>();
        for (Message msg : historyMessages) {
            final User user = userDao.findEntityById(msg.getSenderId());
            historyMap.put(msg, user.getName());
        }
        final Advertisment advertisment = new AdvertismentDAO(conn)
                .findEntity(message.getAdvertismentId());
        if (message.getSenderId() != (int) request.getSession()
                .getAttribute("userId")) {
            request.setAttribute("receivedmessage", message);
        } else {
            request.setAttribute("sentmessage", message);
        }
        final User receiver = new UserDAO(conn)
                .findEntityById(message.getReceiverId());
        LOGGER.info("Чтение сообщения \"" + message.getMessage()
        + "\", получатель: " + receiver.getName());
        request.setAttribute("history", historyMap);
        request.setAttribute("subject", advertisment.getTitle());
        Controller.pool.freeConnection(conn);
        
    }

    /**
     * Метод выводит список входящих сообщений пользователя.
     * @param request - ссылка на объект request
     */
     public void findReceivedMessages(final HttpServletRequest request) {
         List<Message> receivedMessages = null;
         final Map<Message, List<String>> receivedMap = new TreeMap<>();
         final Connection conn = Controller.pool.getConnection();
         msgDao = new MessageDAO(conn);
         final int receiverId = (int) request.getSession()
                 .getAttribute("userId");
         receivedMessages = msgDao.findReceivedMessages(receiverId);
         if (receivedMessages.isEmpty()) {
             request.setAttribute("findNull",
                     MESSAGE_MANAGER.getString("error.messagenotfound"));
         } else {
             for (final Message message : receivedMessages) {
                 final List<String> msgDetails = new ArrayList<>();
                 final User sender = new UserDAO(conn).findEntityById(message.getSenderId());
                 final Advertisment advertisment = new AdvertismentDAO(conn)
                         .findEntity(message.getAdvertismentId());
                 msgDetails.add(sender.getName());
                 msgDetails.add(advertisment.getTitle());
                 receivedMap.put(message, msgDetails);
             }
             request.getSession().setAttribute("receivedmap",
                     receivedMap);
         }
         Controller.pool.freeConnection(conn);
         LOGGER.error("Просмотр сообщений пользователем: "
                 + (new UserDAO(conn).findEntityById(receiverId)).getEmail());
                
     }

     /**
      * Метод выводит список исходящих сообщений пользователя.
      * @param request - ссылка на объект request
      */
     public void findSentMessages(final HttpServletRequest request) {
         List<Message> sentMessages = null;
         final Map<Message, List<String>> sentMap = new TreeMap<>();
         final Connection conn = Controller.pool.getConnection();
         msgDao = new MessageDAO(conn);
         final int receiverId = (int) request.getSession()
                 .getAttribute("userId");
         sentMessages = msgDao.findSentMessages(receiverId);
         if (sentMessages.isEmpty()) {
             request.setAttribute("findNull",
                     MESSAGE_MANAGER.getString("error.messagenotfound"));
         } else {
             for (final Message message : sentMessages) {
                 final List<String> msgDetails = new ArrayList<>();
                 final User sender = new UserDAO(conn).findEntityById(message.getSenderId());
                 final Advertisment advertisment = new AdvertismentDAO(conn)
                         .findEntity(message.getAdvertismentId());
                 msgDetails.add(sender.getName());
                 msgDetails.add(advertisment.getTitle());
                 sentMap.put(message, msgDetails);
             }
             request.getSession().setAttribute("sentmap",
                     sentMap);
         }
         Controller.pool.freeConnection(conn);
         LOGGER.error("Просмотр сообщений пользователем: "
                 + (new UserDAO(conn).findEntityById(receiverId)).getEmail());
                 
     }

}

package com.epam.advertisments.logic;

import java.sql.Connection;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import com.epam.advertisments.controller.Controller;
import com.epam.advertisments.dao.AdvertismentDAO;
import com.epam.advertisments.dao.ImageDAO;
import com.epam.advertisments.model.Advertisment;
import com.epam.advertisments.model.Image;
import com.epam.advertisments.resource.ConfigurationManager;
import com.epam.advertisments.service.ImageService;
/**
 * Класс, реализующий логику работы с объектами Image.
 * @author irina Mitsko
 *
 */
public class ImageLogic extends AbstractLogic {

    /**
     * Логгер для класса.
     */
    private static final Logger LOGGER
    = (Logger) LogManager.getLogger(AdvertismentLogic.class);

    /**
     * Метод добавления картинки к объявлению.
     */
    @Override
    public String add(final HttpServletRequest request) {
        final Connection conn = Controller.pool.getConnection();
        int result = 0;
        int advertismentId = 0;
        final ImageDAO dao = new ImageDAO(conn);
        final AdvertismentDAO adsDao = new AdvertismentDAO(conn);
        if (request.getSession().getAttribute("newadv") != null) {
            final int userId = (int) request.getSession()
                    .getAttribute("userId");
            advertismentId = adsDao.findLastUploadedEntity(userId);
        } else {
            advertismentId = Integer.parseInt(request.getParameter("advId"));
        }
        final Advertisment adv = adsDao.findEntity(advertismentId);
        
        if (request.getAttribute("imgdeleted") == null) {
            result = ImageService.saveImage(request, advertismentId, dao);
            if (result > 0) {
                request.getSession().setAttribute("uploadsuccess",
                        MESSAGE_MANAGER.getString("success.upload"));
                request.getSession().removeAttribute("error");
                if (adv != null) {
                    final List<Image> images = dao.findAll(adv.getId());
                    request.setAttribute("adv1", adv);
                    request.setAttribute("images", images);
                    request.setAttribute("saveadvsuccess",
                            MESSAGE_MANAGER.getString("success.deleteadv")
                            + adv.getTitle());
                    LOGGER.info("Загружены фото к объявлению "
                            + adv.getTitle());
                }
            } else {
                LOGGER.error("Не удалось загрузить фото к объявлению.");
            }
        }
        Controller.pool.freeConnection(conn);
        return ConfigurationManager.getProperty("path.page.addimage");
    }

    /**
     * Метод удаляет картинку из обхявления.
     * @param request - ссылка на объект request
     */
    @Override
    public void delete(final HttpServletRequest request) {
        final Connection conn = Controller.pool.getConnection();
        final ImageDAO dao = new ImageDAO(conn);
        final int imageId = Integer.parseInt(request.getParameter("fid"));
        final Image image = dao.findEntity(imageId);
        final int advId = Integer.parseInt(request.getParameter("advId"));
        dao.delete(imageId);
        request.setAttribute("imgdeleted", image);
        final Advertisment adv = new AdvertismentDAO(conn)
                .findEntity(advId);
        final List<Image> images = dao.findAll(adv.getId());
        request.setAttribute("images", images);
        request.setAttribute("adv1", adv);
        request.setAttribute("saveadvsuccess",
                MESSAGE_MANAGER.getString("success.deleteadv")
                + adv.getTitle());
        Controller.pool.freeConnection(conn);
        LOGGER.info("Удаление изображения для объявления "
                + adv.getTitle());
    }
    
    /**
     * Метод не используется.
     * @throws NotSupportedMethodException 
     */
    @Override
    public String update(final HttpServletRequest request){
        throw new UnsupportedOperationException();
    }
}

package com.epam.advertisments.logic;

import java.sql.Connection;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import com.epam.advertisments.controller.Controller;
import com.epam.advertisments.dao.AdvertismentDAO;
import com.epam.advertisments.dao.CategoryDAO;
import com.epam.advertisments.dao.ImageDAO;
import com.epam.advertisments.dao.MarkDAO;
import com.epam.advertisments.dao.UserDAO;
import com.epam.advertisments.model.Advertisment;
import com.epam.advertisments.model.Category;
import com.epam.advertisments.model.Image;
import com.epam.advertisments.model.User;
import com.epam.advertisments.resource.ConfigurationManager;
import com.epam.advertisments.validator.PriceValidator;
/**
 * Класс, реализующий логику работы с объектами Объявлений.
 * @author Irina Mitsko
 *
 */
public class AdvertismentLogic extends AbstractLogic {
    /**
     * Константа для поля id пользователя.
     */
    private static final String PARAM_USERID = "userId";
    /**
     * Константа для поля цена.
     */
    private static final String PARAM_PRICE = "price";
    /**
     * Константа для поля текст объявления.
     */
    private static final String PARAM_TEXT = "description";
    /**
     * Константа для поля id категории объявления.
     */
    private static final String PARAM_CATEGORY = "category";
    /**
     * Константа для поля id объявления.
     */
    private static final String PARAM_ADVID = "advId";
    /**
     * Константа для поля заголовок объявления.
     */
    private static final String PARAM_TITLE = "title";
    /**
     * Константа для поля дата размещения объявления.
     */
    private static final String PARAM_START = "startdate";
    /**
     * Константа для поля дата окончания объявления.
     */
    private static final String PARAM_END = "enddate";
    /**
     * Константа для продление объявления на 14 дней.
     */
    private static final int DAYS = 14;
    /**
     * Ссылка на экземпляр AdvertismentDAO.
     */
    private AdvertismentDAO dao;
    /**
     * Логгер для класса.
     */
    private static final Logger LOGGER
    = (Logger) LogManager.getLogger(AdvertismentLogic.class);

    /**
     * Метод активирует объявление пользователя модератором по id объявления.
     * @param request - ссылка на объект request
     */
    public void activateAdvertisment(final HttpServletRequest request) {
        final Connection conn = Controller.pool.getConnection();
        dao = new AdvertismentDAO(conn);
        final int advId = Integer.parseInt(request.getParameter(PARAM_ADVID));
        if (dao.activate(advId) > 0) {
            request.setAttribute("deletedSuccess",
                    MESSAGE_MANAGER.getString("success.activateadv"));
            LOGGER.info("Объявление "
                    + dao.findEntity(advId).getTitle() + " прошло модерацию.");
        } else {
            request.setAttribute("error",
                    MESSAGE_MANAGER.getString("error.common"));
            LOGGER.error("Ошибка модерации объявления,"
                    + " возможно объявление было удалено.");
        }
        Controller.pool.freeConnection(conn);
    }
    /**
     * Метод, выполняющий команду пользователя по удалению
     *  объявления.
     * @param request - ссылка на объект request
     */
    @Override
    public void delete(final HttpServletRequest request) {
        final Connection conn = Controller.pool.getConnection();
        Advertisment deletedAdv = null;
        if (Integer.parseInt(request.getParameter(PARAM_ADVID)) != 0) {
            dao = new AdvertismentDAO(conn);
            deletedAdv
                = dao.delete(Integer.parseInt(request.getParameter(PARAM_ADVID)));
            request.setAttribute("uid", deletedAdv.getUserId());
        }
        if (deletedAdv.getId() != 0) {
            request.setAttribute("deletedadv", deletedAdv);
            request.setAttribute("deletedSuccess",
                    MESSAGE_MANAGER.getString("success.deleteadv")
                    + deletedAdv.getTitle()
                    + MESSAGE_MANAGER.getString("success.deleteadv2"));
            LOGGER.info("Объявление " + deletedAdv.getTitle() + " удалено.");
            final List<Advertisment> advertisments = dao.findAll();
            request.setAttribute("formoderation", advertisments);
        } else {
            request.getSession().setAttribute("error",
                    MESSAGE_MANAGER.getString("error.common"));
            LOGGER.error("Объявление " + deletedAdv.getTitle()
            + " не удалено.");
        }
        Controller.pool.freeConnection(conn);
    }

    /**
     * Метод продлевает объявление пользователя на 1 мес с текущей даты.
     * @param request - ссылка на объект request
     */
    public void extendAdvertisment(final HttpServletRequest request) {
        final Connection conn = Controller.pool.getConnection();
        dao = new AdvertismentDAO(conn);
        final int advId = Integer.parseInt(request.getParameter(PARAM_ADVID));
        final LocalDate today = LocalDate.now(ZoneId.of("Europe/Minsk"));
        final String startDate = today.toString();
        final LocalDate oneMonthLater = today.plusMonths(1);
        final String endDate = oneMonthLater.toString();
        if (dao.extend(advId, startDate, endDate) >= 1) {
            LOGGER.info("Продление объявления "
        + dao.findEntity(advId).getTitle());
            request.setAttribute("deletedSuccess",
                    MESSAGE_MANAGER.getString("success.extend"));
        } else {
            request.setAttribute("error",
                    MESSAGE_MANAGER.getString("error.common"));
            LOGGER.error("Попытка продления объявления "
                    + dao.findEntity(advId).getTitle() + " - неучдачно");
        }
        Controller.pool.freeConnection(conn);
    }

    /**
     * Метод для добавления нового объявления.
     * @param request - ссылка на объект request
     * @return строка с адресом для перенапрвления запроса пользователя
     */
    @Override
    public String add(final HttpServletRequest request) {
        String page = null;
        final Connection conn = Controller.pool.getConnection();
        dao = new AdvertismentDAO(conn);
        Advertisment advertisment = null;
        if (!request.getParameter(PARAM_ADVID).equals("")) {
            final int advId = Integer.parseInt(request.getParameter(PARAM_ADVID));
            advertisment = dao.findEntity(advId);
            final Category category = new CategoryDAO(conn)
                    .findEntity(advertisment.getCategoryId());
            request.setAttribute("category", category);
            request.setAttribute("advid", advId);
            request.setAttribute("adv", advertisment);
            request.setAttribute("editadv", true);
            request.getSession().setAttribute("adv1", advertisment);
            page = ConfigurationManager.getProperty("path.page.addadv");

        } else {
            advertisment = new Advertisment();
            final int userId
                = Integer.parseInt((String) request.getParameter(PARAM_USERID));
            advertisment.setUserId(userId);
            final int categoryId
                = Integer.parseInt(request.getParameter(PARAM_CATEGORY));
            advertisment.setCategoryId(categoryId);
            final String title = request.getParameter(PARAM_TITLE);
            advertisment.setTitle(title);
            final String text = request.getParameter(PARAM_TEXT);
            advertisment.setText(text);
            String startDate = request.getParameter(PARAM_START);
            String endDate = request.getParameter(PARAM_END);
            if (startDate != null && endDate != null) {
                advertisment.setStartDate(startDate);
                advertisment.setEndDate(endDate);
            } else {
                final LocalDate today
                    = LocalDate.now(ZoneId.of("Europe/Minsk"));
                startDate = today.toString();
                endDate = today.plusDays(DAYS).toString();
            }
            final double price
                = Double.parseDouble(request.getParameter(PARAM_PRICE));
            if (new PriceValidator()
                    .validatePrice(request.getParameter(PARAM_PRICE))) {
                advertisment.setPrice(price);
            }
            final User user = new UserDAO(conn).findEntityById(userId);
            if (dao.save(advertisment) == 1) {
                request.setAttribute("saveadvsuccess",
                        MESSAGE_MANAGER.getString("success.deleteadv")
                        + advertisment.getTitle()
                        + MESSAGE_MANAGER.getString("success.save"));
                request.setAttribute("adv1", advertisment);
                final ImageDAO imgDao = new ImageDAO(conn);
                final List<Image> images = imgDao.findAll(advertisment.getId());
                request.setAttribute("images", images);
                request.getSession().setAttribute("newadv", advertisment);
                page = ConfigurationManager.getProperty("path.page.addimage");
                LOGGER.info("Добавлено объявление "
                        + advertisment.getTitle() + ", пользователь "
                        + user.getEmail());
            } else {
                request.setAttribute("error",
                        MESSAGE_MANAGER.getString("error.common"));
                page = ConfigurationManager.getProperty("path.page.addadv");
                LOGGER.info("Неудачная попытка добавления объявления"
                        + " пользователем " + user.getEmail());
            }
        }
        Controller.pool.freeConnection(conn);
        return page;
    }

    /**
     * Метод, считывающий отредактированные данные со страницы с
     * формой объявления и обновляющий их в базе данных.
     *  @return строка с адресом страницы для добавления фото
     *  или страницы с ошибкой (если обновление прошло с ошибкой).
     */
    @Override
    public String update(final HttpServletRequest request) {
        final Connection conn = Controller.pool.getConnection();
        String page = null;
        final ImageDAO imgDao = new ImageDAO(conn);
        final Advertisment adv = (Advertisment) request
                .getSession().getAttribute("adv1");
        final List<Image> images = imgDao.findAll(adv.getId());
        request.setAttribute("images", images);
        final String title = request.getParameter(PARAM_TITLE);
        adv.setTitle(title);
        final String category = request.getParameter(PARAM_CATEGORY);
        adv.setCategoryId(Integer.parseInt(category));
        final double price = Double.parseDouble(request.getParameter(PARAM_PRICE));
        final boolean isPriceValid = new PriceValidator()
                .validatePrice(request.getParameter(PARAM_PRICE));
        if (isPriceValid) {
            adv.setPrice(price);
        }
        final String description = request.getParameter(PARAM_TEXT);
        adv.setText(description);
        dao = new AdvertismentDAO(conn);
        if (isPriceValid && dao.update(adv)) {
            request.setAttribute("advimages", images);
            request.setAttribute("saveadvsuccess",
                    MESSAGE_MANAGER.getString("success.deleteadv") + "\""
                            + adv.getTitle() + "\"" + "<br>"
                            + adv.getText() + "<br>"
                            + MESSAGE_MANAGER.getString("success.updateprice")
                            + adv.getPrice()
                            + "<br>"
                            + MESSAGE_MANAGER.getString("success.updateterm")
                            + adv.getEndDate());
            request.getSession().setAttribute("adv1", null);
            request.setAttribute("adv1", adv);
            page = ConfigurationManager.getProperty("path.page.addimage");
            LOGGER.info("Редактирование объвления : " + adv.getTitle());
        } else {
            page = ConfigurationManager.getProperty("path.page.error");
        }
        Controller.pool.freeConnection(conn);
        return page;
    }
    /**
     * Метод находит все объявления залогинившегося пользователя
     * или все объявления пользователя по id (для администратора).
     *  @return строка с адресом страницы с объявлениями или страницы
     *  с формой авторизации (в случае если пользователь разлогинился).
     * @param request - ссылка на объект request
     */
    public String showUsersAds(final HttpServletRequest request) {
        final Connection conn = Controller.pool.getConnection();
        String page = ConfigurationManager.getProperty("path.page.auth");
        dao = new AdvertismentDAO(conn);
        int userId = 0;
        if (request.getSession().getAttribute(PARAM_USERID) != null) {
         // если админ смотрит объявления пользователя
            if (request.getParameter("uid") != null) {
                userId = Integer.parseInt(request.getParameter("uid"));
            } else if (request.getAttribute("uid") != null) {
                //если админ возвращается на страницу объявлений пользователя
                //после удаления
                userId = (int) request.getAttribute("uid");
            } else {
                // если пользователь смотрит свои объявления
                userId = (int) request.getSession().getAttribute(PARAM_USERID);
            }
            final List<Advertisment> advertisments = dao.findAll(userId);
            CategoryDAO catDao = new CategoryDAO(conn);
            request.setAttribute("catDao", catDao);
            if (advertisments.size() != 0) {
                request.setAttribute("listOfAdv", advertisments);
                request.getSession().setAttribute("edit", true);
            } else {
                request.getSession().setAttribute("null",
                        MESSAGE_MANAGER.getString("error.notfound"));
            }
            page = ConfigurationManager.getProperty("path.page.usersadv");
            LOGGER.info("Просмотр объявлений пользователя с id=" + userId);
        } else {
            page = ConfigurationManager.getProperty("path.page.auth");
        }
        Controller.pool.freeConnection(conn);
        return page;
    }

    /**
     * Метод, выполняющий команду по выводу страницы с объявлением.
     *  @return адрес строки с объявлением или строка с ошибкой в
     *   случае ксли объявление не найдено.
     * @param request - ссылка на объект request
     */
    public String showAds(final HttpServletRequest request) {
        final Connection conn = Controller.pool.getConnection();
        String page = null;
        Advertisment advertisment = null;
        if (request.getParameter("aid") != null
                && request.getParameter("aid").matches("\\d+")) {
            final int advId = Integer.parseInt(request.getParameter("aid"));
            dao = new AdvertismentDAO(conn);
            advertisment = dao.findEntity(advId);
            final User user = new UserDAO(conn)
                    .findEntityById(advertisment.getUserId());
            request.setAttribute("user", user);
            final double avgMark = new MarkDAO(conn)
                    .findRating(advertisment.getUserId());
            request.setAttribute("rating", avgMark);
            final List<Image> images = new ImageDAO(conn).findAll(advId);
            request.setAttribute("adv", advertisment);
            request.setAttribute("myadv", true);
            request.setAttribute("images", images);
            page = ConfigurationManager.getProperty("path.page.advertisment");
            LOGGER.info("Просмотр объявления "
                    + advertisment.getTitle()
                    + ", id=" + advertisment.getId());
        } else {
            request.setAttribute("notfound",
                    MESSAGE_MANAGER.getString("error.notfound"));
            page = ConfigurationManager.getProperty("path.page.error");
        }
        Controller.pool.freeConnection(conn);
        return page;
    }
    /**
     * Метод выполняет поиск объявления в разделе по ключевому слову
     * в заголовке или тексте.
     * @param request ссылка на объект request пользователя
     */
    public void searchAds(final HttpServletRequest request) {
        final Connection conn = Controller.pool.getConnection();
        final AdvertismentDAO dao = new AdvertismentDAO(conn);
        final int catId = Integer.parseInt(request.getParameter("categoryId"));
        final String searchedText = request.getParameter("text");
        final List<Advertisment> advs = dao.findByKeyWords(catId, searchedText);
        LOGGER.info("Поиск объявлений по ключевому слову: " + searchedText);
        if (!advs.isEmpty()) {
            request.setAttribute("advertisments", advs);
            final ImageDAO imgDao = new ImageDAO(conn);
            request.setAttribute("imgdao", imgDao);
            request.setAttribute("searched", true);
        } else {
            request.setAttribute("findNull",
                    MESSAGE_MANAGER.getString("error.notfound2"));
        }
        Controller.pool.freeConnection(conn);    
        }
    /**
     * Метод выводит список объявлений для модерации.
     * @param request - ссылка на объект request пользователя
     */
    public void showModeration(final HttpServletRequest request) {
        final Connection conn = Controller.pool.getConnection();
        final AdvertismentDAO dao = new AdvertismentDAO(conn);
        final List<Advertisment> advertisments = dao.findAll();
        request.setAttribute("formoderation", advertisments);
        final CategoryDAO catDao = new CategoryDAO(conn);
        request.setAttribute("catDao", catDao);
        ImageDAO imgDao = new ImageDAO(conn);
        request.setAttribute("imgdao", imgDao);
        if (advertisments.size() == 0) {
            request.getSession().setAttribute("findNull",
                    MESSAGE_MANAGER.getString("message.findnull"));
        } else {
            request.getSession().setAttribute("listOfAdv",
                    advertisments);
            request.getSession().setAttribute("edit", true);
        }
        Controller.pool.freeConnection(conn);
    }

}

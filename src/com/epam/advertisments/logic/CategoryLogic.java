package com.epam.advertisments.logic;

import java.sql.Connection;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.epam.advertisments.controller.Controller;
import com.epam.advertisments.dao.AdvertismentDAO;
import com.epam.advertisments.dao.CategoryDAO;
import com.epam.advertisments.dao.ImageDAO;
import com.epam.advertisments.model.Advertisment;
import com.epam.advertisments.model.Category;
import com.epam.advertisments.resource.ConfigurationManager;

/**
 * Класс, реализующий логику работы с объектами Category.
 * @author Irina Mitsko
 *
 */
public class CategoryLogic extends AbstractLogic {
    private static final String PARAM_TITLE = "title";
    private static final String PARAM_CAT_ID = "categoryid";
    /**
     * Логгер для класса.
     */
    private static final Logger LOGGER
    = (Logger) LogManager.getLogger(CategoryLogic.class);
    
    /**
     * Метод, выполняющий создание новой категории
     * или вызов страницы с редактируемой категорией.
     *  @return строка с адресом страницы.
     */
    @Override
    public String add(final HttpServletRequest request) {
        final Connection conn = Controller.pool.getConnection();
        String page = ConfigurationManager.getProperty("path.page.addcat");
        final CategoryDAO dao = new CategoryDAO(conn);
        Category category = new Category();
        final String catid = request.getParameter(PARAM_CAT_ID);
            if ("".equals(catid)) {
                final String categoryName = request.getParameter(PARAM_TITLE);
                category.setName(categoryName);
                if (dao.save(category) > 0) {
                    request.setAttribute("success",
                            MESSAGE_MANAGER
                                .getString("success.createcategory"));
                    final List<Category> categories = dao.findAll();
                    request.getSession().setAttribute("categories", categories);
                    request.setAttribute("dao", new AdvertismentDAO(conn));
                    page = ConfigurationManager
                            .getProperty("path.page.categories");
                    LOGGER.info("Добавлена категория " + category.getName());
                } else {
                    request.setAttribute("error",
                            MESSAGE_MANAGER.getString("error.common"));
                    LOGGER.error("Категория не добавлена");
                }
            } else {
                final int categoryId = Integer.parseInt((String) request
                        .getParameter(PARAM_CAT_ID));
                category = dao.findEntity(categoryId);
                request.setAttribute("category", category);
                LOGGER.info("Редактирование раздела " + category.getName());
            }
        Controller.pool.freeConnection(conn);
        return page;
    }

    /**
     * Метод удаляет раздел каталога.
     * @param request - ссылка на объект request
     */
    @Override
    public void delete(final HttpServletRequest request) {
        final Connection conn = Controller.pool.getConnection();
        final CategoryDAO dao = new CategoryDAO(conn);
        int categoryId = Integer.parseInt(request.getParameter(PARAM_CAT_ID));
        final Category deleted = dao.delete(categoryId);
        if (deleted.getName() != null) {
            request.getSession().setAttribute("categories", dao.findAll());
            request.setAttribute("managecategory",
                    MESSAGE_MANAGER.getString("success.deletecat"));
            LOGGER.info("Удаление раздела " + deleted.getName());
        } else {
            request.setAttribute("managecategory",
                    MESSAGE_MANAGER.getString("error.common"));
            LOGGER.info("Удаление раздела " + deleted.getName()
                + " не выполнено.");
        }
        Controller.pool.freeConnection(conn);
    }

    /**
     * Метод, считывающий отредактированные данные из формы добавления
     * категории и обновдяющий их в базе данных.
     *  @return строка с адресом страницы редактирования списка категорий.
     *  @param - ссылка на объект request
     */
    @Override
    public String update(final HttpServletRequest request) {
        final Connection conn = Controller.pool.getConnection();
        final CategoryDAO dao = new CategoryDAO(conn);
        final int categoryID = Integer.parseInt(request
                .getParameter(PARAM_CAT_ID));
        final Category category = dao.findEntity(categoryID);
        final String name = request.getParameter(PARAM_TITLE);
        category.setName(name);
        if (dao.update(category)) {
            request.setAttribute("managecategory",
                    MESSAGE_MANAGER.getString("success.updatecategory"));
            LOGGER.info("Редактирование раздела : "
                    + category.getName());
        } else {
            request.setAttribute("managecategory",
                    MESSAGE_MANAGER.getString("error.common"));
            LOGGER.error("Редактирование раздела : "
                    + category.getName() + " завершено с ошибкой.");
        }
        Controller.pool.freeConnection(conn);
        return ConfigurationManager.getProperty("path.page.managecategories");
    }
    /**
     * Метод проверяет роль пользователя и если роль соответствует ADMIN,
     * перенаправляет на страницу со списком разделов для редактирования.
     * @param request - ссылка на объект request
     * @return строка с адресом страницы для перенаправления
     */
    public String manageCategories(final HttpServletRequest request) {
        final Connection conn = Controller.pool.getConnection();
        final List<Category> allCategories = new CategoryDAO(conn).findAll();
        request.getSession().setAttribute("categories", allCategories);
        AdvertismentDAO dao = new AdvertismentDAO(conn);
        Map<Category, List<Advertisment>> categoriesMap = new TreeMap<>();
        for (final Category category : allCategories) {
            final List<Advertisment> advertisments
            = dao.findAllFromCategory(category.getId());
            categoriesMap.put(category, advertisments);
        }
        request.setAttribute("categoriesMap", categoriesMap);
        Controller.pool.freeConnection(conn);
        return ConfigurationManager.getProperty("path.page.categories");
    } 
    
    /**
     * Метод формирует список объявлений из одного раздела.
     * @param request - ссылка на объект request
     * @return строка со ссылкой на страницу раздела или страницу
     * с ошибкой если неверно введен параетр категории в запросе
     */
    public String showCategoryAds(final HttpServletRequest request) {
        String page = ConfigurationManager.getProperty("path.page.category");
        final Connection conn = Controller.pool.getConnection();
        final AdvertismentDAO dao = new AdvertismentDAO(conn);
        final ImageDAO imgDao = new ImageDAO(conn);
        request.setAttribute("imgdao", imgDao);
        final Category category = new CategoryDAO(conn)
                .findEntity(Integer.parseInt(request
                        .getParameter("catid")));
        request.setAttribute("categoryName", category.getName());
        request.setAttribute("categoryId", category.getId());
        final List<Advertisment> advertisments
        = dao.findAllFromCategory(Integer.parseInt(request
                .getParameter("catid")));
        
        if (!advertisments.isEmpty()) {
            request.setAttribute("advertisments", advertisments);
            LOGGER.info("Просмотр раздела " + category.getName());
        } else if (category.getName() != null) {
            request.setAttribute("findNull",
                    MESSAGE_MANAGER.getString("error.notfound2"));
            LOGGER.info("Просмотр раздела " + category.getName());
        } else {
            page = ConfigurationManager.getProperty("path.page.error");
            LOGGER.info("Попытка просмотра несуществующего раздела");
        }
        
        Controller.pool.freeConnection(conn);
        return page;
    
    }

}

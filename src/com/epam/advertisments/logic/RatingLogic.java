package com.epam.advertisments.logic;

import java.sql.Connection;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.epam.advertisments.controller.Controller;
import com.epam.advertisments.dao.MarkDAO;
import com.epam.advertisments.model.Mark;
import com.epam.advertisments.resource.ConfigurationManager;

/**
 * Класс, реализующий логику работы с объектами Mark
 * (выставление оцценок).
 * @author Irina Mitsko
 *
 */
public class RatingLogic extends  AbstractLogic {
    /**
     * Логгер для класса.
     */
    private static final Logger LOGGER
    = (Logger) LogManager.getLogger(RatingLogic.class);

    /**
     * Метод, сохраняющий оценки пользователей друг другу.
     *  @return строка с подтверждением о принятии оценки.
     */
    @Override
    public String add(final HttpServletRequest request) {
        final Connection conn = Controller.pool.getConnection();
        final Mark mark = new Mark();
        final int rate = Integer.parseInt(request.getParameter("reviewStars"));
        mark.setMarkValue(rate);
        final int senderId = Integer.parseInt((String) request
                .getParameter("sender_id"));
        mark.setSenderId(senderId);
        final int receiverId = Integer.parseInt((String) request
                .getParameter("receiver_id"));
        mark.setReceipentId(receiverId);
        final MarkDAO dao = new MarkDAO(conn);
        dao.delete(mark);
        if (dao.save(mark) > 0) {
            request.setAttribute("sendmark", MESSAGE_MANAGER
                    .getString("success.mark"));
            LOGGER.info("Пользователь (id " + senderId + ") поставил оценку"
                            + rate + " пользователю (" + receiverId + ")");
        } else {
            request.setAttribute("sendmark",
                    MESSAGE_MANAGER.getString("error.common"));
        }
        Controller.pool.freeConnection(conn);
        return ConfigurationManager.getProperty("path.page.success");
    }

    /**
     * Метод не используется для работы логики выставления оценок.
     */
    @Override
    public void delete(final HttpServletRequest request) {
        throw new UnsupportedOperationException();
    }

    /**
     * Метод не используется для работы логики выставления оценок.
     */
    @Override
    public String update(final HttpServletRequest request) {
        throw new UnsupportedOperationException();
    }

}

package com.epam.advertisments.logic;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.jstl.core.Config;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import com.epam.advertisments.controller.Controller;
import com.epam.advertisments.dao.AdvertismentDAO;
import com.epam.advertisments.dao.UserDAO;
import com.epam.advertisments.model.Advertisment;
import com.epam.advertisments.model.User;
import com.epam.advertisments.model.UserRole;
import com.epam.advertisments.resource.ConfigurationManager;
import com.epam.advertisments.validator.EmailValidator;
import com.epam.advertisments.validator.LoginValidator;
import com.epam.advertisments.validator.PhoneValidator;

/**
 * Класс, реализующий логику работы с объектами User.
 * @author Irina Mitsko
 *
 */
public class UserLogic extends AbstractLogic {
    /**
     * Константа для параметра id пользователя.
     */
    private static final String PARAM_ID = "userId";
    /**
     * Константа для параметра имя пользователя.
     */
    private static final String PARAM_NAME = "username";
    /**
     * Константа для параметра емейл пользователя.
     */
    private static final String PARAM_EMAIL = "email";
    /**
     * Константа для параметра телефон.
     */
    private static final String PARAM_PHONE = "phone";
    /**
     * Константа для параметра Пароль.
     */
    private static final String PARAM_PASSWORD = "password";
    
    /**
     * Логгер для класса.
     */
    private static final Logger LOGGER
    = (Logger) LogManager.getLogger(UserLogic.class);

    /**
     * Метод забрает данные из полей формы и сохраняет нового
     * пользователя в базе.
     *  @return строка с адресом титульной страницы в случае
     *   успешной регистрации или регистрационная форма с сообщением
     *    об ошибке.
     */
    @Override
    public String add(final HttpServletRequest request) {
        final Connection conn = Controller.pool.getConnection();
        String page = ConfigurationManager.getProperty("path.page.registration");
        final User user = new User();
        final String name = request.getParameter(PARAM_NAME);
        user.setName(name);
        final String email = request.getParameter(PARAM_EMAIL);
        if (new EmailValidator().validateEmail(email)) {
            user.setEmail(email);
        }
        final String phone = request.getParameter(PARAM_PHONE);
        if (new PhoneValidator().validatePhone(phone)) {
            user.setPhone(phone);
        }
        final String password = request.getParameter(PARAM_PASSWORD);
        user.setPassword(password);
        final UserRole role = UserRole.USER;
        user.setRole(role);
        final UserDAO dao = new UserDAO(conn);
        if (user.getEmail() != null && user.getPhone() != null
                && dao.save(user) == 1) {
            final User loddedUser = dao.findEntity(email);
            request.getSession().setAttribute("loggeduser", loddedUser);
            request.getSession().setAttribute(PARAM_ID, loddedUser.getId());
            request.getSession().setAttribute("role",
                    loddedUser.getRole().getUserProfileType());
            request.getSession().setAttribute("edit", true);
            page = ConfigurationManager.getProperty("path.page.main");
            LOGGER.info("Регистрация нового пользователя: " + email);
        } else {
            request.setAttribute("error",
                    MESSAGE_MANAGER.getString("error.registration"));
            LOGGER.error("Попытка регистрации нового пользователя: "
                    + email + " завершена с ошибкой.");
        }
        Controller.pool.freeConnection(conn);
        return page;
    }

    /**
     * Метод удаляет пользователя из базы данных.
     * @param request - ссылка на объект request
     */
    @Override
    public void delete(final HttpServletRequest request) {
        final Connection conn = Controller.pool.getConnection();
        final int userId = Integer.parseInt(request.getParameter(PARAM_ID));
        final User user = new UserDAO(conn).delete(userId);
        if (user.getName() != null) {
            request.setAttribute("deteteduser",
                    MESSAGE_MANAGER.getString("success.deleteuser1")
                    + user.getName() + ", "
                    + user.getEmail()
                    + MESSAGE_MANAGER.getString("success.deleteuser2"));
            LOGGER.info("Удаление пользователя " + user.getName());
        } else {
            request.setAttribute("deteteduser",
                    MESSAGE_MANAGER.getString("error.common"));
            LOGGER.error("Удаление пользователя " + user.getName()
                + " завершено с ошибкой!");
        }
        Controller.pool.freeConnection(conn);

    }

    /**
     * Метод обновляет регистрационные данные пользователя.
     * @param request - ссылка на объект request
     */
    @Override
    public String update(final HttpServletRequest request) {
        final Connection conn = Controller.pool.getConnection();
        String page = null;
        final int id = Integer.parseInt(request.getParameter(PARAM_ID));
        UserDAO dao = new UserDAO(conn);
        final User user = dao.findEntityById(id);
        final String name = request.getParameter(PARAM_NAME);
        user.setName(name);
        final String phone = request.getParameter(PARAM_PHONE);
        user.setPhone(phone);
        String email = request.getParameter(PARAM_EMAIL);
        user.setEmail(email);
        final String password = request.getParameter(PARAM_PASSWORD);
        if (password != null) {
            user.setPassword(password);
        }
        if (dao.update(user)) {
            request.setAttribute("success",
                    MESSAGE_MANAGER.getString("success.updateuser")
                    + user.getName()
                            + MESSAGE_MANAGER.getString("success.updateuser2")
                            + user.getEmail()
                            + MESSAGE_MANAGER.getString("success.updateuser3"));
            final User loggeduser = (User) request.getSession()
                    .getAttribute("loggeduser");
            if (loggeduser.getId() == user.getId()) {
                request.getSession().setAttribute("loggeduser", user);
                page = ConfigurationManager
                        .getProperty("path.page.registration");
            } else {
                page = ConfigurationManager
                        .getProperty("path.page.manageusers");
            }
            LOGGER.error("Редактирование данных пользователя: "
                    + user.getEmail()
                + " Кем изменено: " + loggeduser.getEmail());
        } else {
            page = ConfigurationManager.getProperty("path.page.error");
        }
        Controller.pool.freeConnection(conn);
        return page;
    }

    /**
     * Метод проверяет корректность введенных логина и пароя пользователя
     * и дает доступ в личный кабинет.
     * @param request - ссылка на объект request
     * @return адрес страницы с данными пользователя доступными
     *  для редактирования
     */
    public String login(final HttpServletRequest request) {
        final Connection conn = Controller.pool.getConnection();
        String page = ConfigurationManager.getProperty("path.page.auth");
        final HttpSession session = request.getSession();
        final UserDAO dao = new UserDAO(conn);
        final String email = request.getParameter(PARAM_EMAIL);
        String password = request.getParameter(PARAM_PASSWORD);
        final User user = dao.findEntity(request.getParameter(PARAM_EMAIL));
        if(LoginValidator.valigateLoginData(dao, email, password)) {
                session.setAttribute("loggeduser", user);
                session.setAttribute(PARAM_ID, user.getId());
                session.setAttribute("role",
                        user.getRole().getUserProfileType());
                session.setAttribute("edit", true);
                page = ConfigurationManager.getProperty("path.page.main");
                LOGGER.info("Авторизация пользователя: " + user.getEmail());
        } else {
            request.setAttribute("error",
                    MESSAGE_MANAGER.getString("error.wronglogin"));
            LOGGER.error("Неудачная попытка авторизации пользователя: "
                    + user.getEmail() + ". Неверный логин или пароль.");
        }
        Controller.pool.freeConnection(conn);
        return page;
    }

    /**
     * Метод, выполняющий команду по получению доступа к данным
     *  пользователя администратором для редактирования.
     * объявления.
     * @return адрес страницы с данными пользователя доступными
     *  для редактирования
     * @param request - ссылка на объект request
     */
    public String editUser(final HttpServletRequest request) {
        String page = null;
        final Connection conn = Controller.pool.getConnection();
        if (request.getParameter(PARAM_ID) != null) {
            final int userId =
                    Integer.parseInt(request.getParameter(PARAM_ID));
            final User managedUser = new UserDAO(conn).findEntityById(userId);
            request.setAttribute("manageduser", managedUser);
            page = ConfigurationManager.getProperty("path.page.userdata");
            LOGGER.info("Редактирование пользователя: "
            + managedUser.getEmail());
        }
        Controller.pool.freeConnection(conn);
        return page;
    }
    
    /**
     * Метод разрушает сессию пользователя.
     */
    public void logout(final HttpServletRequest request) {
        final User loggedUser
        = (User) request.getSession().getAttribute("loggeduser");
    if (loggedUser != null) {
        LOGGER.info("Выход из системы пользователя: "
    + loggedUser.getEmail());
    }
    Config.remove(request.getSession(), Config.FMT_LOCALE);
    MESSAGE_MANAGER.changeResource(Locale.getDefault());
    EL_MANAGER.changeResource(Locale.getDefault());
    request.getSession().invalidate();
    }
    
    /**
     * Метод находит всех пользователей (или одного) в базе данных, объявления каждого
     * пользователя и сохраняет их в коллекцию Map вида Пользователь-Объявления.
     * @return строка с адресом страницы.
     * @return строка с адресом для перенаправления пользователя
     */
    public String manageUsers(final HttpServletRequest request) {
        final Connection conn = Controller.pool.getConnection();
        List<User> allUsers;
        final Map<User, List<Advertisment>> map = new HashMap<>();
        if (request.getParameter(PARAM_EMAIL) == null) {
            allUsers = new UserDAO(conn).findAll();
        } else {
            allUsers = new ArrayList<>();
            final User user
                = new UserDAO(conn).findEntity(request.getParameter(PARAM_EMAIL));
            allUsers.add(user);
        }
        for (final User user : allUsers) {
            final List<Advertisment> userAdvs
            = new AdvertismentDAO(conn).findAll(user.getId());
            map.put(user, userAdvs);
        }
        request.setAttribute("usersmap", map);
        Controller.pool.freeConnection(conn);
        return ConfigurationManager.getProperty("path.page.users");
    }
    
}

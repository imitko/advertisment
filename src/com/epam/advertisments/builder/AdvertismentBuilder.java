package com.epam.advertisments.builder;

import java.sql.ResultSet;
import java.sql.SQLException;
import com.epam.advertisments.model.Advertisment;

/**
 * Класс для создания объектоов типа Advertisment.
 *
 * @author Irina Mitsko
 *
 */
public class AdvertismentBuilder extends AbstractBuilder<Advertisment> {

    /**
     * Метод, конструирующий конкретную сущность на основе полученных
     * из базы данных полей.
     */
    @Override
    public void buildEntity(final Advertisment advertisment,
            final ResultSet resultSet) {
        try {
            final int userId = Integer.parseInt(resultSet.getString("user_id"));
            advertisment.setUserId(userId);
            final int categoryId = Integer.parseInt(resultSet.getString("category_id"));
            advertisment.setCategoryId(categoryId);
            final String title = resultSet.getString("title");
            advertisment.setTitle(title);
            final String text = resultSet.getString("text");
            advertisment.setText(text);
            final double price = Double.parseDouble(resultSet.getString("price"));
            advertisment.setPrice(price);
            final String startDate = resultSet.getString("start_date");
            advertisment.setStartDate(startDate);
            final String endDate = resultSet.getString("end_date");
            advertisment.setEndDate(endDate);
            final int isActive = Integer.parseInt(resultSet.getString("is_active"));
            advertisment.setActive(isActive == 1);
            final int isModerated = Integer.parseInt(resultSet.getString("is_moderated"));
            advertisment.setModerated(isModerated == 1);
        } catch (NumberFormatException e) {
            LOGGER.error("Ошибка! Несовпадающий формат данных." + e);
            final StackTraceElement[] trace = e.getStackTrace();
            for (final StackTraceElement el : trace) {
                LOGGER.error("\n " + el);
            }
        } catch (SQLException e) {
            LOGGER.error("Ошибка при работе с базой данных!" + e);
            final StackTraceElement[] trace = e.getStackTrace();
            for (final StackTraceElement el : trace) {
                LOGGER.error("\n " + el);
            }
        }
    }
}

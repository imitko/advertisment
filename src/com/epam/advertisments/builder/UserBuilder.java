package com.epam.advertisments.builder;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import com.epam.advertisments.model.User;
import com.epam.advertisments.model.UserRole;

/**
 * Класс для создания объектоов типа User.
 * @author Irina Mitsko
 */
public class UserBuilder extends AbstractBuilder<User> {

    /**
     * Метод, конструирующий объект типа User на основе полученных
     * из базы данных полей.
     */
    @Override
    public void buildEntity(final User user,
            final ResultSet resultSet) {
        try {
            if (user.getId() == 0) {
                final int userId = Integer.parseInt(resultSet.getString("id"));
                user.setId(userId);
            }
            if (user.getEmail() == null) {
                final String email = resultSet.getString("email");
                user.setEmail(email);
            }
            final String username = resultSet.getString("name");
            user.setName(username);
            String columnName = "password";
            ResultSetMetaData metaData = resultSet.getMetaData();
            for (int i = 1; i <= metaData.getColumnCount(); i++)
                if (columnName.equals(metaData.getColumnName(i))) {
                    final String password = resultSet.getString("password");
                    user.setPassword(password);
                }
            final String phone = resultSet.getString("phone");
            user.setPhone(phone);
            final UserRole role
                = UserRole.valueOf(resultSet.getString("role").toUpperCase());
            user.setRole(role);
        } catch (SQLException e) {
            LOGGER.error("Ошибка при работе с базой данных!" + e);
            final StackTraceElement[] trace = e.getStackTrace();
            for (final StackTraceElement el : trace) {
                LOGGER.error("\n " + el);
            }
        }
    }
}

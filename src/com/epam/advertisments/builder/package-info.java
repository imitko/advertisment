/**
 * Пакет для создания объектов класов по одному принципу (шаблон builder)
 */
/**
 * @author Irina Mitsko
 *
 */
package com.epam.advertisments.builder;

package com.epam.advertisments.builder;

import java.sql.ResultSet;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import com.epam.advertisments.model.Entity;


/**
 * Абстрактный класс для создания и получения экземпляра сущности.
 *
 * @author Irina Mitsko
 *
 * @param <T>
 *            - любой дочерний класс, наследующий Entity
 */
public abstract class AbstractBuilder<T extends Entity> {
    /**
     * Создание экземпляра базового класса Entity.
     */
    protected Entity entity = new Entity();
    /**
     * Логгер для класса и классов-наследников.
     */
    protected static final Logger LOGGER = (Logger) LogManager
            .getLogger(AbstractBuilder.class);

    /**
     * Геттер для получения экземпляра Entity.
     *
     * @return entity - созданный экземпляр класса Entity или любого наследника.
     */
    public Entity getEntity() {
        return entity;
    }

    /**
     * Абстрактный метод для создания сущности любого производного класса.
     * @param newEntity - объект класса-наследника
     * @param rs- объект ResultSet (так как сущности создаются на основе
     * полученных данных из БД)
     */
    public abstract void buildEntity(T newEntity, ResultSet rs);

}

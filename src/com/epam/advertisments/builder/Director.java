package com.epam.advertisments.builder;

import java.sql.ResultSet;

import com.epam.advertisments.model.Entity;

/**
 * Класс, управляющий созданием объектов.
 * @author Irina Mitsko
 *
 */
public final class Director {
    /**
     * Закрытый конструктор.
     */
    private Director() {
    }

    /**
     * Метод, возвращающий созданный объект на основе конкретной реализации
     * абстрактного класса AbstractBuilder.
     * @param builder - объект дочернего класса, насдедующегося
     * от AbstractBuilder
     * @param entity - любой объект, являющийся потомком Entity
     * @param resultSet - объект ResultSet
     * @return созданный объект конкретного класса-потомка Entity
     */
    public static Entity createEntity(final AbstractBuilder builder,
            final Entity entity, final ResultSet resultSet) {
        builder.buildEntity(entity, resultSet);
        return builder.getEntity();

    }
}

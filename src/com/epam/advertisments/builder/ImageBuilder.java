package com.epam.advertisments.builder;

import java.sql.Blob;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.epam.advertisments.model.Image;

/**
 * Класс для создания объектоов типа Image.
 * @author Irina Mitsko
 *
 */
public class ImageBuilder extends AbstractBuilder<Image> {

    /**
     * Метод, конструирующий конкретный объект Image на основе
     *  полученных из базы данных полей.
     */
    @Override
    public void buildEntity(final Image image, final ResultSet resultSet) {
        try {
            if (image.getId() == 0) {
                final int imageId
                        = Integer.parseInt(resultSet.getString("id"));
                image.setId(imageId);
            }
            if (image.getAdvertismentId() == 0) {
                final int advertismentId
                = Integer.parseInt(resultSet.getString("advertisment_id"));
                image.setAdvertismentId(advertismentId);
            }
                final Blob img = resultSet.getBlob("image");
                image.setImage(img);
                final String fileName = resultSet.getString("file_name");
                image.setFileName(fileName);
                final String encoding = resultSet.getString("encoding");
                image.setEncoding(encoding);
            
        } catch (SQLException e) {
            LOGGER.error("Ошибка при работе с базой данных!" + e);
            final StackTraceElement[] trace = e.getStackTrace();
            for (final StackTraceElement el : trace) {
                LOGGER.error("\n " + el);
            }
        }

    }

}

package com.epam.advertisments.builder;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.epam.advertisments.model.Category;

/**
 * Класс для создания объектоов типа Сategory.
 * @author Irina Mitsko
 *
 */
public class CategoryBuilder extends AbstractBuilder<Category> {

    /**
     * Метод, конструирующий конкретную сущность на основе полученных
     *  из базы данных полей.
     */
    @Override
    public void buildEntity(final Category category,
            final ResultSet resultSet) {
        try {
            final String name = resultSet.getString("category");
            category.setName(name);
            if (category.getId() == 0) {
                final int categoryId = Integer.parseInt(resultSet.getString("id"));
                category.setId(categoryId);
            }
        } catch (SQLException e) {
            LOGGER.error("Ошибка при работе с базой данных!" + e);
            final StackTraceElement[] trace = e.getStackTrace();
            for (final StackTraceElement el : trace) {
                LOGGER.error("\n " + el);
            }
        }

    }

}

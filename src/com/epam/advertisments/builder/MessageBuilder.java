package com.epam.advertisments.builder;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.epam.advertisments.model.Message;

/**
 * Класс для создания объектоов типа Message.
 * @author Irina Mitsko
 *
 */
public class MessageBuilder extends AbstractBuilder<Message> {

    /**
     * Метод, конструирующий объект типа Message на основе полученных
     * из базы данных полей.
     */
    @Override
    public void buildEntity(final Message message,
            final ResultSet resultSet) {
        try {
            if (message.getId() == 0) {
                final int messageId = Integer.parseInt(resultSet.getString("id"));
                message.setId(messageId);
            }
            if (message.getReceiverId() == 0) {
                if(resultSet.getString("receiver_id") != null) {
                    final int receiver
                    = Integer.parseInt(resultSet.getString("receiver_id"));
                message.setReceiverId(receiver);
                }
            }
            if (message.getAdvertismentId() == 0) {
                if(resultSet.getString("advertisment_id") != null) {
                    final int advertismrntId
                    = Integer.parseInt(resultSet.getString("advertisment_id"));
                message.setAdvertismentId(advertismrntId);
                } else {
                    message.setAdvertismentId(0);
                }
                
            }
            if (resultSet.getString("sender_id") != null) {
                final int sender
                = Integer.parseInt(resultSet.getString("sender_id"));
            message.setSenderId(sender);
            }
            final String text = resultSet.getString("message_text");
            message.setMessage(text);
            final String date = resultSet.getString("date");
            message.setDate(date);
        } catch (NumberFormatException e) {
            LOGGER.error("Ошибка! Несоответствие типа данных полей." + e);
            final StackTraceElement[] trace = e.getStackTrace();
            for (final StackTraceElement el : trace) {
                LOGGER.error("\n " + el);
            }
        } catch (SQLException e) {
            LOGGER.error("Ошибка при работе с базой данных!" + e);
            final StackTraceElement[] trace = e.getStackTrace();
            for (final StackTraceElement el : trace) {
                LOGGER.error("\n " + el);
            }
        }
    }

}

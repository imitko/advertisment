package com.epam.advertisments.resource;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Enum для извлечения из файла текстовых сообщений,
 * показываемых пользователю.
 * @author Irina Mitsko
 *
 */
public enum MessageManager {
    /**
     * Экземпляр класса для получения
     * доступа к свойствам класса.
     */
    INSTANCE;
    /**
     * Ссылка на объект класса ResourceBundle.
     */
    private ResourceBundle resourceBundle;
    /**
     * Путь к файлу с языковыми константами.
     */
    private static final String RESOURCE_NAME
        = "resources.messages";

    /**
     * Конструктор для инициализации объекта ResourceBundle.
     */
    MessageManager() {
        resourceBundle
            = ResourceBundle.getBundle(RESOURCE_NAME, Locale.getDefault());
    }

    /**
     * Метод для изменения язывковой локали.
     * @param locale - выбранная локаль
     */
    public void changeResource(final Locale locale) {
        resourceBundle
            = ResourceBundle.getBundle(RESOURCE_NAME, locale);
    }

    /**
     * Метод для вывода языковой константы.
     * @param key - ключ константы
     * @return значение констатны
     */
    public String getString(final String key) {
        return resourceBundle.getString(key);
    }

}

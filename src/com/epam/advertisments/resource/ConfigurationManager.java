package com.epam.advertisments.resource;

import java.util.ResourceBundle;
/**
 * Класс для извлечения информации из файла config.properties.
 * @author Irina Mitsko
 *
 */
public final class ConfigurationManager {
    /**
     * Путь к файлу с языковыми константами.
     */
    private static final ResourceBundle RESOURCE_BUNDLE
        = ResourceBundle.getBundle("resources.config");

    /**
     * Конструктор для ограничения создания экземпляров
     * класса.
     */
    private ConfigurationManager() {
    }

    /**
     * Метод для вывода языковой константы.
     * @param key - ключ константы
     * @return значение констатны
     */
    public static String getProperty(final String key) {
        return RESOURCE_BUNDLE.getString(key);
    }
}

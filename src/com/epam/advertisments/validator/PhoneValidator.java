package com.epam.advertisments.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Класс для валидации данных поля "телефон".
 * @author Irina Mitsko
 *
 */
public class PhoneValidator {
    /**
     * Шаблон для введенных данных в поле "телефон".
     */
    private static final String PHONE_PATTERN = "[\\+]\\d{3}\\s\\d{2}\\s\\d{7}";
    /**
     * Ссылка на поле типа Pattern.
     */
    private Pattern pattern;
    /**
     * Ссылка на поле типа Matcher.
     */
    private Matcher matcher;

    /**
     * Конструктор c инициализацией поля Pattern.
     */
    public PhoneValidator() {
        pattern = Pattern.compile(PHONE_PATTERN);
    }


    /**
     * Метод проверяет корректность ввделенного телефона (длина и цифры).
     * @param phone - строка, содеожащая данные с полем телефон
     * @return true если введенные данные корректны
     */
    public boolean validatePhone(final String phone) {
        matcher = pattern.matcher(phone);
        return matcher.matches();
    }
}

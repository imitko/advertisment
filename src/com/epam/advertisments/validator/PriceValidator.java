package com.epam.advertisments.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Класс для проверки валидности введенной
 *  пользователем цены товара.
 * @author Irina Mitsko
 *
 */
public class PriceValidator {
    /**
     * Шаблон для введенных данных в поле "телефон".
     */
    private static final String PRICE_PATTERN = "\\d+\\.?\\d*?";
    /**
     * Ссылка на поле типа Pattern.
     */
    private Pattern pattern;
    /**
     * Ссылка на поле типа Matcher.
     */
    private Matcher matcher;

    /**
     * Конструктор c инициализацией поля Pattern.
     */
    public PriceValidator() {
        pattern = Pattern.compile(PRICE_PATTERN);
    }
    /**
     * Метод проверяет корректность ввденной суммы на наличие положительных
     *  целых или дробных чисел.
     * @param price - строка, содержащая данные с полем цена
     * @return true если введенные данные корректны
     */
    public boolean validatePrice(final String price) {
        matcher = pattern.matcher(price);
        return matcher.matches();
    }
}

package com.epam.advertisments.validator;



import com.epam.advertisments.dao.UserDAO;
import com.epam.advertisments.model.User;
import com.epam.advertisments.service.PasswordChecker;
/**
 * Класс для валидации авторизационных данных пользователя.
 * @author Irina Mitsko
 *
 */
public final class LoginValidator {
    /**
     * Закрытый конструктор.
     */
    private LoginValidator() {
    }
    /**
     * Метод проверяет корректность введенных логина и пароля.
     * @param dao - ссылка на объект dao
     * @param email - емейл, укоторый ввел пользователь
     * @param password - пароль, который ввел пользователь
     * @return true, если введенные данные верны
     */
    public static boolean valigateLoginData(final UserDAO dao,
            final String email, final String password) {
        final User user = dao.findEntity(email);
        return user != null
                && PasswordChecker.hashPassword(password)
                .equals(user.getPassword());
    }

}

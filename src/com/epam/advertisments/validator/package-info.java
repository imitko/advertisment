/**
 * Пакет с классом для валидации введенных данных
 *  в формы на сайте.
 */
/**
 * @author Irina Mitsko
 *
 */
package com.epam.advertisments.validator;

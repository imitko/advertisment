package com.epam.advertisments.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Класс для валидации данных поля "емейл".
 * @author Irina Mitsko
 *
 */
public class EmailValidator {

    /**
     * Шаблон для введенных данных в поле "телефон".
     */
    private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+"
            + "(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\-?[A-Za-z0-9]*)*"
            + "(\\.[A-Za-z]{2,})$";
    /**
     * Ссылка на поле типа Pattern.
     */
    private Pattern pattern;
    /**
     * Ссылка на поле типа Matcher.
     */
    private Matcher matcher;

    /**
     * Конструктор c инициализацией поля Pattern.
     */
    public EmailValidator() {
        pattern = Pattern.compile(EMAIL_PATTERN);
    }

    /**
     * Метод проверяет корректность ввденного eмейла (наличие "@" и ".").
     * @param email - строка, содержащая данные с полем емейл
     * @return true если введенные данные корректны
     */
    public boolean validateEmail(final String email) {
        matcher = pattern.matcher(email);
        return matcher.matches();
    }
}

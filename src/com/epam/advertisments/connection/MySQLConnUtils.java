package com.epam.advertisments.connection;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *  Класс с  методами освобождения ресурсов при
 *   работе с соединением.
 * @author Irina Mitsko
 */
public final class MySQLConnUtils {

    /**
     * Конструктор по-умолчанию.
     */
    private MySQLConnUtils() {
    }
    /**
     * Логгер для класса.
     */
    static final Logger LOGGER
        = (Logger) LogManager.getLogger(MySQLConnUtils.class);

    /**
     * Метод, закрывающий Statement.
     * @param statement - экземпляр созданного Statement
     */
    public static void closeStatement(final Statement statement) {
        if (statement != null) {
            try {
                statement.close();
            } catch (SQLException e) {
                LOGGER.error("Ошибка!" + e);
                final StackTraceElement[] trace = e.getStackTrace();
                for (final StackTraceElement el : trace) {
                    LOGGER.error("\n " + el);
                }
            }
        }
    }
       

}

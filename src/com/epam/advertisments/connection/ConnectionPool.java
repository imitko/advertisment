package com.epam.advertisments.connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import com.epam.advertisments.resource.ConfigurationManager;

/**
 * Класс для создания пула соединений с базой данных.
 * @author Irina Mitsko
 *
 */
public final class ConnectionPool {
    /**
     * Единственный экземпляр класса.
     */
    private static ConnectionPool instance;
    /**
     * Количество разрешенных соединений одновременно.
     */
    private static final int ALLOWED_CONNECTIONS = 3;

    /**
     * Создание блокирующей очереди открытых соединений.
     */
     static BlockingQueue<Connection> freeConnections
        = new ArrayBlockingQueue<>(ALLOWED_CONNECTIONS);
    /**
     * Создание блокирующей очереди используемых соединений.
     */
    private static BlockingQueue<Connection> usedConnections
    = new ArrayBlockingQueue<>(ALLOWED_CONNECTIONS);
    
    /**
     * Флаг для проверки создания объекта класса.
     */
    private static AtomicBoolean isCreated = new AtomicBoolean(false);

    /**
     * Логгер для класса.
     */
    static final Logger LOGGER
        = (Logger) LogManager.getLogger(ConnectionPool.class);

    /**
     * Закрытый конструктор для предотвращения возможности создания более 1
     * экземпляра класса.
     */
    private ConnectionPool() {
        for (int i = 0; i < ALLOWED_CONNECTIONS; i++) {
            Connection connection = null;
            try {
                Class.forName(ConfigurationManager
                        .getProperty("db.driverclass"));
                try {
                    connection
                    = DriverManager.getConnection(ConfigurationManager
                            .getProperty("db.url"),
                            ConfigurationManager.getProperty("db.user"),
                            ConfigurationManager.getProperty("db.password"));
                    LOGGER.info("Создано соединение #" + i);
                    freeConnections.put(connection);
                    LOGGER.info("Соединение добавлено в очередь.");
                } catch (SQLException | InterruptedException  e) {
                    LOGGER.error("Ошибка!" + e);
                    final StackTraceElement[] trace = e.getStackTrace();
                    for (final StackTraceElement el : trace) {
                        LOGGER.error("\n " + el);
                    }
                }
            } catch (ClassNotFoundException e) {
                LOGGER.error("Ошибка!" + e);
                final StackTraceElement[] trace = e.getStackTrace();
                for (final StackTraceElement el : trace) {
                    LOGGER.error("\n " + el);
                }
            }
        }
    }

    private static ReentrantLock locker = new ReentrantLock();

    /**
     * Геттер для получения экземпляра класса.
     * @return instance - экземпляр класса.
     */
    public static ConnectionPool getInstance() {
        if(!isCreated.get()) {
        locker.lock();
        try {
            if (instance == null) {
                instance = new ConnectionPool();
                isCreated = new AtomicBoolean(true);
            }
        } finally {
            locker.unlock();
        }
        }
        return instance;
    }

    /**
     * Метод, получающий соединение из очереди.
     * @return connection - экземпляр соединения
     */
    public Connection getConnection() {
        Connection connection = null;
        try {
            connection = freeConnections.take();
            usedConnections.put(connection);
            LOGGER.info("Соединение взято из пула.");
        } catch (InterruptedException e) {
            LOGGER.error("Ошибка!" + e);
            final StackTraceElement[] trace = e.getStackTrace();
            for (final StackTraceElement el : trace) {
                LOGGER.error("\n " + el);
            }
        }
        return connection;
    }

    /**
     * Метод возвращения соединения в очередь свободных соединений.
     * @param connection - используемое соединение.
     */
    public void freeConnection(final Connection connection) {
        try {
            usedConnections.take();
            freeConnections.put(connection);
            LOGGER.info("Соединение возвращено в пул.");
        } catch (InterruptedException e) {
            LOGGER.error("Ошибка!" + e);
            final StackTraceElement[] trace = e.getStackTrace();
            for (final StackTraceElement el : trace) {
                LOGGER.error("\n " + el);
            }

        }
    }
    
    /**
     * Метод закрытия всех соединений из очереди свободных соединений.
     * @param connection - используемое соединение.
     */
    public void closeFreeConnections() {
        for (final Connection freeConn : freeConnections) {
            if(freeConn != null) {
                try {
                    freeConn.close();
                } catch (SQLException e) {
                    LOGGER.error("Ошибка!" + e);
                    final StackTraceElement[] trace = e.getStackTrace();
                    for (final StackTraceElement el : trace) {
                        LOGGER.error("\n " + el);
                    }
                }
            }
        }
    }
    
    /**
     * Метод закрытия всех соединений из очереди используемых соединений.
     * @param connection - используемое соединение.
     */
    public void closeUsedConnections() {
        for (final Connection usedConn : usedConnections) {
            if(usedConn != null) {
                try {
                    usedConn.close();
                } catch (SQLException e) {
                    LOGGER.error("Ошибка!" + e);
                    final StackTraceElement[] trace = e.getStackTrace();
                    for (final StackTraceElement el : trace) {
                        LOGGER.error("\n " + el);
                    }
                }
            }
        }
    }

}
